import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

public class SortLines {
    public static void main(String[] args) {
        Path inputFilePath = Paths.get("D:\\SoftUni\\SoftUni\\InputOutput,FilesAndDirectories\\6.SortLines\\resources\\input.txt");
        Path outputFilePath = Paths.get("D:\\SoftUni\\SoftUni\\InputOutput,FilesAndDirectories\\6.SortLines\\resources\\output.txt");

        try{
            List<String> lines = Files.readAllLines(inputFilePath);
            Collections.sort(lines);
            Files.write(outputFilePath, lines);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
