import java.io.*;
import java.util.Scanner;

public class ExtractIntegers {
    public static void main(String[] args) {
        String inputFilePath = "D:\\SoftUni\\SoftUni\\InputOutput,FilesAndDirectories\\04.ExtractIntegers\\resources\\input.txt";
        String outputFilePath = "D:\\SoftUni\\SoftUni\\InputOutput,FilesAndDirectories\\04.ExtractIntegers\\resources\\output.txt";

        try (Scanner in = new Scanner(new FileInputStream(inputFilePath)); PrintWriter out = new PrintWriter(new FileOutputStream(outputFilePath))) {

            while (in.hasNext()) {
                if (in.hasNextInt()) {
                    out.println(in.nextInt());
                }
                in.next();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
