import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class CopyBytes {
    public static void main(String[] args) {
        String inputFilePath = "D:\\SoftUni\\SoftUni\\InputOutput,FilesAndDirectories\\03.CopyBytes\\resources\\input.txt";
        String outputFilePath = "D:\\SoftUni\\SoftUni\\InputOutput,FilesAndDirectories\\03.CopyBytes\\resources\\output.txt";

        int asciiCodeEmptySpace = 32;
        int asciiCodeNewLine = 10;

        try (FileInputStream in = new FileInputStream(inputFilePath); FileOutputStream out = new FileOutputStream(outputFilePath)) {
            int oneByte = 0;
            while ((oneByte = in.read()) >= 0) {
                if (oneByte == asciiCodeEmptySpace || oneByte == asciiCodeNewLine) {
                    out.write(oneByte);
                } else {
                    String digits = String.valueOf(oneByte);
                    for (int i = 0; i < digits.length(); i++) {
                        out.write(digits.charAt(i));
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
