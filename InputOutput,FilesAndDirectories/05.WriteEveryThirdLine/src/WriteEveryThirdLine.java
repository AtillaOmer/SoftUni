import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class WriteEveryThirdLine {
    public static void main(String[] args) {
        String inputFilePath = "D:\\SoftUni\\SoftUni\\InputOutput,FilesAndDirectories\\05.WriteEveryThirdLine\\resources\\input.txt";
        String outputFilePath = "D:\\SoftUni\\SoftUni\\InputOutput,FilesAndDirectories\\05.WriteEveryThirdLine\\resources\\output.txt";

        try (Scanner in = new Scanner(new FileInputStream(inputFilePath)); PrintWriter out = new PrintWriter(new FileOutputStream(outputFilePath))) {
            String line = in.nextLine();
            int counter = 1;
            while (in.hasNext()) {
                if (counter % 3 == 0) {
                    out.println(line);
                }
                counter++;
                line = in.nextLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
