import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class WriteToFile {
    public static void main(String[] args) {
        String inputFilePath = "D:\\SoftUni\\SoftUni\\InputOutput,FilesAndDirectories\\02.WriteToFile\\resources\\input.txt";
        String outputFilePath = "D:\\SoftUni\\SoftUni\\InputOutput,FilesAndDirectories\\02.WriteToFile\\resources\\output.txt";

        List<Character> symbols = new ArrayList<>();
        Collections.addAll(symbols, ',', '.', '!', '?');
        try (FileInputStream inputStream = new FileInputStream(inputFilePath); FileOutputStream outputStream = new FileOutputStream(outputFilePath)) {

            int oneByte = 0;

            while ((oneByte = inputStream.read()) >= 0) {
                if (!symbols.contains((char) oneByte)) {
                    outputStream.write(oneByte);
                }
            }

        }  catch (IOException e) {
            e.printStackTrace();
        }
    }
}
