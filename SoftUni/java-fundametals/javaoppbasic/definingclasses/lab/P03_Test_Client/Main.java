package javaoppbasic.definingclasses.lab.P03_Test_Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Map<Integer, BankAccount> accountMap = new HashMap<>();

        String input;

        while (true) {
            if ("End".equals(input = reader.readLine())) {
                break;
            }

            String[] tokens = input.split(" ");
            int id = Integer.parseInt(tokens[1]);
            try {

                switch (tokens[0]) {
                    case "Create":
                        if (!accountMap.containsKey(id)) {
                            BankAccount account = new BankAccount();
                            account.setId(id);
                            accountMap.put(id, account);
                        } else {
                            System.out.println("Account already exists");
                        }
                        break;
                    case "Deposit":
                        double amuount = Double.parseDouble(tokens[2]);
                        if (accountMap.containsKey(id)) {
                            BankAccount account = accountMap.get(id);
                            account.deposit(amuount);
                        } else {
                            System.out.println("Account does not exist");
                        }
                        break;
                    case "Withdraw":
                        id = Integer.parseInt(tokens[1]);
                        amuount = Double.parseDouble(tokens[2]);
                        if (accountMap.containsKey(id)) {
                            BankAccount account = accountMap.get(id);
                            if (account.getBalance() >= amuount && amuount >= 0)
                                account.withdraw(amuount);
                            else
                                System.out.println("Insufficient balance");
                        } else {
                            System.out.println("Account does not exist");
                        }
                        break;
                    case "Print":
                        if (accountMap.containsKey(id)) {
                            BankAccount account = accountMap.get(id);
                            System.out.println(String.format("Account %s, balance %.2f", account, account.getBalance()));
                        } else {
                            System.out.println("Account does not exist");
                        }
                        break;
                }
            } catch (Exception ex) {
                break;
            }
        }
    }
}
