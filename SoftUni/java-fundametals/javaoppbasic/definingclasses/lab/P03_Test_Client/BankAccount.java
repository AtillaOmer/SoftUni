package javaoppbasic.definingclasses.lab.P03_Test_Client;

public class BankAccount {
    private final static double DEFAULT_INTEREST = 0.2;

    private static double rate = DEFAULT_INTEREST;
    private static int bankAccountCount;

    private int id;
    private double balance;

    public BankAccount() {
        this.id = ++bankAccountCount;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public double getBalance() {
        return balance;
    }

    public void deposit(double amount) {
        if (amount >= 0)
            this.balance += amount;
    }

    public void withdraw(double amount) {
        if (amount <= balance && amount >= 0) {
            this.balance -= amount;
        }
    }

    public static void setInterestRate(double interest) {
        rate = interest;
    }

    public double getInterest(int years) {
        return this.balance * rate * years;
    }

    @Override
    public String toString() {
        return String.format("ID" + this.id);
    }
}
