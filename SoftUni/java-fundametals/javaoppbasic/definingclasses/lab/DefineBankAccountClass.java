package javaoppbasic.definingclasses.lab;

import javaoppbasic.definingclasses.lab.P03_Test_Client.BankAccount;

public class DefineBankAccountClass {
    public static void main(String[] args) {
        BankAccount ac = new BankAccount();

        ac.setId(1);
        ac.deposit(150000);

        System.out.println(String.format("Account %s, balance: %.2f ",ac, ac.getBalance()));
    }
}
