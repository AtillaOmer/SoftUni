package javaoppbasic.definingclasses.lab;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Map<Integer, BankAccount> accountMap = new HashMap<>();

        String input = reader.readLine();
        while (true) {
            if (input.equals("End")) {
                break;
            }

            String[] tokens = input.split("\\s+");
            String command = tokens[0];

            switch (command) {
                case "Create":
                    BankAccount account = new BankAccount();
                    accountMap.put(account.getId(), account);
                    System.out.println(String.format("Account ID%s created", account.getId()));
                    break;

                case "Deposit":
                    int id = Integer.parseInt(tokens[1]);
                    BigDecimal amount =  new BigDecimal(tokens[2]);

                    if (accountMap.containsKey(id)) {
                        account = accountMap.get(id);
                        account.deposit(Double.parseDouble(amount.toString()));
                        System.out.println("Deposited " + amount + " to ID" + account.getId());
                    } else {
                        System.out.println("Account does not exist");
                    }
                    break;

                case "SetInterest":
                    double interest = Double.parseDouble(tokens[1]);
                    BankAccount.setInterestRate(interest);
                    break;

                case "GetInterest":
                    id = Integer.parseInt(tokens[1]);
                    int year = Integer.parseInt(tokens[2]);
                    if (accountMap.containsKey(id)) {
                        account = accountMap.get(id);
                        System.out.println(String.format("%.2f", account.getInterest(year)));
                    } else {
                        System.out.println("Account does not exist");
                    }
                    break;
            }

            input = reader.readLine();
        }

    }
}
