package javaoppbasic.definingclasses.lab;

import javaoppbasic.definingclasses.lab.P03_Test_Client.BankAccount;

public class GettersAndSetters {
    public static void main(String[] args) {
        BankAccount account = new BankAccount();

        account.setId(1);
        account.deposit(15000);
        account.withdraw(5000);

        System.out.println(String.format("Account %s, balance %.2f", account, account.getBalance()));
    }
}
