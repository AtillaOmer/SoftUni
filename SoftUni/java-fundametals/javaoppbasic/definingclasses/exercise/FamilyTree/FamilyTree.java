package javaoppbasic.definingclasses.exercise.FamilyTree;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FamilyTree {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Pattern dateAndDate = Pattern.compile("(\\d+\\/\\d+\\/\\d{4}) - (\\d+\\/\\d+\\/\\d{4})");
        Pattern dateAndName = Pattern.compile("(\\d+\\/\\d+\\/\\d{4}) - (\\w+\\s+\\w+)");
        Pattern nameAndDate = Pattern.compile("(\\w+\\s+\\w+) - (\\d+\\/\\d+\\/\\d+)");
        Pattern nameAndName = Pattern.compile("(\\w+\\s+\\w+) - (\\w+\\s+\\w+)");
        Pattern personPattern = Pattern.compile("(\\w+\\s+\\w+)\\s+(\\d+\\/\\d+\\/\\d+)");

        String searchedPersonName = reader.readLine();

        Map<String, Person> peopleWithName = new LinkedHashMap<>();
        Map<String, Person> peopleWithDate = new LinkedHashMap<>();
        String input;

        while (true) {

            if ("End".equals(input = reader.readLine()))
                break;

            Matcher dateAndDateMatcher = dateAndDate.matcher(input);
            Matcher dateAndNameMatcher = dateAndName.matcher(input);
            Matcher nameAndDateMatcher = nameAndDate.matcher(input);
            Matcher nameAndNameMatcher = nameAndName.matcher(input);
            Matcher personMatcher = personPattern.matcher(input);

            if (dateAndDateMatcher.find()) {
                String firstBirthday = dateAndDateMatcher.group(1);
                String secondBirthday = dateAndDateMatcher.group(2);

                peopleWithDate.putIfAbsent(firstBirthday, new Person());
                peopleWithDate.putIfAbsent(secondBirthday, new Person());

                Person person = peopleWithDate.get(firstBirthday);
                Person childPerson = peopleWithDate.get(secondBirthday);

                person.setBirthDay(firstBirthday);
                childPerson.setBirthDay(secondBirthday);


                person.getChildren().add(childPerson);
                childPerson.getParents().add(person);

                peopleWithDate.put(firstBirthday, person);
                peopleWithDate.putIfAbsent(secondBirthday, childPerson);
            }

            if (dateAndNameMatcher.find()) {
                String birthday = dateAndNameMatcher.group(1);
                String name = dateAndNameMatcher.group(2);

                peopleWithDate.putIfAbsent(birthday, new Person());
                Person person = peopleWithDate.get(birthday);
                person.setBirthDay(birthday);

                peopleWithName.putIfAbsent(name, new Person(name));
                Person childPerson = peopleWithName.get(name);


                person.getChildren().add(childPerson);
                childPerson.getParents().add(person);

                peopleWithDate.put(birthday, person);
                peopleWithName.put(name, childPerson);
            }

            if (nameAndDateMatcher.find()) {
                String name = nameAndDateMatcher.group(1);
                String date = nameAndDateMatcher.group(2);

                peopleWithName.putIfAbsent(name, new Person(name));
                peopleWithDate.putIfAbsent(date, new Person());

                Person person = peopleWithName.get(name);
                Person childPerson = peopleWithDate.get(date);
                childPerson.setBirthDay(date);

                person.getChildren().add(childPerson);
                childPerson.getParents().add(person);

                peopleWithName.put(name, person);
                peopleWithDate.put(date, childPerson);

            }

            if (nameAndNameMatcher.find()) {
                String firstName = nameAndNameMatcher.group(1);
                String secondName = nameAndNameMatcher.group(2);

                peopleWithName.putIfAbsent(firstName, new Person(firstName));
                peopleWithName.putIfAbsent(secondName, new Person(secondName));

                Person person = peopleWithName.get(firstName);
                Person child = peopleWithName.get(secondName);

                person.getChildren().add(child);
                child.getParents().add(person);

                peopleWithName.putIfAbsent(firstName, person);
                peopleWithName.put(secondName, child);
            }

            if (personMatcher.find()) {
                String name = personMatcher.group(1);
                String birthday = personMatcher.group(2);


                peopleWithName.putIfAbsent(name, new Person(name, birthday));
                peopleWithDate.putIfAbsent(birthday, new Person(name, birthday));


                Person personWithName = peopleWithName.get(name);
                Person personWithDate = peopleWithDate.get(birthday);

                personWithName.setBirthDay(birthday);
                personWithDate.setName(name);

                List<Person> parents = personWithName.getParents();
                List<Person> parents1 = personWithDate.getParents();

                List<Person> children = personWithName.getChildren();
                List<Person> children1 = personWithDate.getChildren();

                synchronizeLists(parents, parents1);
                synchronizeLists(children, children1);

                peopleWithName.put(name, personWithName);
                peopleWithDate.put(birthday, personWithDate);
            }
        }

        if (searchedPersonName.contains("/")) {
            System.out.println(peopleWithDate.get(searchedPersonName).toString());
            System.out.println("Parents:");
            for (Person person : peopleWithDate.get(searchedPersonName).getParents()) {
                System.out.println(person.toString());
            }
            System.out.println("Children:");
            for (Person person : peopleWithDate.get(searchedPersonName).getChildren()) {
                System.out.println(person.toString());
            }
        } else {
            System.out.println(peopleWithName.get(searchedPersonName).toString());
            System.out.println("Parents:");
            for (Person person : peopleWithName.get(searchedPersonName).getParents()) {
                System.out.println(person.toString());
            }
            System.out.println("Children:");
            for (Person person : peopleWithName.get(searchedPersonName).getChildren()) {
                System.out.println(person.toString());
            }
        }
    }

    private static void synchronizeLists(List<Person> listOne, List<Person> listTwo) {

        if (listOne.size() == 0 && listTwo.size() == 0)
            return;

        if (listOne.isEmpty() && !listTwo.isEmpty()) {
            listOne.addAll(listTwo);
            return;
        }

        if (!listOne.isEmpty() && listTwo.isEmpty()) {
            listTwo.addAll(listOne);
            return;
        }
        try {
            if (listOne.size() <= listTwo.size()) {
                List<Person> temp = new ArrayList<>();
                for (Person person : listOne) {
                    for (Person person1 : listOne) {
                        if (!(person.getName().equals(person1.getName())) || !(person1.getBirthDay().equals(person.getBirthDay()))) {
                            temp.add(person);
                        }
                    }
                }
                listOne.addAll(temp);
            } else if (listOne.size() >= listTwo.size()) {
                List<Person> temp = new ArrayList<>();
                for (Person person : listOne) {
                    for (Person person1 : listTwo) {
                        if (!person1.getName().equals(person.getName()) || !person1.getBirthDay().equals(person.getBirthDay())) {
                            temp.add(person);
                        }
                    }
                }
                listTwo.addAll(temp);
            }
        } catch (Exception ex) {
            System.out.println("");
        }
    }
}

class Person {
    private String name;
    private String birthDay;
    private List<Person> parents;
    private List<Person> children;

    public Person() {
        this.parents = new ArrayList<>();
        this.children = new ArrayList<>();
    }

    public Person(String name) {
        this.name = name;
        this.parents = new ArrayList<>();
        this.children = new ArrayList<>();
    }

    public Person(String name, String birthDay) {
        this.name = name;
        this.birthDay = birthDay;
        this.parents = new ArrayList<>();
        this.children = new ArrayList<>();
    }

    public Person(String name, String birthDay, List<Person> parents, List<Person> children) {
        this.name = name;
        this.birthDay = birthDay;
        this.parents = parents;
        this.children = children;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public List<Person> getParents() {
        return parents;
    }

    public void setParents(List<Person> parents) {
        this.parents = parents;
    }

    public List<Person> getChildren() {
        return children;
    }

    public void setChildren(List<Person> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return String.format("%s %s", this.name, this.birthDay);
    }
}
