package javaoppbasic.definingclasses.exercise.CompanyRoster;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CompanyRoster {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Map<String, Department> departments = new HashMap<>();
        int n = Integer.parseInt(reader.readLine());

        while (n-- > 0) {

            String[] tokens = reader.readLine().split("\\s+");
            String employeeName = tokens[0];
            double employeeSalary = Double.parseDouble(tokens[1]);
            String employeePosition = tokens[2];
            String employeeDepartment = tokens[3];
            String employeeEmail;
            int employeeAge;

            Employee employee = new Employee(employeeName, employeeSalary, employeePosition, employeeDepartment);

            if (tokens.length == 5)
                if (tokens[4].contains("@")) {
                    employeeEmail = tokens[4];
                    employee.setEmail(employeeEmail);
                } else {
                    employeeAge = Integer.parseInt(tokens[4]);
                    employee.setAge(employeeAge);
                }
            if (tokens.length == 6) {
                employeeEmail = tokens[4];
                employeeAge = Integer.parseInt(tokens[5]);
                employee.setEmail(employeeEmail);
                employee.setAge(employeeAge);
            }

            if (departments.containsKey(employeeDepartment)) {
                Department department = departments.get(employeeDepartment);


                department.getEmployees().add(employee);
                department.setSalary(employeeSalary);

                departments.put(employeeDepartment, department);
            } else {
                List<Employee> employees = new ArrayList<>();
                employees.add(employee);

                Department department = new Department(employeeDepartment, employees);
                department.setSalary(employeeSalary);
                departments.put(employeeDepartment, department);
            }
        }

        Department maxDepartment = new Department();
        double maxValue = Double.MIN_VALUE;
        for (Map.Entry<String, Department> department : departments.entrySet()) {


            if (department.getValue().getSalary()/department.getValue().getEmployees().size() > maxValue) {
                maxValue = department.getValue().getSalary()/department.getValue().getEmployees().size();
                maxDepartment = department.getValue();
            }
        }

        System.out.println("Highest Average Salary: " + maxDepartment.getName());
        maxDepartment.getEmployees().stream()
                .sorted((employee1, employee2) -> Double.compare(employee2.getSalary(), employee1.getSalary()))
                .forEach(employee -> System.out.println(String.format("%s %.2f %s %d", employee.getName(),
                        employee.getSalary(), employee.getEmail(), employee.getAge())));
    }
}

class Employee {
    private String name;
    private double salary;
    private String position;
    private String department;
    private String email;
    private int age;

    public Employee() {
    }

    public Employee(String name, double salary, String position, String department) {
        this.name = name;
        this.salary = salary;
        this.position = position;
        this.department = department;
        this.email = "n/a";
        this.age = -1;
    }

    public Employee(String name, double salary, String position, String department, String email) {
        this.name = name;
        this.salary = salary;
        this.position = position;
        this.department = department;
        this.email = email;
        this.age = -1;
    }

    public Employee(String name, double salary, String position, String department, int age) {
        this.name = name;
        this.salary = salary;
        this.position = position;
        this.department = department;
        this.email = "n/a";
        this.age = age;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}

class Department {
    private double salary = 0;
    private String name;
    private List<Employee> employees;

    public Department() {
    }

    public Department(String name, List<Employee> employees) {
        this.name = name;
        this.employees = employees;
    }

    public double getSalary() {
        return this.salary;
    }

    public void setSalary(double salary) {
        this.salary += salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public double getSalarySum() {
        return this.employees.stream()
                .mapToDouble(x -> (x.getSalary()))
                .sum();
    }
}
