package javaoppbasic.definingclasses.exercise.CarSalesman;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CarSalesman {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());
        Map<String, Engine> engines = new LinkedHashMap<>();
        List<Car> cars = new ArrayList<>();

        while (n-- > 0) {
            String[] tokens = reader.readLine().split("\\s+");
            String engineModel = tokens[0];
            int enginePower = Integer.parseInt(tokens[1]);
            int engineDisplacement;
            String engineEfficiency;

            Engine engine = new Engine(engineModel, enginePower);

            if (tokens.length == 3) {
                try {
                    engineDisplacement = Integer.parseInt(tokens[2]);
                    engine.setDisplacement(engineDisplacement);
                } catch (Exception ex) {
                    engineEfficiency = tokens[2];
                    engine.setEfficiency(engineEfficiency);
                }
            }
            if (tokens.length == 4) {
                engineDisplacement = Integer.parseInt(tokens[2]);
                engineEfficiency = tokens[3];
                engine.setDisplacement(engineDisplacement);
                engine.setEfficiency(engineEfficiency);
            }

            engines.put(engineModel, engine);
        }

        int m = Integer.parseInt(reader.readLine());

        while (m-- > 0) {

            String[] tokens = reader.readLine().split("\\s+");
            String carModel = tokens[0];
            String carEngine = tokens[1];


            Engine engine = engines.get(carEngine);
            Car car = new Car(carModel, engine);

            if (tokens.length == 3) {
                if (tokens[2].matches("\\d+")) {
                    int carWeight = Integer.parseInt(tokens[2]);
                    car.setWeight(carWeight);
                } else {
                    String carColor = tokens[2];
                    car.setColor(carColor);
                }
            }

            if (tokens.length == 4) {
                int carWeight = Integer.parseInt(tokens[2]);
                String carColor = tokens[3];
                car.setWeight(carWeight);
                car.setColor(carColor);
            }

            cars.add(car);
        }

        cars.stream().forEach(System.out::println);
    }
}

class Car {
    private String model;
    private Engine engine;
    private int weight;
    private String color;

    public Car() {
    }

    public Car(String model, Engine engine) {
        this.model = model;
        this.engine = engine;
    }

    public Car(String model, Engine engine, int weight, String color) {
        this.model = model;
        this.engine = engine;
        this.weight = weight;
        this.color = color;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(this.model + ":").append(System.lineSeparator())
                .append(this.engine.toString()).append(System.lineSeparator())
                .append(String.format("Weight: %s", this.weight == 0 ? "n/a" : this.weight)).append(System.lineSeparator())
                .append(String.format("Color: %s", this.color == null ? "n/a" : this.color));

        return stringBuilder.toString();
    }
}

class Engine {
    private String model;
    private int power;
    private int displacement;
    private String efficiency;

    public Engine() {
    }

    public Engine(String model, int power) {
        this.model = model;
        this.power = power;
    }

    public Engine(String model, int power, int displacement, String efficiency) {
        this.model = model;
        this.power = power;
        this.displacement = displacement;
        this.efficiency = efficiency;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getDisplacement() {
        return displacement;
    }

    public void setDisplacement(int displacement) {
        this.displacement = displacement;
    }

    public String getEfficiency() {
        return efficiency;
    }

    public void setEfficiency(String efficiency) {
        this.efficiency = efficiency;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append(this.model + ":").append(System.lineSeparator())
                .append("Power: " + this.power).append(System.lineSeparator())
                .append(String.format("Displacement: %s", this.displacement == 0 ? "n/a" : this.displacement)).append(System.lineSeparator())
                .append(String.format("Efficiency: %s", this.efficiency == null ? "n/a" : this.efficiency));

        return builder.toString();
    }
}
