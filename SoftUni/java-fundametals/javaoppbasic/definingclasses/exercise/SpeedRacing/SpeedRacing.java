package javaoppbasic.definingclasses.exercise.SpeedRacing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class SpeedRacing {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Map<String, Car> cars = new LinkedHashMap<>();
        String input;
        int n = Integer.parseInt(reader.readLine());

        while (n-- > 0) {
            input = reader.readLine();
            String[] tokens = input.split("\\s+");
            String model = tokens[0];
            double fuel = Double.parseDouble(tokens[1]);
            double cost = Double.parseDouble(tokens[2]);

            Car car = new Car(model, fuel, cost);
            cars.put(model, car);
        }

        while (true) {
            if ("End".equals(input = reader.readLine()))
                break;

            String[] tokens = input.split("\\s+");
            String model = tokens[1];
            int amountKm = Integer.parseInt(tokens[2]);

            if (cars.containsKey(model)) {
                Car car = cars.get(model);
                car.canCarGetTheDistance(amountKm);
            }
        }

        cars.entrySet().stream()
                .forEach(car -> System.out.println(String.format("%s %.2f %d", car.getValue().getModel(), car.getValue().getFuel(), car.getValue().getDistance())));

    }
}

class Car {
    private String model;
    private double fuel;
    private double cost;
    private int distance;

    public Car() {
    }

    public Car(String model, double fuel, double cost) {
        this.model = model;
        this.fuel = fuel;
        this.cost = cost;
        this.distance = 0;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getFuel() {
        return fuel;
    }

    public void setFuel(double fuel) {
        this.fuel = fuel;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public void canCarGetTheDistance(int km) {
        if (km * this.cost <= this.fuel) {
            this.fuel -= km * this.cost;
            this.distance += km;
        } else {
            System.out.println("Insufficient fuel for the drive");
        }
    }
}
