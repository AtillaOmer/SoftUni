package javaoppbasic.definingclasses.exercise.CatLady;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class CatLady {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Map<String, Cat> cats = new HashMap<>();

        String input;

        while (true) {

            if ("End".equals(input = reader.readLine()))
                break;

            String[] tokens = input.split("\\s+");
            String breed = tokens[0];
            String name = tokens[1];


            Siames siames;
            Cymric cymric;
            StreetExtraordinaire streetExtraordinaire;

            switch (breed) {
                case "Siamese":
                    double earSize = Double.parseDouble(tokens[2]);
                    siames = new Siames(name, breed, earSize);
                    cats.put(name, siames);
                    break;
                case "Cymric":
                    double furLength = Double.parseDouble(tokens[2]);
                    cymric = new Cymric(name, breed, furLength);
                    cats.put(name, cymric);
                    break;
                case "StreetExtraordinaire":
                    double decibelsOfMeows = Double.parseDouble(tokens[2]);
                    streetExtraordinaire = new StreetExtraordinaire(name, breed, decibelsOfMeows);
                    cats.put(name, streetExtraordinaire);
                    break;
            }
        }

        String catName = reader.readLine();
        if (cats.containsKey(catName)) {
            Cat cat = cats.get(catName);
            System.out.println(cat.toString());
        }
    }
}

class Cat {
    private String name;
    private String breed;

    public Cat() {
    }

    public Cat(String name, String breed) {
        this.name = name;
        this.breed = breed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }
}

class Siames extends Cat {
    private double earSize;

    public Siames() {
    }

    public Siames(String name, String breed, double earSize) {
        super(name, breed);
        this.earSize = earSize;
    }

    public double getEarSize() {
        return earSize;
    }

    public void setEarSize(double earSize) {
        this.earSize = earSize;
    }

    @Override
    public String toString() {
        return String.format("%s %s %.2f", this.getBreed(), this.getName(), this.earSize);
    }
}

class Cymric extends Cat {
    private double furLength;

    public Cymric() {
    }

    public Cymric(String name, String breed, double furLength) {
        super(name, breed);
        this.furLength = furLength;
    }

    public double getFurLength() {
        return furLength;
    }

    public void setFurLength(double furLength) {
        this.furLength = furLength;
    }

    @Override
    public String toString() {
        return String.format("%s %s %.2f", this.getBreed(), this.getName(), this.getFurLength());
    }
}

class StreetExtraordinaire extends Cat {
    private double decibelsOfMeows;

    public StreetExtraordinaire() {
    }

    public StreetExtraordinaire(String name, String breed, double decibelsOfMeows) {
        super(name, breed);
        this.decibelsOfMeows = decibelsOfMeows;
    }

    public double getDecibelsOfMeows() {
        return decibelsOfMeows;
    }

    public void setDecibelsOfMeows(double decibelsOfMeows) {
        this.decibelsOfMeows = decibelsOfMeows;
    }

    @Override
    public String toString() {
        return String.format("%s %s %.2f", this.getBreed(), this.getName(), this.getDecibelsOfMeows());
    }
}
