package javaoppbasic.definingclasses.exercise.Google;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Google {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Map<String, Person> persons = new LinkedHashMap<>();
        String input;

        while (true) {
            if ("End".equals(input = reader.readLine()))
                break;

            String[] tokens = input.split("\\s+");
            String name = tokens[0];
            String type = tokens[1];

            persons.putIfAbsent(name, new Person(name));
            Person person = persons.get(name);

            switch (type) {
                case "company":
                    String companyName = tokens[2];
                    String depaartment = tokens[3];
                    Double salary = Double.parseDouble(tokens[4]);

                    Company company = new Company(companyName, depaartment, salary);
                    person.setCompany(company);
                    break;
                case "pokemon":
                    String pokemonName = tokens[2];
                    String pokemonType = tokens[3];

                    Pokemon pokemon = new Pokemon(pokemonName, pokemonType);
                    person.getPokemons().add(pokemon);
                    break;
                case "parents":
                    String parentName = tokens[2];
                    String parentBirtdhay = tokens[3];

                    Parent parent = new Parent(parentName, parentBirtdhay);
                    person.getParents().add(parent);
                    break;
                case "children":
                    String childName = tokens[2];
                    String childBirtdhay = tokens[3];

                    Child child = new Child(childName, childBirtdhay);
                    person.getChildren().add(child);
                    break;
                case "car":
                    String model = tokens[2];
                    String speed = tokens[3];

                    Car car = new Car(model, speed);
                    person.setCar(car);
                    break;
            }

        }

        String searchedName = reader.readLine();
        if (persons.containsKey(searchedName))
            System.out.println(persons.get(searchedName));
    }
}

class Person {
    private String name;
    private Company company;
    private List<Pokemon> pokemons;
    private List<Parent> parents;
    private List<Child> children;
    private Car car;

    public Person() {
    }

    public Person(String name) {
        this.name = name;
        pokemons = new ArrayList<>();
        parents = new ArrayList<>();
        children = new ArrayList<>();
    }

    public Person(Company company, List<Pokemon> pokemons, List<Parent> parents, List<Child> children, Car car) {
        this.company = company;
        this.pokemons = pokemons;
        this.parents = parents;
        this.children = children;
        this.car = car;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public List<Pokemon> getPokemons() {
        return pokemons;
    }

    public void setPokemons(List<Pokemon> pokemons) {
        this.pokemons = pokemons;
    }

    public List<Parent> getParents() {
        return parents;
    }

    public void setParents(List<Parent> parents) {
        this.parents = parents;
    }

    public List<Child> getChildren() {
        return children;
    }

    public void setChildren(List<Child> children) {
        this.children = children;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public void printPerson() {
        System.out.println(this.getName() + ":");
        System.out.println("Company:");

    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(this.name).append(System.lineSeparator())
                .append(this.company != null ? this.company : "Company:").append(System.lineSeparator())
                .append(this.car != null ? this.car : "Car:").append(System.lineSeparator());


        stringBuilder.append("Pokemon:").append(System.lineSeparator());
        for (Pokemon pokemon : pokemons) {
            stringBuilder.append(pokemon).append(System.lineSeparator());
        }

        stringBuilder.append("Parents:").append(System.lineSeparator());
        for (Parent parent : parents) {
            stringBuilder.append(parent).append(System.lineSeparator());
        }

        stringBuilder.append("Children:").append(System.lineSeparator());
        for (Child child : children) {
            stringBuilder.append(child).append(System.lineSeparator());
        }


        return stringBuilder.toString();
    }
}

class Company {
    private String name;
    private String departyment;
    private double salary;

    public Company() {
    }

    public Company(String name, String departyment, double salary) {
        this.name = name;
        this.departyment = departyment;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepartyment() {
        return departyment;
    }

    public void setDepartyment(String departyment) {
        this.departyment = departyment;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("Company:").append(System.lineSeparator())
                .append(String.format("%s %s %.2f", this.name, this.departyment, this.salary));

        return stringBuilder.toString();
    }
}

class Pokemon {
    private String name;
    private String type;

    public Pokemon() {
    }

    public Pokemon(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return String.format("%s %s", this.name, this.type);
    }
}

class Parent {
    private String name;
    private String birthday;

    public Parent() {
    }

    public Parent(String name, String birthday) {
        this.name = name;
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    @Override
    public String toString() {
        return String.format("%s %s", this.name, this.birthday);
    }
}

class Child {
    private String name;
    private String birthDay;

    public Child() {
    }

    public Child(String name, String birthDay) {
        this.name = name;
        this.birthDay = birthDay;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    @Override
    public String toString() {
        return String.format("%s %s", this.name, this.birthDay);
    }
}

class Car {
    private String model;
    private String speed;

    public Car() {
    }

    public Car(String model, String speed) {
        this.model = model;
        this.speed = speed;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Car:").append(System.lineSeparator())
                .append(this.model + " " + this.speed);

        return sb.toString();
    }
}
