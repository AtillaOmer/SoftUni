package javaoppbasic.encapsulation.lab.SalaryIncrease;

import java.text.DecimalFormat;

public class Person {
    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.0############################################");
    private String firstName;
    private String lastName;
    private Integer age;
    private Double salary;

    public Person() {
    }

    public Person(String firstName, String lastName, Integer age, Double salary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.salary = salary;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public void increaseSalary(double bonus) {
        if (this.age < 30) {
            this.salary += this.salary * (bonus / 200);
        } else {
            this.salary += this.salary * (bonus / 100);
        }
    }

    @Override
    public String toString() {
        return String.format("%s %s gets %s leva", this.firstName, this.lastName, DECIMAL_FORMAT.format(this.salary));
    }
}
