package javaoppbasic.encapsulation.exercise.PizzaCalories;

import java.util.List;

public class Pizza {
    private String name;
    private Dough dough;
    private List<Topping> toppings;

    public Pizza(String name, Dough dough, List<Topping> toppings) {
        setName(name);
        setDough(dough);
        setToppings(toppings);
    }

    public String getName() {
        return name;
    }

    void setName(String name) {
        if (name.trim().isEmpty() || name.length() > 15) {
            throw new IllegalArgumentException("Pizza name should be between 1 and 15 symbols.");
        }
        this.name = name;
    }

    private void setDough(Dough dough) {
        this.dough = dough;
    }

    public void setToppings(List<Topping> toppings) {
        if (toppings.size() < 0 || toppings.size() > 10) {
            throw new IllegalArgumentException("Number of toppings should be in range [0..10].");
        }
        this.toppings = toppings;
    }

    public Double getCalories() {
        Double pizzaCalories = 0d;
        pizzaCalories += this.dough.getCalories();
        for (Topping topping : this.toppings) {
            pizzaCalories += topping.getCalories();
        }

        return pizzaCalories;
    }
}
