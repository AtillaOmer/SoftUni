package javaoppbasic.encapsulation.exercise.PizzaCalories;

import java.util.*;

public class Topping {
    private static List<String> TOPPING_LIST = new ArrayList<>(Arrays.asList("MEAT", "VEGGIES", "CHEESE", "SAUCE"));
    private static Map<String, Double> TOPPING_CALORIES_MAP = setToppingCaloriesMap();

    private String topping;
    private Integer weight;

    public Topping(String toppingType, Integer weightInGrams) {
        setTopping(toppingType);
        setWeight(weightInGrams);
    }

    private void setTopping(String topping) {
        if (!TOPPING_LIST.contains(topping.toUpperCase())) {
            throw new IllegalArgumentException("Cannot place " + topping + " on top of your pizza.");
        }
        this.topping = topping;
    }

    private void setWeight(Integer weight) {
        if (weight == null || weight < 1 || weight > 50) {
            throw new IllegalArgumentException(this.topping + " weight should be in the range [1..50].");
        }
        this.weight = weight;
    }

    public static Map<String, Double> setToppingCaloriesMap() {
        Map<String, Double> map = new LinkedHashMap<>();
        map.put("MEAT", 1.2);
        map.put("VEGGIES", 0.8);
        map.put("SAUCE", 0.9);
        map.put("CHEESE", 1.1);

        return map;
    }

    public Double getCalories() {
        return (2*this.weight) * TOPPING_CALORIES_MAP.get(this.topping.toUpperCase());
    }
}
