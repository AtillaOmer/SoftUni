package javaoppbasic.encapsulation.exercise.PizzaCalories;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        try {
            String[] tokens = reader.readLine().split("\\s+");
            String pizzaName = tokens[1];
            Integer toppingNums = Integer.parseInt(tokens[2]);

            if (toppingNums < 0 || toppingNums > 10)
                throw new IllegalArgumentException("Number of toppings should be in range [0..10].");

            tokens = reader.readLine().split(" ");
            String flourType = tokens[1];
            String bakingTechnique = tokens[2];
            Integer doughWeight = Integer.parseInt(tokens[3]);
            Dough dough = new Dough(flourType, bakingTechnique, doughWeight);

            List<Topping> toppings = new ArrayList<>();
            while (toppingNums-- > 0) {
                tokens = reader.readLine().split(" ");
                String toppingName = tokens[1];
                Integer toppingWeight = Integer.parseInt(tokens[2]);

                Topping topping = new Topping(toppingName, toppingWeight);
                toppings.add(topping);
            }
            Pizza pizza = new Pizza(pizzaName, dough, toppings);
            System.out.println(String.format("%s - %.2f", pizza.getName(), pizza.getCalories()));

        } catch (IllegalArgumentException iae) {
            System.out.println(iae.getMessage());
        }
    }

}
