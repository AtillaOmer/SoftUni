package javaoppbasic.encapsulation.exercise.PizzaCalories;

import java.util.*;

public class Dough {
    private static List<String> DOUGH_LIST = new ArrayList<>(Arrays.asList("WHITE", "WHOLEGRAIN"));
    private static List<String> BAKING_TECHNIQUE_LIST = new ArrayList<>(Arrays.asList("CRISPY", "CHEWY", "HOMEMADE"));
    private static Map<String, Double> DOUGH_CALORIES_MAP = setDoughtCalorieMap();

    private String flourType;
    private String bakingTechnique;
    private Integer weight;

    public Dough(String flourType, String bakingTechnique, Integer weight) {
        setFlourType(flourType);
        setBakingTechnique(bakingTechnique);
        setWeight(weight);
    }

    public void setFlourType(String flourType) {
        if (flourType.equals(null) || !DOUGH_LIST.contains(flourType.toUpperCase())) {
            throw new IllegalArgumentException("Invalid type of dough.");
        }
        this.flourType = flourType;
    }

    public void setBakingTechnique(String bakingTechnique) {
        if (bakingTechnique.equals(null) || !BAKING_TECHNIQUE_LIST.contains(bakingTechnique.toUpperCase())) {
            throw new IllegalArgumentException("Invalid type of dough.");
        }
        this.bakingTechnique = bakingTechnique;
    }

    public void setWeight(Integer weight) {
        if (weight == null || weight < 1 || weight > 200) {
            throw new IllegalArgumentException("Dough weight should be in the range [1..200].");
        }
        this.weight = weight;
    }

    public static Map<String, Double> getDoughCaloriesMap() {
        return DOUGH_CALORIES_MAP;
    }

    private static Map<String, Double> setDoughtCalorieMap() {
        Map<String, Double> map = new LinkedHashMap<>();
        map.put("WHITE", 1.5);
        map.put("WHOLEGRAIN", 1.0);
        map.put("CRISPY", 0.9);
        map.put("CHEWY", 1.1);
        map.put("HOMEMADE", 1.0);

        return map;
    }

    public Double getCalories() {
        return (2 * this.weight) * DOUGH_CALORIES_MAP.get(this.flourType.toUpperCase()) * DOUGH_CALORIES_MAP.get(this.bakingTechnique.toUpperCase());
    }
}
