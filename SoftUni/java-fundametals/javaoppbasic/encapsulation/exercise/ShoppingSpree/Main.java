package javaoppbasic.encapsulation.exercise.ShoppingSpree;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Map<String, Person> persons = new LinkedHashMap<>();
        Map<String, Product> products = new LinkedHashMap<>();
        String[] InputPersons = reader.readLine().split(";");
        String[] inputProducts = reader.readLine().split(";");

        try {

            for (String inputPerson : InputPersons) {
                String[] tokens = inputPerson.split("=");
                persons.put(tokens[0], new Person(tokens[0], Double.parseDouble(tokens[1])));
            }

            for (String inputProduct : inputProducts) {
                String[] tokens = inputProduct.split("=");
                products.put(tokens[0], new Product(tokens[0], Double.parseDouble(tokens[1])));
            }

            String input;
            while (true) {
                input = reader.readLine();
                if ("END".equals(input)) {
                    break;
                }

                String[] tokens = input.split(" ");
                String personName = tokens[0];
                String productName = tokens[1];
                Person person = persons.get(personName);
                Product product = products.get(productName);
                if (person.getMoney() >= product.getCost()) {
                    person.addProduct(product);
                    System.out.println(String.format("%s bought %s", person.getName(), product.getName()));
                }
                else
                    System.out.println(String.format("%s can't afford %s", person.getName(), product.getName()));
            }

            persons.entrySet()
                    .forEach(person -> {
                        System.out.println(String.format("%s - %s", person.getValue().getName(), person.getValue().getProducts()));
                    });

        } catch (IllegalArgumentException iae) {
            System.out.println(iae.getMessage());
        }
    }
}
