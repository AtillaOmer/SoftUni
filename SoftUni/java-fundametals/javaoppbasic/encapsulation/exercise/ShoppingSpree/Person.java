package javaoppbasic.encapsulation.exercise.ShoppingSpree;

import java.util.ArrayList;
import java.util.List;

public class Person {
    private String name;
    private Double money;
    private List<Product> bag;

    public Person(String name, Double money) {
        setName(name);
        setMoney(money);
        bag = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        if (name.equals(null) || name.trim().isEmpty()) {
            throw new IllegalArgumentException("Name cannot be empty");
        }
        this.name = name;
    }

    public Double getMoney() {
        return money;
    }

    private void setMoney(Double money) {
        if (money < 0) {
            throw new IllegalArgumentException("Money cannot be negative");
        }
        this.money = money;
    }

    private void setBagOfProducts(List<Product> bagOfProducts) {
        this.bag = bagOfProducts;
    }


    public void addProduct(Product product) {
        this.bag.add(product);
        this.money -= product.getCost();
    }

    public String getProducts() {
        if (this.bag.isEmpty()){
            return String.format("Nothing bought");
        } else {
            return String.format(this.bag.toString().replaceAll("[\\[\\]]", ""));
        }
    }
}
