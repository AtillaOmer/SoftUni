package javaoppbasic.encapsulation.exercise.FootballTeamGenerator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String inputLine;
        Map<String, Team> teams = new HashMap<>();

        while (true) {
            if ("END".equalsIgnoreCase(inputLine = reader.readLine()))
                break;

            String[] tokens = inputLine.split(";");

            String command = tokens[0];
            String teamName = tokens[1];
            try {
                switch (command.toUpperCase()) {
                    case "TEAM":
                        teams.put(teamName, new Team(teamName));
                        break;
                    case "ADD":
                        String playerName = tokens[2];
                        Integer playerEndurance = Integer.parseInt(tokens[3]);
                        Integer playerSprint = Integer.parseInt(tokens[4]);
                        Integer playerDribble = Integer.parseInt(tokens[5]);
                        Integer playerPassing = Integer.parseInt(tokens[6]);
                        Integer playerShooting = Integer.parseInt(tokens[7]);

                        if (teams.containsKey(teamName)) {
                            teams.get(teamName).addPlayer(new Player(playerName, playerEndurance, playerSprint, playerDribble, playerPassing, playerShooting));
                        } else {
                            throw new IllegalArgumentException("Team " + teamName + " not exist.");
                        }
                        break;
                    case "REMOVE":
                        playerName = tokens[2];
                        if (teams.containsKey(teamName)) {
                            teams.get(teamName).removePlayer(playerName);
                        } else {
                            throw new IllegalArgumentException("Team " + teamName + " not exist.");
                        }
                        break;
                    case "RATING":
                        if (teams.containsKey(teamName)) {
                            System.out.println(teams.get(teamName));
                        } else {
                            throw new IllegalArgumentException("Team " + teamName + " not exist.");
                        }
                        break;
                    default:
                        break;
                }
            } catch (IllegalArgumentException iae) {
                System.out.println(iae.getMessage());
            }
        }
    }
}
