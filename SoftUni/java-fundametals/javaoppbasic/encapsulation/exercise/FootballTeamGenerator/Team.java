package javaoppbasic.encapsulation.exercise.FootballTeamGenerator;

import java.util.*;

public class Team {
    private List<Player> players;
    private String name;
    private double rating;

    public Team(String name) {
        this.players = new ArrayList<>();
        setName(name);
        this.rating = 0;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        if (name.trim().isEmpty() || name == null) {
            throw new IllegalArgumentException("A name should not be empty.");
        }
        this.name = name;
    }

    public double getRating() {
        if (this.rating > 0)
            return rating / this.players.size();
        else
            return 0;
    }

    public void addPlayer(Player player) {
        this.players.add(player);
        rating += (player.getEndurance() + player.getSprint() + player.getDribble() + player.getPassing() + player.getShooting()) / 5d;
    }

    public void removePlayer(String playerName) {
        Optional<Player> player = players.stream()
                .filter(p -> p.getName().equalsIgnoreCase(playerName))
                .findFirst();

        if (player.isPresent()) {
            for (Iterator<Player> playerrList = players.iterator(); playerrList.hasNext(); ) {
                Player p = playerrList.next();
                if (p.getName().equals(playerName)) {
                    rating -= (player.get().getEndurance() + player.get().getSprint() + player.get().getDribble() + player.get().getPassing() + player.get().getShooting()) / 5d;
                    playerrList.remove();
                }
            }
        } else {
            throw new IllegalArgumentException("Player " + playerName + " is not in " + this.name + " team.");
        }
    }

    @Override
    public String toString() {
        return String.format("%s - %d", this.name, Math.round(this.getRating()));
    }
}
