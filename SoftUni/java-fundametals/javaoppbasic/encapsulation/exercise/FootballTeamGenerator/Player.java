package javaoppbasic.encapsulation.exercise.FootballTeamGenerator;

public class Player {
    private String name;
    private Integer endurance;
    private Integer sprint;
    private Integer dribble;
    private Integer passing;
    private Integer shooting;

    public Player(String name, Integer endurance, Integer sprint, Integer dribble, Integer passing, Integer shooting) {
        setName(name);
        setEndurance(endurance);
        setSprint(sprint);
        setDribble(dribble);
        setPassing(passing);
        setShooting(shooting);
    }

    private void setName(String name) {
        if (name.trim().isEmpty() || name == null)
            throw new IllegalArgumentException("A name should not be empty.");
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Integer getEndurance() {
        return endurance;
    }

    private void setEndurance(Integer endurance) {
        if (endurance < 0 || endurance > 100 || endurance == null) {
            throw new IllegalArgumentException("Endurance should be between 0 and 100.");
        }
        this.endurance = endurance;
    }

    public Integer getSprint() {
        return sprint;
    }

    private void setSprint(Integer sprint) {
        if (sprint < 0 || sprint > 100 || sprint == null) {
            throw new IllegalArgumentException("Sprint should be between 0 and 100.");
        }
        this.sprint = sprint;
    }

    public Integer getDribble() {
        return dribble;
    }

    private void setDribble(Integer dribble) {
        if (dribble < 0 || dribble > 100 || dribble == null) {
            throw new IllegalArgumentException("Dribble should be between 0 and 100.");
        }
        this.dribble = dribble;
    }

    public Integer getPassing() {
        return passing;
    }

    private void setPassing(Integer passing) {
        if (passing < 0 || passing > 100 || passing == null) {
            throw new IllegalArgumentException("Passing should be between 0 and 100.");
        }
        this.passing = passing;
    }

    public Integer getShooting() {
        return shooting;
    }

    private void setShooting(Integer shooting) {
        if (shooting < 0 || shooting > 100 || shooting == null) {
            throw new IllegalArgumentException("Shooting should be between 0 and 100.");
        }
        this.shooting = shooting;
    }
}