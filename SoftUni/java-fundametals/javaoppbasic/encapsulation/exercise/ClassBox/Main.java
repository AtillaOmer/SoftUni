package javaoppbasic.encapsulation.exercise.ClassBox;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        double length = Double.parseDouble(reader.readLine());
        double width = Double.parseDouble(reader.readLine());
        double height = Double.parseDouble(reader.readLine());

        Box box = new Box(length, width, height);

        System.out.println(String.format("Surface Area - %.2f", box.surfaceArea()));
        System.out.println(String.format("Lateral Surface Area - %.2f", box.lateralSurfaceArea()));
        System.out.println(String.format("Volume - %.2f", box.volume()));
    }
}

class Box {
    private double length;
    private double width;
    private double height;

    public Box() {
    }

    public Box(double length, double width, double height) {

        this.length = length;
        this.width = width;
        this.height = height;
    }

    public double surfaceArea() {
        //        Surface Area = 2lw + 2lh + 2wh

        return 2 * this.length * this.width + 2 * this.length * this.height + 2 * this.width * this.height;
    }

    public double lateralSurfaceArea() {
        //        Lateral Surface Area = 2lh + 2wh

        return 2 * this.length * this.height + 2 * this.width * this.height;
    }

    public double volume() {
        //        Volume = lwh

        return this.length * this.width * this.height;
    }

}
