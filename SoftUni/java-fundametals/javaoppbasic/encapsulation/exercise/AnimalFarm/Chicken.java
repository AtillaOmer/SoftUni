package javaoppbasic.encapsulation.exercise.AnimalFarm;

import java.text.DecimalFormat;

public class Chicken {
    private String name;
    private Integer age;

    public Chicken(String name, Integer age) {
        setName(name);
        setAge(age);
    }

    private void setName(String name) {
        if (name.isEmpty() || name.equals(" ") || name.equals(null)) {
            throw new IllegalArgumentException("Name cannot be empty.");
        }
        this.name = name;
    }

    private void setAge(Integer age) {
        if (age < 0 || age > 15) {
            throw new IllegalArgumentException("Age should be between 0 and 15.");
        }
        this.age = age;
    }

    private double calculateProductPerDay() {
        if (this.age < 6) {
            return 2;
        } else if (this.age < 11) {
            return 1;
        } else {
            return 0.75;
        }
    }

    public double productPerDay() {
        return calculateProductPerDay();
    }

    @Override
    public String toString() {
        DecimalFormat df = new DecimalFormat("#.###################################################");
        return String.format("Chicken %s (age %d) can produce %s eggs per day.", this.name, this.age, df.format(this.productPerDay()));
    }
}
