package javaadvanced.exampreparationI;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class FootballStats {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Map<String, TreeMap<String, String>> states = new LinkedHashMap<>();

        String input;
        while (!"Season End".equalsIgnoreCase(input = reader.readLine())) {
            String[] tokens = input.split("\\s+");
            String firstTeam = tokens[0];
            String secondTeam = tokens[2];
            String result = tokens[4];

            if (!states.containsKey(firstTeam)) {
                TreeMap<String, String> match = new TreeMap<>();
                if (!match.containsKey(secondTeam)) {
                    match.put(secondTeam, result);
                }

                states.put(firstTeam, match);
            } else {
                states.get(firstTeam).put(secondTeam, result);
            }

            if (!states.containsKey(secondTeam)) {
                TreeMap<String, String> match = new TreeMap<>();
                if (!match.containsKey(firstTeam)) {
                    match.put(firstTeam, new StringBuilder(result).reverse().toString());
                }

                states.put(secondTeam, match);
            } else {
                states.get(secondTeam).put(firstTeam, new StringBuilder(result).reverse().toString());
            }
        }

//        Map<String, TreeMap<String, String>> sortedMap = states.entrySet()
//                .stream()
//                .sorted((a,b) -> {
//                    return states.get(a).values().iterator().next().compareTo(states.get(b).values().iterator().next());
//                }).collect(Collectors.toMap())
        for (Map.Entry<String, TreeMap<String, String>> stateEntrySet : states.entrySet()) {
            for (Map.Entry<String, String> s : stateEntrySet.getValue().entrySet()) {
                System.out.println(String.format("%s - %s -> %s", stateEntrySet.getKey(), s.getKey(), s.getValue()));
            }
        }
    }
}
