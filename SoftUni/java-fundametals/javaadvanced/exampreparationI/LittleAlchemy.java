package javaadvanced.exampreparationI;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LittleAlchemy {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String[] input = reader.readLine().split("\\s+");
        Pattern pattern = Pattern.compile("(\\w+\\s\\w+)\\s(\\d+)");

        ArrayDeque <Integer> stonesQueue = new ArrayDeque<>();
        ArrayDeque<Integer> storageStack = new ArrayDeque<>();

        for (String s : input) {
            stonesQueue.add(Integer.parseInt(s));
        }

        String commandLine;
        while (!"Revision".equals(commandLine = reader.readLine())) {

            Matcher matcher = pattern.matcher(commandLine);
            matcher.find();

            String command = matcher.group(1);
            int n = Integer.parseInt(matcher.group(2));

            if ("Apply acid".equals(command)) {
                for (int i = 0; i < n; i++) {
                    if (!stonesQueue.isEmpty()) {
                        int stone = stonesQueue.remove();
                        stone --;
                        if (stone == 0) {
                            storageStack.push(stone);
                        } else {
                            stonesQueue.add(stone);
                        }
                    }
                }
            } else if ("Air leak".equals(command)) {
                if (!storageStack.isEmpty()) {
                    int gold = storageStack.pop();
                    gold = n;
                    stonesQueue.add(gold);
                }
            }
        }

        System.out.println(stonesQueue.toString().replaceAll("[,\\[\\]]", ""));
        System.out.println(storageStack.size());
    }
}
