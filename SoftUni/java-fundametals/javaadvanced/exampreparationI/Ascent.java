package javaadvanced.exampreparationI;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Ascent {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String regex = "(\\,|\\_)([a-zA-Z]+)(\\d)";
        Pattern pattern = Pattern.compile(regex);

        Map<String, String> encryptedDictionary = new LinkedHashMap<>();

        String input = "";
        while (!"Ascend".equalsIgnoreCase(input)) {

            for (Map.Entry<String, String> stringStringEntry : encryptedDictionary.entrySet()) {
                input = input.replaceAll(stringStringEntry.getKey(), stringStringEntry.getValue());
            }

            Matcher matcher = pattern.matcher(input);

            while (matcher.find()) {

                String word = matcher.group(0);
                String message = matcher.group(2);
                String firstChar = matcher.group(1);
                int digit = Integer.parseInt(matcher.group(3));


                StringBuilder result = new StringBuilder();


                if (!encryptedDictionary.containsKey(word)) {
                    for (char c : message.toCharArray()) {
                        if (firstChar.equalsIgnoreCase(",")) {
                            char ch = (char) (c + (char) digit);
                            result.append(ch);
                        } else if (firstChar.equalsIgnoreCase("_")) {
                            char ch = (char) (c - (char) digit);
                            result.append(ch);
                        }
                    }

                    encryptedDictionary.put(word, result.toString());
                } else {
                    result.append(encryptedDictionary.get(word));
                }

                input = input.replaceAll(word, result.toString());
            }

            System.out.println(input);
            input = reader.readLine();
        }
    }
}
