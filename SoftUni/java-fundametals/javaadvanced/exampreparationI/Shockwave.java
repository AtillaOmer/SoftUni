package javaadvanced.exampreparationI;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Shockwave {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int[] size = Arrays.stream(reader.readLine().split("\\s+")).mapToInt(e -> Integer.parseInt(e)).toArray();
        int[][] array = new int[size[0]][size[1]];

        for (int i = 0; i < size[0]; i++) {
            for (int j = 0; j < size[1]; j++) {
                array[i][j] = 0;
            }
        }

        String inputLine;
        while (!"Here We Go".equals(inputLine = reader.readLine())) {

            int[] shockwaves = Arrays.stream(inputLine.split("\\s+")).mapToInt(Integer::parseInt).toArray();

            int x1 = shockwaves[0];
            int y1 = shockwaves[1];
            int x2 = shockwaves[2];
            int y2 = shockwaves[3];

            for (int i = x1; i <= x2; i++) {
                for (int j = y1; j <= y2; j++) {
                        array[i][j] ++;
                }
            }
        }

        for (int i = 0; i < size[0]; i++) {
            for (int j = 0; j < size[1]; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }
}
