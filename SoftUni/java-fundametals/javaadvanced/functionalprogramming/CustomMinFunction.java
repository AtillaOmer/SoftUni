package javaadvanced.functionalprogramming;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.function.Function;

public class CustomMinFunction {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		int[] numbers = Arrays.stream(reader.readLine().split("\\s+")).mapToInt(e -> Integer.parseInt(e)).toArray();

		Function<int[], Integer> minFunc = (arr) -> {
			int min = Integer.MAX_VALUE;
			for (Integer num : arr) {
				if (num < min) {
					min = num;
				}
			}
			return min;
		};

		System.out.println(minFunc.apply(numbers));

	}

}
