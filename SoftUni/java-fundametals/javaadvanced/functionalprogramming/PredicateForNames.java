package javaadvanced.functionalprogramming;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.function.BiConsumer;
import java.util.function.BiPredicate;

public class PredicateForNames {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		int length = Integer.parseInt(reader.readLine());
		String[] names = reader.readLine().split("\\s+");

		BiPredicate<String, Integer> checkLength = (name, wordLength) -> {

			return name.length() <= wordLength;

		};

		BiConsumer<String[], Integer> printer = (arr, wordLength) -> {
			for (String name : arr) {
				if (checkLength.test(name, wordLength)) {
					System.out.println(name);
				}
			}
		};

		printer.accept(names, length);

	}
}
