package javaadvanced.functionalprogramming;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class AppliedArithmetics {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		List<Integer> numbers = Arrays.stream(reader.readLine().split("\\s+")).map(Integer::parseInt)
				.collect(Collectors.toList());

		String command;
		Consumer<String> consumer = operation -> {
			switch (operation) {
			case "add":
				applyCommand(numbers, "add");
				break;
			case "multiply":
				applyCommand(numbers, "multiply");
				break;
			case "subtract":
				applyCommand(numbers, "subtract");
				break;
			default:
				applyCommand(numbers, "print");
				break;
			}
		};

		while (!"end".equals(command = reader.readLine())) {

			consumer.accept(command);
		}
	}

	private static void applyCommand(List<Integer> list, String command) {

		switch (command) {
		case "add":
			for (int i = 0; i < list.size(); i++) {
				list.set(i, list.get(i) + 1);
			}
			break;
		case "multiply":
			for (int i = 0; i < list.size(); i++) {
				list.set(i, list.get(i) * 2);
			}
			break;
		case "subtract":
			for (int i = 0; i < list.size(); i++) {
				list.set(i, list.get(i) - 1);
			}
			break;
		default:
			for (int i : list) {
				System.out.print(i + " ");
			}
			System.out.println();
		}

	}

}
