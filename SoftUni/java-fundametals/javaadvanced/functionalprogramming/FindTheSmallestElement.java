package javaadvanced.functionalprogramming;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class FindTheSmallestElement {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		List<Integer> numbers = Arrays.stream(reader.readLine().split("\\s+")).map(e -> Integer.valueOf(e))
				.collect(Collectors.toList());

		Function<List<Integer>, Integer> findSmallestNum = list -> {
			int index = -1;
			int min = Integer.MAX_VALUE;
			for (Integer num : list) {
				if (num <= min) {
					min = num;
					index = list.lastIndexOf(num);
				}
			}
			return index;
		};

		System.out.println(findSmallestNum.apply(numbers));
	}

}
