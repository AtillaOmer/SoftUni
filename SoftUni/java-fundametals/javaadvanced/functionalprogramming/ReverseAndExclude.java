package javaadvanced.functionalprogramming;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.function.Function;

public class ReverseAndExclude {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		int[] numbers = Arrays.stream(reader.readLine().split("\\s+")).mapToInt(e -> Integer.parseInt(e)).toArray();
		int n = Integer.parseInt(reader.readLine());

		StringBuilder sb = new StringBuilder();

		Function<int[], StringBuilder> reverser = arr -> {
			StringBuilder temp = new StringBuilder();
			for (int i = arr.length - 1; i >= 0; i--) {
				if (arr[i] % n != 0)
					temp.append(arr[i]).append(" ");
			}
			return temp;
		};

		sb = reverser.apply(numbers);

		System.out.println(sb.toString().trim());
	}

}
