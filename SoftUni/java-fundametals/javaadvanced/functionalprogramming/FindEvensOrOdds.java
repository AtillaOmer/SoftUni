package javaadvanced.functionalprogramming;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.function.Predicate;

public class FindEvensOrOdds {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		int[] bounds = Arrays.stream(reader.readLine().split("\\s+")).mapToInt(e -> Integer.valueOf(e)).toArray();
		String command = reader.readLine();

		Predicate<Integer> evenOrOdd = num -> checkNumber(num, command);

		for (int i = bounds[0]; i <= bounds[1]; i++) {
			if (evenOrOdd.test(i)) {
				System.out.print(i + " ");
			}
		}
	}

	private static boolean checkNumber(Integer num, String command) {
		switch (command) {
		case "even":
			return num % 2 == 0;
		case "odd":
			return num % 2 != 0;
		}

		return true;
	}

}
