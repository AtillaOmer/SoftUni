package javaadvanced.functionalprogramming;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;

public class CustomComparator {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		String[] input = reader.readLine().split("\\s+");

		Integer[] numbers = new Integer[input.length];
		for (int i = 0; i < input.length; i++) {
			numbers[i] = Integer.parseInt(input[i]);
		}

		Arrays.sort(numbers, new Sorter());
		
		for (Integer num : numbers) {
			System.out.print(num + " ");
		}

	}

}

class Sorter implements Comparator<Integer> {

	// 1 2 3 4 5 6 | 2 4 6 1 3 5
	@Override
	public int compare(Integer a, Integer b) {

		if (a % 2 == 0 && b % 2 == 0) {
			return Integer.compare(a, b);
		} else if (a % 2 == 0 && b % 2 != 0) {
			return -1;
		} else if (a % 2 != 0 && b % 2 == 0) {
			return 0;
		} else if (a % 2 != 0 && b % 2 != 0) {
			return Integer.compare(a, b);
		}
		return 0;
	}

}
