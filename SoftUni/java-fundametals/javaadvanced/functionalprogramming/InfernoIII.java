package javaadvanced.functionalprogramming;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class InfernoIII {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		List<Integer> gems = Arrays.stream(reader.readLine().split("\\s+")).map(e -> Integer.parseInt(e))
				.collect(Collectors.toList());

		List<String> filters = new ArrayList<>();

		String input;

		while (!"Forge".equals(input = reader.readLine())) {
			String[] tokens = input.split(";");

			String filterName = tokens[0];
			String filterType = tokens[1];
			int num = Integer.parseInt(tokens[2]);

			if ("Exclude".equals(filterName)) {
				filters.add(filterType + ":" + num);
			} else if ("Reverse".equals(filterName)) {
				filters.remove(filterType + ":" + num);
			}
		}

		Collections.reverse(filters);
		exclude(gems, filters);

		System.out.println(String.join(" ", gems.toString().replaceAll("[\\[\\],]", "")));
	}

	static void exclude(List<Integer> gems, List<String> filters) {

		for (String filter : filters) {
			String[] tokens = filter.split(":");

			String direction = tokens[0];
			int value = Integer.parseInt(tokens[1]);

			List<Integer> indexesToDelete = new ArrayList<>();

			switch (direction) {
			case "Sum Left":
				if (gems.get(0) == value) {
					indexesToDelete.add(0);
				}
				for (int i = 1; i < gems.size(); i++) {
					int sum = gems.get(i) + gems.get(i - 1);

					if (value == sum) {
						indexesToDelete.add(i);
					}
				}
				break;
			case "Sum Right":
				if (gems.get(gems.size() - 1) == value) {
					indexesToDelete.add(gems.size() - 1);
				}
				for (int i = 0; i < gems.size() - 1; i++) {
					int sum = gems.get(i) + gems.get(i + 1);
					if (sum == value) {
						indexesToDelete.add(i);
					}
				}
				break;
			case "Sum Left Right":
				if (gems.size() > 2) {
					for (int i = 1; i < gems.size() - 1; i++) {
						int sum = gems.get(i - 1) + gems.get(i) + gems.get(i + 1);
						if (sum == value) {
							indexesToDelete.add(i);
						}
					}
				} else if (gems.size() == 2) {
					if (gems.get(0) + gems.get(1) == value) {
						indexesToDelete.add(0);
					}
				} else if (gems.size() == 1) {
					if (gems.get(0) == value) {
						indexesToDelete.add(0);
					}
				}

				if (gems.size() >= 2) {
					if (gems.get(0) + gems.get(1) == value) {
						indexesToDelete.add(0);
					}

					if (gems.get(gems.size() - 1) + gems.get(gems.size() - 2) == value) {
						indexesToDelete.add(gems.size() - 1);
					}
				}
				break;
			}

			indexesToDelete.sort((a, b) -> {
				return Integer.compare(b, a);
			});

			for (int i : indexesToDelete) {
				gems.remove(i);
			}
		}
	}
}
