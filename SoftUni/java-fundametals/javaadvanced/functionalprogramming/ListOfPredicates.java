package javaadvanced.functionalprogramming;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class ListOfPredicates {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		int n = Integer.parseInt(reader.readLine());
		int[] divisors = Arrays.stream(reader.readLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();

		StringBuilder sb = new StringBuilder();
		BiPredicate<Integer, Integer> isDivisible = (a, b) -> a % b == 0;
		Predicate<Integer> predicate = num -> checkNum(num, divisors, isDivisible);

		for (int i = 1; i <= n; i++) {
			if (predicate.test(i)) {
				sb.append(i + " ");
			}
		}

		System.out.println(sb.toString().trim());
	}

	private static boolean checkNum(Integer num, int[] divisors, BiPredicate<Integer, Integer> isDivisible) {
		boolean divisible = true;
		for (int n : divisors) {
			if (!isDivisible.test(num, n)) {
				return isDivisible.test(num, n);
			}
		}
		return divisible;
	}

}
