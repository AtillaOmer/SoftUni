package javaadvanced.functionalprogramming;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class ConsumerPrint {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		
		List<String> people = Arrays.stream(reader.readLine().split("\\s+")).map(e -> String.valueOf(e)).collect(Collectors.toList());
		
		Consumer<List<String>> printer = (param) -> {
			for (String person : param) {
				System.out.println(person);
			}
		};
		
		printer.accept(people);

	}

}
