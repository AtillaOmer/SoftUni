package javaadvanced.functionalprogramming;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.function.Consumer;

public class KnightsOfHonor {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		String[] people = reader.readLine().split("\\s+");

		Consumer<String[]> printer = (arr) -> {
			for (String person : arr) {
				System.out.println("Sir " + person);
			}
		};

		printer.accept(people);
	}

}
