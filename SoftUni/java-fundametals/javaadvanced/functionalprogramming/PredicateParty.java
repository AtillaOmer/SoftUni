package javaadvanced.functionalprogramming;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class PredicateParty {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		List<String> names = Arrays.stream(reader.readLine().split("\\s+")).collect(Collectors.toList());

		String command;
		Predicate<String> checkCommand = (inputCommand) -> "Remove".equals(inputCommand);

		while (!"Party!".equals(command = reader.readLine())) {

			String[] commandTokens = command.split("\\s+");
			String commandPart = commandTokens[0];
			String condition = commandTokens[1];
			String parameter = commandTokens[2];

			if (checkCommand.test(commandPart)) {
				if (condition.equals("StartsWith")) {
					names.removeIf(name -> name.startsWith(parameter));
				} else if (condition.equals("EndsWith")) {
					names.removeIf(name -> name.endsWith(parameter));
				} else if (condition.equals("Length")) {
					names.removeIf(name -> name.length() == Integer.parseInt(parameter));
				}

			} else {
				for (int i = 0; i < names.size(); i++) {
					if (condition.equals("StartsWith") && names.get(i).startsWith(parameter)) {
						names.add(i + 1, names.get(i));
						i++;
					} else if (condition.equals("EndsWith") && names.get(i).endsWith(parameter)) {
						names.add(i + 1, names.get(i));
						i++;
					} else if (condition.equals("Length") && names.get(i).length() == Integer.parseInt(parameter)) {
						names.add(i + 1, names.get(i));
						i++;
					}
				}
			}
		}

		if (names.size() == 0) {
			System.out.println("Nobody is going to the party!");
		} else {
			System.out.println(names.toString().replaceAll("[\\[\\]]", "") + " are going to the party!");
		}
	}

}
