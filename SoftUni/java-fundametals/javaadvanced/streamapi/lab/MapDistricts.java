package javaadvanced.streamapi.lab;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class MapDistricts {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		Map<String, List<Integer>> cities = new HashMap<>();
		List<String> tokens = Arrays.asList(reader.readLine().split("\\s+"));

		for (String token : tokens) {

			String[] tokenArgs = token.split(":");
			String city = tokenArgs[0];
			int district = Integer.parseInt(tokenArgs[1]);

			cities.putIfAbsent(city, new ArrayList<>());
			cities.get(city).add(district);
		}

		int bound = Integer.valueOf(reader.readLine());

		cities.entrySet().stream().filter(getFilterByPopulationPredicate(bound))
				.sorted(getSortByDescendingPopulationComparator()).forEach(getPrintMapEntryConsumer());

	}

	private static Consumer<Map.Entry<String, List<Integer>>> getPrintMapEntryConsumer() {
		return kv -> {
			System.out.print(kv.getKey() + ": ");
			kv.getValue().stream().sorted((s1, s2) -> Integer.compare(s2, s1)).limit(5)
					.forEach(dp -> System.out.print(dp + " "));
			System.out.println();
		};
	}

	private static Comparator<Entry<String, List<Integer>>> getSortByDescendingPopulationComparator() {
		return (kv1, kv2) -> Integer.compare(kv2.getValue().stream().mapToInt(Integer::valueOf).sum(),
				kv1.getValue().stream().mapToInt(Integer::valueOf).sum());
	}

	private static Predicate<? super Entry<String, List<Integer>>> getFilterByPopulationPredicate(int bound) {
		return kv -> kv.getValue().stream().mapToInt(Integer::valueOf).sum() > bound;
	}

}
