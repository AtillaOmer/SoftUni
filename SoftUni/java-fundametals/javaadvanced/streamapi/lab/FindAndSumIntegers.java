package javaadvanced.streamapi.lab;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class FindAndSumIntegers {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		int sum = Arrays.stream(reader.readLine().split("\\s+")).filter(n -> !n.isEmpty()).filter(n -> {
			if (n.length() > 1) {
				if (!(n.charAt(0) == '-') && !(n.charAt(0) == '+') && !Character.isDigit(n.charAt(0))) {
					return false;
				}
				for (int i = 1; i < n.length(); i++) {
					if (!Character.isDigit(n.charAt(i))) {
						return false;
					}
				}
				return true;
			} else {
				if (Character.isDigit(n.charAt(0))) {
					return true;
				}
				return false;
			}
		}).mapToInt(Integer::parseInt).sum();

		if (sum == 0) {
			System.out.println("No match");
		} else {
			System.out.println(sum);
		}
	}

}
