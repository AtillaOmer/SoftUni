package javaadvanced.streamapi.exercise;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StudentsByEnrollmentYear {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		Pattern pattern = Pattern.compile("^([0-9]+)\\s(.*)$");
		List<String> students = new ArrayList<>();
		String input;

		while (!"END".equals(input = reader.readLine())) {
			students.add(input);
		}

		students.stream().filter(s -> {
			Matcher matcher = pattern.matcher(s);
			matcher.find();
			String FN = matcher.group(1);

			return FN.endsWith("14") || FN.endsWith("15");
		}).forEach(s -> {
			Matcher matcher = pattern.matcher(s);
			matcher.find();
			String marks = matcher.group(2);

			System.out.println(marks);
		});

	}

}
