package javaadvanced.streamapi.exercise;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LittleJohn {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		String arrowTypePattern = "\\W{1,3}\\-{5}\\W{1,2}";

		String smallArrowPattern = ">{1}\\-{5}\\>{1}";
		String mediumArrowPattern = ">{2}\\-{5}\\>{1}";
		String largeArrowPattern = ">{3}\\-{5}\\>{2}";

		Pattern typeRegex = Pattern.compile(arrowTypePattern);
		Pattern smallRegex = Pattern.compile(smallArrowPattern);
		Pattern mediumRegex = Pattern.compile(mediumArrowPattern);
		Pattern largeRegex = Pattern.compile(largeArrowPattern);

		int largeArrowsCount = 0;
		int mediumArrowsCount = 0;
		int smallArrowsCount = 0;

		for (int i = 0; i < 4; i++) {
			String input = reader.readLine();

			Matcher matcher = typeRegex.matcher(input);

			while (matcher.find()) {
				String arrow = matcher.group(0);

				Matcher largeArrowMatch = largeRegex.matcher(arrow);

				if (largeArrowMatch.find()) {
					largeArrowsCount++;
				} else {
					Matcher mediumArrowMatch = mediumRegex.matcher(arrow);

					if (mediumArrowMatch.find()) {
						mediumArrowsCount++;
					} else {
						Matcher smallArrowMatch = smallRegex.matcher(arrow);

						if (smallArrowMatch.find()) {
							smallArrowsCount++;
						}
					}
				}
			}
		}

		int theNumbderInDec = 100 * smallArrowsCount + 10 * mediumArrowsCount + largeArrowsCount;

		String theNumberInBinary = Integer.toBinaryString(theNumbderInDec);

		StringBuilder sb = new StringBuilder();

		for (int i = theNumberInBinary.length() - 1; i >= 0; i--) {
			sb.append(theNumberInBinary.charAt(i));
		}

		String combinedNumber = theNumberInBinary + sb.toString();

		int combinedNumberInDec = Integer.parseInt(combinedNumber, 2);
		System.out.println(combinedNumberInDec);

	}
}