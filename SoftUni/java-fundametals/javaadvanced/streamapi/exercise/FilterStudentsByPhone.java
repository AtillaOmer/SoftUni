package javaadvanced.streamapi.exercise;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FilterStudentsByPhone {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		String input;
		List<String> students = new ArrayList<>();

		Pattern pattern = Pattern.compile("^(\\w+\\s\\w+)\\s([\\+\\d]\\d+)$");

		while (!"END".equals(input = reader.readLine())) {
			students.add(input);
		}

		students.stream().filter(x -> {
			Matcher matcher = pattern.matcher(x);
			matcher.find();
			String phoneNumber = matcher.group(2);

			return phoneNumber.startsWith("02") || phoneNumber.startsWith("+3592");
		}).forEach(x -> {
			Matcher matcher = pattern.matcher(x);
			matcher.find();
			String name = matcher.group(1);

			System.out.println(name);
		});

	}

}
