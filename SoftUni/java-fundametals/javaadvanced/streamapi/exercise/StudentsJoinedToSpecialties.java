package javaadvanced.streamapi.exercise;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StudentsJoinedToSpecialties {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		Pattern spPattern = Pattern.compile("^(.+?\\s+.+?)\\s+(.*)$");
		Pattern jPattern = Pattern.compile("^(\\w+\\s+\\w+)\\s+(.*)$");
		Pattern stPattern = Pattern.compile("^(.+?)\\s+(.*)$");

		List<Student> students = new ArrayList<>();
		List<StudentSpecialty> specialties = new ArrayList<>();
		List<String> joined = new ArrayList<>();
		String input;
		

		while (!"Students:".equals(input = reader.readLine())) {
			Matcher matcher = spPattern.matcher(input);
			matcher.find();

			String spec = matcher.group(1);
			String facNo = matcher.group(2);

			StudentSpecialty studSpec = new StudentSpecialty(spec, facNo);
			specialties.add(studSpec);
		} 
		
		while (!"END".equals(input = reader.readLine())) {
			Matcher matcher = stPattern.matcher(input);
			matcher.find();

			String facNo = matcher.group(1);
			String studName = matcher.group(2);

			Student student = new Student(studName, facNo);
			students.add(student);
		}
		
		try {
		specialties.forEach(spec -> students.forEach(stud -> {
			if (spec.getFacultyNumber().equals(stud.getFacultyNumber())) {
				joined.add(String.format("%s %s %s", stud.getStudentName(), stud.getFacultyNumber(),
						spec.getSpecialtyName()));
			}
		}));
		}catch (Exception e) {
			// TODO: handle exception
		}

		joined.sort((a, b) -> {
			Matcher matcherA = jPattern.matcher(a);
			Matcher matcherB = jPattern.matcher(b);

			matcherA.find();
			matcherB.find();

			String nameA = matcherA.group(1);
			String nameB = matcherB.group(1);

			return nameA.compareTo(nameB);

		});

		joined.forEach(stud -> System.out.println(stud));
	}

}

class Student {
	String studentName;
	String facultyNumber;

	public Student(String studentName, String facultyNumber) {
		super();
		this.studentName = studentName;
		this.facultyNumber = facultyNumber;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getFacultyNumber() {
		return facultyNumber;
	}

	public void setFacultyNumber(String facultyNumber) {
		this.facultyNumber = facultyNumber;
	}

}

class StudentSpecialty {
	String specialtyName;
	String facultyNumber;

	public StudentSpecialty(String specialtyName, String facultyNumber) {
		super();
		this.specialtyName = specialtyName;
		this.facultyNumber = facultyNumber;
	}

	public String getSpecialtyName() {
		return specialtyName;
	}

	public void setSpecialtyName(String specialtyName) {
		this.specialtyName = specialtyName;
	}

	public String getFacultyNumber() {
		return facultyNumber;
	}

	public void setFacultyNumber(String facultyNumber) {
		this.facultyNumber = facultyNumber;
	}

}
