package javaadvanced.streamapi.exercise;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class GroupByGroup {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		Pattern pattern = Pattern.compile("^([a-zA-Z]+\\s[a-zA-Z]+)\\s(\\d+)$");
		List<Person> students = new ArrayList<>();
		String input;

		while (!"END".equals(input = reader.readLine())) {

			Matcher matcher = pattern.matcher(input);
			matcher.find();

			String name = matcher.group(1);
			int group = Integer.valueOf(matcher.group(2));

			Person person = new Person(name, group);
			students.add(person);
		}

		students.stream().collect(Collectors.groupingBy(Person::getGroup)).forEach((k, v) -> {
			System.out.println(String.format("%d - %s", k, String.join(",", v.stream().map(p -> p.getName())
					.collect(Collectors.toList()).toString().replaceAll("[\\[\\]]", ""))));
		});
	}

}

class Person {
	String name;
	int group;

	public Person(String name, int group) {
		super();
		this.name = name;
		this.group = group;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getGroup() {
		return group;
	}

	public void setGroup(int group) {
		this.group = group;
	}

}
