package javaadvanced.streamapi.exercise;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WeakStudents {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		Pattern pattern = Pattern.compile("^([a-zA-Z]+\\s[a-zA-Z]+)\\s(.*)$");
		List<String> students = new ArrayList<>();
		String input;

		while (!"END".equals(input = reader.readLine())) {
			students.add(input);
		}

		students.stream().filter(x -> {
			Matcher matcher = pattern.matcher(x);
			matcher.find();

			return Arrays.stream(matcher.group(2).split("\\s+")).mapToInt(Integer::parseInt).filter(y -> y <= 3)
					.count() >= 2;
		}).forEach(s -> {
			Matcher matcher = pattern.matcher(s);
			matcher.find();
			String name = matcher.group(1);
			System.out.println(name);
		});

	}

}
