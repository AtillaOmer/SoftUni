package javaadvanced.streamapi.exercise;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExcellentStudents {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		Pattern pattern = Pattern.compile("^([a-zA-Z]+\\s[a-zA-Z]+)\\s(.*)$");
		List<String> students = new ArrayList<>();
		String input;

		while (!"END".equals(input = reader.readLine())) {
			students.add(input);
		}

		students.stream().filter(x -> x.contains("6")).forEach(x -> {
			Matcher matcher = pattern.matcher(x);
			matcher.find();
			String name = matcher.group(1);

			System.out.println(name);
		});

	}

}
