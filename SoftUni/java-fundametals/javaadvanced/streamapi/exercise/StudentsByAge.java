package javaadvanced.streamapi.exercise;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class StudentsByAge {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		String line;
		List<String> people = new ArrayList<>();

		while (!"END".equals(line = reader.readLine())) {
			people.add(line);
		}

		people.stream().filter(x -> {
			String[] tokens = x.split("\\s+");
			int age = Integer.parseInt(tokens[2]);
			if (18 <= age && age <= 24) {
				return true;
			} else {
				return false;
			}
		}).forEach(x -> System.out.printf("%s%n", x));

	}

}
