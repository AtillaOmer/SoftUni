package javaadvanced.streamapi.exercise;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class StudentsByGroup {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		List<String> names = new ArrayList<>();
		String line;

		while (!"END".equals(line = reader.readLine())) {
			names.add(line);
		}

		names.stream().filter(x -> {
			String[] tokens = x.split(" ");
			if (Integer.parseInt(tokens[2]) == 2) {
				return true;
			} else {
				return false;
			}
		}).sorted((x, y) -> {
			String[] tokens1 = x.split(" ");
			String[] tokens2 = y.split(" ");
			
			return tokens1[0].compareTo(tokens2[0]);
		}).forEach(x -> {
			String[] tokens = x.split(" ");
			String fName = tokens[0];
			String lName = tokens[1];
			System.out.printf("%s %s%n", fName, lName);
		});

	}

}
