package javaadvanced.streamapi.exercise;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class StudentsByFirstAndLastName {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		List<String> names = new ArrayList<>();
		String line;

		while (!"END".equals(line = reader.readLine())) {
			names.add(line);
		}

		names.stream().filter(x -> {
			String[] tokens = x.split("\\s+");

			int result = tokens[0].compareTo(tokens[1]);
			if (result < 0)
				return true;
			else
				return false;
		}).forEach(x -> System.out.printf("%s%n", x));
	}

}
