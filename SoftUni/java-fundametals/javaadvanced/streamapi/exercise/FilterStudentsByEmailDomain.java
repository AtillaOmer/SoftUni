package javaadvanced.streamapi.exercise;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class FilterStudentsByEmailDomain {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		List<String> students = new ArrayList<>();
		String line;

		while (!"END".equals(line = reader.readLine())) {
			students.add(line);
		}

		students.stream().filter(s -> s.endsWith("@gmail.com")).forEach(student -> {
			String[] tokens = student.split("\\s+");
			String fName = tokens[0];
			String lName = tokens[1];

			System.out.println(fName + " " + lName);
		});

	}

}
