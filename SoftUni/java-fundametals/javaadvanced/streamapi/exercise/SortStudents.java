package javaadvanced.streamapi.exercise;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class SortStudents {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		List<String> students = new ArrayList<>();

		String line;

		while (!"END".equals(line = reader.readLine())) {
			students.add(line);
		}

		students.stream().sorted((a, b) -> {
			String[] tokens1 = a.split("\\s+");
			String[] tokens2 = b.split("\\s+");

			String name1 = tokens1[1];
			String name2 = tokens2[1];

			return name1.compareTo(name2);
		}).sorted((a, b) -> {
			String[] tokens1 = a.split("\\s+");
			String[] tokens2 = b.split("\\s+");

			String fName1 = tokens1[0];
			String lName1 = tokens1[1];

			String fName2 = tokens2[0];
			String lName2 = tokens2[1];

			if (lName1.compareTo(lName2) == 0) {
				return fName2.compareTo(fName1);
			} else {
				return 0;
			}

		}).forEach(x -> System.out.printf("%s%n", x));

	}

}
