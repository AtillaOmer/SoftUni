package javaadvanced.practicalexam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Genome {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        List<Gene> individs = new ArrayList<>();
        Pattern pattern = Pattern.compile("([!@#$?\\w+]+)([=]\\d+)([-]{2}\\d+)([<]{2}\\w+)");
        String input;
        Map<String, Gene> individMap = new HashMap<>();

        while (!"Stop!".equals(input = reader.readLine())) {
            Matcher matcher = pattern.matcher(input);

            if (matcher.find()) {
                String name = matcher.group(1).replaceAll("[!@#$?]", "");
                int nameLength = Integer.parseInt(matcher.group(2).substring(1, matcher.group(2).length()));
                int geneCounts = Integer.parseInt(matcher.group(3).substring(2, matcher.group(3).length()));
                String organism = matcher.group(4).substring(2, matcher.group(4).length());

                if (name.length() == nameLength) {

                    if (individMap.containsKey(organism)) {
                        Gene gene = individMap.get(organism);
                        gene.setGeneCount(geneCounts);
                        individMap.put(organism, gene);
                       // individMap.get(name).setGeneCount(geneCounts);

                    } else {
                        Gene ind = new Gene(organism, name.substring(0, name.length() - 1), geneCounts);
                        individMap.put(organism, ind);
                    }
                }
            }
        }

        Map<String, Gene> sortedMap = individMap.entrySet()
                .stream()
                .sorted((a, b) -> {
                    return Integer.compare(b.getValue().getGeneCount(), a.getValue().getGeneCount());
                }).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (a, b) -> a, LinkedHashMap::new));

        for (Map.Entry<String, Gene> stringIndividEntry : sortedMap.entrySet()) {
            System.out.println(String.format("%s has genome size of %d", stringIndividEntry.getValue().getOrganizm()
                    , stringIndividEntry.getValue().getGeneCount()));
        }
    }

    private static class Gene {
        String organizm;
        String name;
        int geneCount;

        public Gene(String organizm, String name, int geneCount) {
            this.organizm = organizm;
            this.name = name;
            this.geneCount = geneCount;
        }

        public String getOrganizm() {
            return organizm;
        }

        public void setOrganizm(String organizm) {
            this.organizm = organizm;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getGeneCount() {
            return geneCount;
        }

        public void setGeneCount(int geneCount) {
            this.geneCount += geneCount;
        }
    }
}
