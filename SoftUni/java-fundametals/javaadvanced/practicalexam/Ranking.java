package javaadvanced.practicalexam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

public class Ranking {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String input;
        Map<String, String> contests = new LinkedHashMap<>();
        Map<String, Person> persons = new TreeMap<>();
        while (!"end of contests".equalsIgnoreCase(input = reader.readLine())) {
            String[] tokens = input.split(":");
            String contest = tokens[0];
            String pass = tokens[1];

            contests.put(contest, pass);
        }

        while (!"end of submissions".equals(input = reader.readLine())) {
            String[] tokens = input.split("=>");
            String contest = tokens[0];
            String pass = tokens[1];
            String person = tokens[2];
            int point = Integer.parseInt(tokens[3]);

            for (Map.Entry<String, String> con : contests.entrySet()) {
                if (con.getKey().equals(contest) && con.getValue().equals(pass)) {


                    if (!persons.containsKey(person)) {

                        Person per = new Person(person, point);
                        per.contests.put(contest, point);
                        persons.put(person, per);
                    } else {
                        Person per = persons.get(person);
                        if (per.contests.containsKey(contest)) {
                            if (point > per.contests.get(contest)) {
                                per.contests.put(contest, point);
                            }
                        } else {
                            per.contests.put(contest, point);
                        }

                        persons.put(person, per);

                    }
                }
            }
        }

        Person person1 = new Person("person", 0);
        for (Map.Entry<String, Person> personEntry : persons.entrySet()) {
            int maxPoints = Integer.MIN_VALUE;
            for (Map.Entry<String, Person> entry : persons.entrySet()) {
                if (entry.getValue().getAllPoints() > maxPoints) {
                    maxPoints = entry.getValue().getAllPoints();
                    person1 = entry.getValue();
                }
            }
        }

        System.out.println("Best candidate is " + person1.getName() + " with total " + person1.getAllPoints() + " points.");
        System.out.println("Ranking: ");
        for (Map.Entry<String, Person> personEntry : persons.entrySet()) {
            System.out.println(personEntry.getKey());
            Map<String, Integer> sortedPoints = personEntry.getValue().contests.entrySet().stream()
                    .sorted((a, b) -> {
                        return Integer.compare(b.getValue(), a.getValue());
                    }).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (a,b) -> a, LinkedHashMap::new));
            for (Map.Entry<String, Integer> con : sortedPoints.entrySet()) {
                System.out.println(String.format("#  %s -> %d", con.getKey(), con.getValue()));
            }
        }
    }

    private static class Person {
        String name;
        Map<String, Integer> contests = new LinkedHashMap<>();
        int points;

        public Person(String name, int points) {
            this.name = name;
            this.points = points;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Map<String, Integer> getContests() {
            return contests;
        }

        public void setContests(Map<String, Integer> contests) {
            this.contests = contests;
        }

        public int getPoints() {
            return points;
        }

        public void setPoints(int points) {
            this.points = points;
        }

        public int getAllPoints() {
            int sum = 0;
            for (Map.Entry<String, Integer> con : contests.entrySet()) {
                sum += con.getValue();
            }

            return sum;
        }
    }
}
