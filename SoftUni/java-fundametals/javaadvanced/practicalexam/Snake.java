package javaadvanced.practicalexam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Snake {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());
        String[][] screen = new String[n][n];

        String[] directions = reader.readLine().split(", ");

        String[] input;
        int startX = 0;
        int startY = 0;
        long snakeLength = 1;


        for (int i = 0; i < n; i++) {
            input = reader.readLine().split("\\s+");
            for (int j = 0; j < n; j++) {
                if (input[j].equals("*") || input[j].equals("f") || input[j].equals("s") || input[j].equals("e"))
                    screen[i][j] = input[j];
                else
                    screen[i][j] = "*";
                if (screen[i][j].toLowerCase().equals("s")) {
                    startX = i;
                    startY = j;
                    screen[i][j] = "*";
                }
            }
        }

        int currentX = startX;
        int currentY = startY;

        for (String direction : directions) {
            switch (direction.toLowerCase()) {
                case "up":
                    if (currentX == 0) {
                        currentX = n - 1;
                    } else {
                        currentX--;
                    }

                    if (screen[currentX][currentY].toLowerCase().equals("*"))
                        break;

                    if (screen[currentX][currentY].toLowerCase().equals("e")) {
                        System.out.println("You lose! Killed by an enemy!");
                        return;
                    } else if (screen[currentX][currentY].toLowerCase().equals("f")) {
                        snakeLength++;
                        screen[currentX][currentY] = "*";
                    }
                    break;
                case "down":
                    if (currentX == n - 1) {
                        currentX = 0;
                    } else {
                        currentX++;
                    }

                    if (screen[currentX][currentY].toLowerCase().equals("*"))
                        break;

                    if (screen[currentX][currentY].toLowerCase().equals("e")) {
                        System.out.println("You lose! Killed by an enemy!");
                        return;
                    } else if (screen[currentX][currentY].toLowerCase().equals("f")) {
                        snakeLength++;
                        screen[currentX][currentY] = "*";

                    }
                    break;
                case "right":
                    if (currentY == n - 1) {
                        currentY = 0;
                    } else {
                        currentY++;
                    }

                    if (screen[currentX][currentY].toLowerCase().equals("*"))
                        break;

                    if (screen[currentX][currentY].toLowerCase().equals("e")) {
                        System.out.println("You lose! Killed by an enemy!");
                        return;
                    } else if (screen[currentX][currentY].toLowerCase().equals("f")) {
                        snakeLength++;
                        screen[currentX][currentY] = "*";

                    }
                    break;
                case "left":
                    if (currentY == 0) {
                        currentY = n - 1;
                    } else {
                        currentY--;
                    }

                    if (screen[currentX][currentY].toLowerCase().equals("*"))
                        break;

                    if (screen[currentX][currentY].toLowerCase().equals("e")) {
                        System.out.println("You lose! Killed by an enemy!");
                        return;
                    } else if (screen[currentX][currentY].toLowerCase().equals("f")) {
                        snakeLength++;
                        screen[currentX][currentY] = "*";

                    }
                    break;
                default:
                    return;
            }
        }

        int footCount = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (screen[i][j].toLowerCase().equals("f"))
                    footCount++;
            }
        }

        if (footCount > 0) {
            System.out.println("You lose! There is still " + footCount + " food to be eaten.");
        } else {
            System.out.println("You win! Final snake length is " + snakeLength);
        }
    }
}