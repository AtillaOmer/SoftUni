    package javaadvanced.practicalexam;

    import java.io.BufferedReader;
    import java.io.IOException;
    import java.io.InputStreamReader;
    import java.util.ArrayDeque;
    import java.util.regex.Matcher;
    import java.util.regex.Pattern;

    public class Internship {
        public static void main(String[] args) throws IOException {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

            Pattern pattern = Pattern.compile("[A-Z][a-z]+\\s+[A-Z][a-z]+");
            int problems = Integer.parseInt(reader.readLine());
            int canidates = Integer.parseInt(reader.readLine());

            ArrayDeque<String> problemsStack = new ArrayDeque<>();
            ArrayDeque<String> candidateQueue = new ArrayDeque<>();

            for (int i = 0; i < problems; i++) {
                String getProblem = reader.readLine();
                problemsStack.push(getProblem);
            }

            for (int i = 0; i < canidates; i++) {
                String getCandidate = reader.readLine();
                Matcher matcher = pattern.matcher(getCandidate);
                if (matcher.find())
                    candidateQueue.add(getCandidate);
            }

            String currentCandidate;
            String currentProblem;

            while (candidateQueue.size() > 0 || problemsStack.size() > 0){
                if (candidateQueue.size() == 1) {
                    currentCandidate = candidateQueue.remove();
                    System.out.println(String.format("%s gets the job!", currentCandidate));
                    return;
                } else {
                    currentCandidate = candidateQueue.remove();
                    if (!problemsStack.isEmpty()){
                        currentProblem = problemsStack.pop();

                        int canidateAsciiSum = 0;
                        int problemAsciiSum = 0;

                        for (char ch : currentCandidate.toCharArray()) {
                            canidateAsciiSum += (int) ch;
                        }

                        for (char ch : currentProblem.toCharArray()) {
                            problemAsciiSum += (int) ch;
                        }

                        if (canidateAsciiSum > problemAsciiSum) {
                            candidateQueue.add(currentCandidate);
                            System.out.println(String.format("%s solved %s.", currentCandidate, currentProblem));
                        } else {
                            problemsStack.addLast(currentProblem);
                            System.out.println(String.format("%s failed %s.", currentCandidate, currentProblem));
                        }
                    } else {
                        for (String candidate : candidateQueue) {
                            StringBuilder sb = new StringBuilder(candidateQueue.toString());
                            sb.deleteCharAt(sb.length() - 1);
                            System.out.println(sb.toString().replaceAll("[\\[\\]]", ""));
                          //  System.out.println(String.join(",", candidateQueue.toString()));
                            return;
                        }
                    }
                }
            }

            if (candidateQueue.size() == 1) {
                currentCandidate = candidateQueue.remove();
                System.out.println(String.format("%s gets the job!", currentCandidate));
            } else if(!candidateQueue.isEmpty()){
                StringBuilder sb = new StringBuilder(candidateQueue.toString());
                sb.deleteCharAt(sb.length() - 1);
                System.out.println(sb.toString().replaceAll("[\\[\\]]", ""));
            } else {
                return;
            }
        }
    }
