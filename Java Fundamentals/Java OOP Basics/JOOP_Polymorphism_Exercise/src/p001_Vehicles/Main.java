package p001_Vehicles;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;

public class Main {
    private static DecimalFormat DECIMA_FORMAT_KILOMETERS = new DecimalFormat("#.##");
    private final static String CAR = "Car";
    private final static String DRIVE_CAR = "Car travelled %s km";
    private final static String REFUEL_CAR = "Car needs refueling";
    private final static String TRUCK = "Truck";
    private final static String DRIVE_TRUCK = "Truck travelled %s km";
    private final static String REFUEL_TRUCK = "Truck needs refueling";
    private final static String DRIVE = "Drive";
    private final static String REFUEL = "Refuel";

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String[] carTokens = reader.readLine().split(" ");
        Double carFuelQuantity = Double.parseDouble(carTokens[1]);
        Double carFuelConsumption = Double.parseDouble(carTokens[2]);

        String[] truckTokens = reader.readLine().split(" ");
        Double truckFuelQuantity = Double.parseDouble(truckTokens[1]);
        Double truckFuelConsumption = Double.parseDouble(truckTokens[2]);

        Vehicle car = new Car(carFuelQuantity, carFuelConsumption);
        Vehicle truck = new Truck(truckFuelQuantity, truckFuelConsumption);

        int n = Integer.parseInt(reader.readLine());
        while (n-- > 0) {
            String[] tokens = reader.readLine().split("\\s+");
            String command = tokens[0];
            String vehicleType = tokens[1];
            Double property = Double.parseDouble(tokens[2]);

            switch (command) {
                case DRIVE:
                    switch (vehicleType) {
                        case CAR:
                            if (car.drive(property))
                                System.out.println(String.format(DRIVE_CAR, DECIMA_FORMAT_KILOMETERS.format(property)));
                            else
                                System.out.println(REFUEL_CAR);
                            break;
                        case TRUCK:
                            if (truck.drive(property))
                                System.out.println(String.format(DRIVE_TRUCK, DECIMA_FORMAT_KILOMETERS.format(property)));
                            else
                                System.out.println(REFUEL_TRUCK);
                            break;
                    }
                    break;
                case REFUEL:
                    switch (vehicleType) {
                        case CAR:
                            car.refuel(property);
                            break;
                        case TRUCK:
                            truck.refuel(property);
                            break;
                    }
                    break;
            }
        }

        System.out.println(car);
        System.out.println(truck);
    }
}
