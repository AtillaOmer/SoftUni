package p001_Vehicles;

public class Car extends Vehicle {
    private static Double CAR_AIR_CONDITIONER_CONSUMPTION = 0.9;

    public Car(Double fuelQuantity, Double fuelConsumption) {
        super(fuelQuantity, fuelConsumption);
    }

    @Override
    public boolean drive(Double km) {
        if (km * (this.fuelConsumption + CAR_AIR_CONDITIONER_CONSUMPTION) > this.fuelQuantity) {
            return false;
        }

        this.fuelQuantity -= km * (this.fuelConsumption + CAR_AIR_CONDITIONER_CONSUMPTION);
        return true;
    }
}
