package p001_Vehicles;

import java.text.DecimalFormat;

public class Truck extends Vehicle {
    private static Double TRUCK_AIR_CONDITIONER_CONSUMPTION = 1.6;
    private static Double TRUCK_REFUEL_QUANTITY = 0.95;

    public Truck(Double fuelQuantity, Double fuelConsumption) {
        super(fuelQuantity, fuelConsumption);
    }

    @Override
    public boolean drive(Double km) {
        if (km * (this.fuelConsumption + TRUCK_AIR_CONDITIONER_CONSUMPTION) > this.fuelQuantity) {
            return false;
        }

        this.fuelQuantity -= km * (this.fuelConsumption + TRUCK_AIR_CONDITIONER_CONSUMPTION);
        return true;
    }

    @Override
    public void refuel(Double quantity) {
        this.fuelQuantity += quantity * TRUCK_REFUEL_QUANTITY;
    }
}
