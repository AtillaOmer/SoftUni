package p001_Vehicles;

import java.text.DecimalFormat;

public class Vehicle {
    protected Double fuelQuantity;
    protected Double fuelConsumption;

    public Vehicle(Double fuelQuantity, Double fuelConsumption) {
        this.fuelQuantity = fuelQuantity;
        this.fuelConsumption = fuelConsumption;
    }

    protected Double getFuelQuantity() {
        return fuelQuantity;
    }

    protected void setFuelQuantity(Double fuelQuantity) {
        this.fuelQuantity = fuelQuantity;
    }


    protected Double getFuelConsumption() {
        return fuelConsumption;
    }

    protected void setFuelConsumption(Double fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public boolean drive(Double km) {
        if (km * fuelConsumption > this.fuelQuantity) {
            return false;
        }

        this.fuelQuantity -= km * fuelConsumption;
        return true;
    }

    public void refuel(Double quantity) {
        this.fuelQuantity += quantity;
    }

    @Override
    public String toString() {
        return String.format("%s: %.2f", this.getClass().getSimpleName(), this.fuelQuantity);
    }
}
