package p003_Wild_Farm;

import p003_Wild_Farm.animals.Animal;
import p003_Wild_Farm.animals.mammal.Mouse;
import p003_Wild_Farm.animals.mammal.Zebra;
import p003_Wild_Farm.animals.mammal.felime.Cat;
import p003_Wild_Farm.animals.mammal.felime.Tiger;
import p003_Wild_Farm.foods.Food;
import p003_Wild_Farm.foods.Meat;
import p003_Wild_Farm.foods.Vegetable;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Main {
    public static final String VEGETABLE = "Vegetable";
    public static final String MEAT = "Meat";
    public static final String MOUSE = "Mouse";
    public static final String CAT = "Cat";
    public static final String TIGER = "Tiger";
    public static final String ZEBRA = "Zebra";
    public static final String ANIMAL_CANNOT_EAT_FOOD = "%ss are not eating that type of food!";

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        List<Animal> animals = new ArrayList<>();
        String input;
        while (!"END".equalsIgnoreCase(input = reader.readLine())) {
            String[] animalTokens = input.split("\\s+");


            Animal animal = createAnimal(animalTokens);
            animals.add(animal);

            String[] foodTokens = reader.readLine().split(" ");
            Food food = createFood(foodTokens);

            try {
                animal.makeSound();
                animal.eat(food);
            } catch (IllegalArgumentException iae){
                System.out.println(iae.getMessage());
            }
        }

        for (Animal a : animals) {
            System.out.println(a);
        }
    }

    private static Food createFood(String[] foodTokens) {
        String foodType = foodTokens[0];
        Integer foodQuantity = Integer.parseInt(foodTokens[1]);
        switch (foodType) {
            case VEGETABLE:
                Food vegetable = new Vegetable(foodQuantity);
                return vegetable;
            case MEAT:
                Food meat = new Meat(foodQuantity);
                return meat;
        }
        return null;
    }

    private static Animal createAnimal(String[] animalTokens) {
        String animalType = animalTokens[0];
        String animalName = animalTokens[1];
        Double animalWeight = Double.parseDouble(animalTokens[2]);
        String animalLivingRegion = animalTokens[3];

        Animal animal = null;
        if (animalTokens.length == 5) {
            String breed = animalTokens[4];
            Animal cat = new Cat(animalName, animalType, animalWeight, animalLivingRegion, breed);
            return cat;
        } else {
            switch (animalType) {
                case MOUSE:
                    Animal mouse = new Mouse(animalName, animalType, animalWeight, animalLivingRegion);
                    return mouse;
                case TIGER:
                    Animal tiger = new Tiger(animalName, animalType, animalWeight, animalLivingRegion);
                    return tiger;
                case ZEBRA:
                    Animal zebra = new Zebra(animalName, animalType, animalWeight, animalLivingRegion);
                    return zebra;
            }
        }

        return null;
    }
}
