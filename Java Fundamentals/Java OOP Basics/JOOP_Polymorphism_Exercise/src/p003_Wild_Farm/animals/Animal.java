package p003_Wild_Farm.animals;

import p003_Wild_Farm.foods.Food;

import java.text.DecimalFormat;

public abstract class Animal {
    protected static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.##");

    protected String animalName;
    protected String animalType;
    protected Double animalWeight;
    protected Integer foodEaten;

    protected Animal(String animalName, String animalType, Double animalWeight) {
        this.animalName = animalName;
        this.animalType = animalType;
        this.animalWeight = animalWeight;
        this.foodEaten = 0;
    }

    public String getAnimalType() {
        return animalType;
    }

    public abstract void makeSound();

    public void eat(Food food) {
        this.foodEaten += food.getQuantity();
    }

    @Override
    public String toString() {
        return String.format("%s[%s, %s, %d]", this.animalType, this.animalName, DECIMAL_FORMAT.format(this.animalWeight), this.foodEaten);
    }
}
