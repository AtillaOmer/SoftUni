package p003_Wild_Farm.animals.mammal;

import p003_Wild_Farm.foods.Food;
import p003_Wild_Farm.foods.Meat;

public class Mouse extends Mammal {
    public static final String MOUSE_FOOD_EXCEPTION = "Mice are not eating that type of food!";
    public Mouse(String animalName, String animalType, Double animalWeight, String livingRegion) {
        super(animalName, animalType, animalWeight, livingRegion);
    }

    @Override
    public void makeSound() {
        System.out.println("SQUEEEAAAK!");
    }

    @Override
    public void eat(Food food) {
        if (food instanceof Meat){
            throw new IllegalArgumentException(MOUSE_FOOD_EXCEPTION);
        }
        super.eat(food);
    }
}
