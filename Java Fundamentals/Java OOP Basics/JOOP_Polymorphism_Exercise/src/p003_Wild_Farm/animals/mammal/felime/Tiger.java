package p003_Wild_Farm.animals.mammal.felime;

import p003_Wild_Farm.foods.Food;
import p003_Wild_Farm.foods.Vegetable;

public class Tiger extends Felime {
    public static final String TIGER_FOOD_EXCEPTION = "Tigers are not eating that type of food!";

    public Tiger(String animalName, String animalType, Double animalWeight, String livingRegion) {
        super(animalName, animalType, animalWeight, livingRegion);
    }

    @Override
    public void makeSound() {
        System.out.println("ROAAR!!!");
    }

    @Override
    public void eat(Food food) {
        if (food instanceof Vegetable) {
            throw new IllegalArgumentException(TIGER_FOOD_EXCEPTION);
        }
        super.eat(food);
    }
}
