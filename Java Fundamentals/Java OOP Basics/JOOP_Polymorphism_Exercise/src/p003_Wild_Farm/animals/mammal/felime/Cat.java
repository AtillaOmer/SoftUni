package p003_Wild_Farm.animals.mammal.felime;

public class Cat extends Felime {
    private String breed;

    public Cat(String animalName, String animalType, Double animalWeight, String livingRegion, String breed) {
        super(animalName, animalType, animalWeight, livingRegion);
        this.setBreed(breed);
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    @Override
    public void makeSound() {
        System.out.println("Meowwww");
    }

    @Override
    public String toString() {
        return String.format("%s[%s, %s, %s, %s, %d]", this.animalType, this.animalName, this.breed, DECIMAL_FORMAT.format(this.animalWeight), this.livingRegion, this.foodEaten);
    }
}
