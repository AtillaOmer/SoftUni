package p003_Wild_Farm.animals.mammal.felime;

import p003_Wild_Farm.animals.mammal.Mammal;

public abstract class Felime extends Mammal {
    public Felime(String animalName, String animalType, Double animalWeight, String livingRegion) {
        super(animalName, animalType, animalWeight, livingRegion);
    }
}
