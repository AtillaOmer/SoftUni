package p003_Wild_Farm.animals.mammal;

import p003_Wild_Farm.foods.Food;
import p003_Wild_Farm.foods.Meat;

public class Zebra extends Mammal {
    public static final String ZEBRA_FOOD_EXCEPTION = "Zebras are not eating that type of food!";

    public Zebra(String animalName, String animalType, Double animalWeight, String livingRegion) {
        super(animalName, animalType, animalWeight, livingRegion);
    }

    @Override
    public void makeSound() {
        System.out.println("Zs");
    }

    @Override
    public void eat(Food food) {
        if (food instanceof Meat) {
            throw new IllegalArgumentException(ZEBRA_FOOD_EXCEPTION);
        }
        super.eat(food);
    }
}
