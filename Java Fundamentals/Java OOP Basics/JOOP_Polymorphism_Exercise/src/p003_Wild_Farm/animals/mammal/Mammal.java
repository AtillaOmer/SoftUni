package p003_Wild_Farm.animals.mammal;

import p003_Wild_Farm.animals.Animal;

public abstract class Mammal extends Animal {
    protected String livingRegion;

    public Mammal(String animalName, String animalType, Double animalWeight, String livingRegion) {
        super(animalName, animalType, animalWeight);
        this.livingRegion = livingRegion;
    }

    @Override
    public String toString() {
        return String.format("%s[%s, %s, %s, %d]", this.animalType, this.animalName, DECIMAL_FORMAT.format(this.animalWeight), this.livingRegion, this.foodEaten);
    }
}
