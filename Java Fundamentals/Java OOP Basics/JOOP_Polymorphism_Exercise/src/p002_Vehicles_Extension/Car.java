package p002_Vehicles_Extension;

public class Car extends Vehicle {
    public Car(Double fuelQuantity, Double fuelConsumption, Double tankCapacity) {
        super(fuelQuantity, fuelConsumption, tankCapacity);
    }

}
