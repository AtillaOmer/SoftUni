package p002_Vehicles_Extension;

public class Vehicle {
    private final static String TANK_MESSAGE = "Cannot fit fuel in tank";
    private final static String NEGATIVE_REFUEL_MESSAGE = "Fuel must be a positive number";
    protected Double fuelQuantity;
    protected Double fuelConsumption;
    protected Double tankCapacity;

    public Vehicle(Double fuelQuantity, Double fuelConsumption, Double tankCapacity) {
        this.setFuelQuantity(fuelQuantity);
        this.setFuelConsumption(fuelConsumption);
        this.setTankCapacity(tankCapacity);
    }

    protected void setFuelQuantity(Double fuelQuantity) {
        this.fuelQuantity = fuelQuantity;
    }

    protected void setFuelConsumption(Double fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public void setTankCapacity(Double tankCapacity) {
        this.tankCapacity = tankCapacity;
    }

    protected boolean drive(Double km) {
        if (km * fuelConsumption > this.fuelQuantity) {
            return false;
        }

        this.fuelQuantity -= km * fuelConsumption;
        return true;
    }

    protected boolean drive(Double km, Double airConditionConsumption) {
        if (km * (fuelConsumption + airConditionConsumption) > this.fuelQuantity) {
            return false;
        }

        this.fuelQuantity -= km * (fuelConsumption + airConditionConsumption);
        return true;
    }

    public void refuel(Double quantity) {
        if (quantity + this.fuelQuantity > this.tankCapacity) {
            throw new IllegalArgumentException(TANK_MESSAGE);
        }
        if (quantity < 1) {
            throw new IllegalArgumentException(NEGATIVE_REFUEL_MESSAGE);
        }
        this.fuelQuantity += quantity;
    }

    @Override
    public String toString() {
        return String.format("%s: %.2f", this.getClass().getSimpleName(), this.fuelQuantity);
    }
}
