package p002_Vehicles_Extension;

public class Truck extends Vehicle {
    private static Double TRUCK_REFUEL_QUANTITY = 0.95;
    private final static String TANK_MESSAGE = "Cannot fit fuel in tank";
    private final static String NEGATIVE_REFUEL_MESSAGE = "Fuel must be a positive number";

    public Truck(Double fuelQuantity, Double fuelConsumption, Double tankCapacity) {
        super(fuelQuantity, fuelConsumption, tankCapacity);
    }


    @Override
    public void refuel(Double quantity) {
        if (quantity < 1) {
            throw new IllegalArgumentException(NEGATIVE_REFUEL_MESSAGE);
        }
        if (quantity + this.fuelQuantity > this.tankCapacity) {
            throw new IllegalArgumentException(TANK_MESSAGE);
        }
        this.fuelQuantity += quantity * TRUCK_REFUEL_QUANTITY;
    }
}
