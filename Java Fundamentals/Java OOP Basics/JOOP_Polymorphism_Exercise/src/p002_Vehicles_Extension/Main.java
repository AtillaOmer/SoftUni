package p002_Vehicles_Extension;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;

public class Main {
    private static DecimalFormat DECIMAL_FORMAT_KILOMETERS = new DecimalFormat("#.##");
    private final static String CAR = "Car";
    private final static String DRIVE_CAR = "Car travelled %s km";
    private final static String REFUEL_CAR = "Car needs refueling";
    private final static Double CAR_AIR_CONDITIONER_CONSUMPTION = 0.9;
    private final static String TRUCK = "Truck";
    private final static String DRIVE_TRUCK = "Truck travelled %s km";
    private final static String REFUEL_TRUCK = "Truck needs refueling";
    private final static Double TRUCK_AIR_CONDITIONER_CONSUMPTION = 1.6;
    private final static String BUS = "Bus";
    private final static String DRIVE_BUS = "Bus travelled %s km";
    private final static String REFUEL_BUS = "Bus needs refueling";
    private final static Double BUS_AIR_CONDITIONER_CONSUMPTION = 1.4;
    private final static String DRIVE = "Drive";
    private final static String REFUEL = "Refuel";
    private final static String DRIVE_EMPTY = "DriveEmpty";

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String[] carTokens = reader.readLine().split(" ");
        Double carFuelQuantity = Double.parseDouble(carTokens[1]);
        Double carFuelConsumption = Double.parseDouble(carTokens[2]);
        Double carTankCapacity = Double.parseDouble(carTokens[3]);

        String[] truckTokens = reader.readLine().split(" ");
        Double truckFuelQuantity = Double.parseDouble(truckTokens[1]);
        Double truckFuelConsumption = Double.parseDouble(truckTokens[2]);
        Double truckTankCapacity = Double.parseDouble(truckTokens[3]);

        String[] busTokens = reader.readLine().split(" ");
        Double busFuelQuantity = Double.parseDouble(busTokens[1]);
        Double busFuelConsumption = Double.parseDouble(busTokens[2]);
        Double busTankCapacity = Double.parseDouble(busTokens[3]);

        Vehicle car = new Car(carFuelQuantity, carFuelConsumption, carTankCapacity);
        Vehicle truck = new Truck(truckFuelQuantity, truckFuelConsumption, truckTankCapacity);
        Vehicle bus = new Bus(busFuelQuantity, busFuelConsumption, busTankCapacity);

        int n = Integer.parseInt(reader.readLine());
        while (n-- > 0) {
            try {
                String[] tokens = reader.readLine().split("\\s+");
                String command = tokens[0];
                String vehicleType = tokens[1];
                Double property = Double.parseDouble(tokens[2]);

                switch (command) {
                    case DRIVE:
                        switch (vehicleType) {
                            case CAR:
                                if (car.drive(property, CAR_AIR_CONDITIONER_CONSUMPTION))
                                    System.out.println(String.format(DRIVE_CAR, DECIMAL_FORMAT_KILOMETERS.format(property)));
                                else
                                    System.out.println(REFUEL_CAR);
                                break;
                            case TRUCK:
                                if (truck.drive(property, TRUCK_AIR_CONDITIONER_CONSUMPTION))
                                    System.out.println(String.format(DRIVE_TRUCK, DECIMAL_FORMAT_KILOMETERS.format(property)));
                                else
                                    System.out.println(REFUEL_TRUCK);
                                break;
                            case BUS:
                                if (bus.drive(property, BUS_AIR_CONDITIONER_CONSUMPTION))
                                    System.out.println(String.format(DRIVE_BUS, DECIMAL_FORMAT_KILOMETERS.format(property)));
                                else
                                    System.out.println(REFUEL_BUS);
                                break;
                        }
                        break;
                    case REFUEL:
                        switch (vehicleType) {
                            case CAR:
                                car.refuel(property);
                                break;
                            case TRUCK:
                                truck.refuel(property);
                                break;
                            case BUS:
                                bus.refuel(property);
                                break;
                        }
                        break;
                    case DRIVE_EMPTY:
                        if (bus.drive(property))
                            System.out.println(String.format(DRIVE_BUS, DECIMAL_FORMAT_KILOMETERS.format(property)));
                        else
                            System.out.println(REFUEL_BUS);
                }
            } catch (IllegalArgumentException iae) {
                System.out.println(iae.getMessage());
            }
        }

        System.out.println(car);
        System.out.println(truck);
        System.out.println(bus);
    }
}
