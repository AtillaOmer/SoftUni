package p001_Shapes_Drawing;

public interface Drawable {
    public void draw();
}
