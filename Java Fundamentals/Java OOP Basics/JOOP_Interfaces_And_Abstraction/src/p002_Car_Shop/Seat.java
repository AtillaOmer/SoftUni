package p002_Car_Shop;

public class Seat implements Car {
    private String model;
    private String color;
    public Integer horsePower;
    private String countryProduced;

    public Seat(String model, String color, Integer horsePower, String countryProduced) {
        this.model = model;
        this.color = color;
        this.horsePower = horsePower;
        this.countryProduced = countryProduced;
    }

    public String getCountryProduced() {
        return countryProduced;
    }

    @Override
    public String getModel() {
        return this.model;
    }

    @Override
    public String getColor() {
        return this.color;
    }

    @Override
    public Integer getHorsePower() {
        return this.horsePower;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(String.format("%s is gray and have %d horse powers", this.getModel(), this.getHorsePower()))
                .append(System.lineSeparator())
                .append(String.format("This is %s produced in %s and have 4 tires", this.getModel(), this.getCountryProduced()));

        return stringBuilder.toString();
    }
}
