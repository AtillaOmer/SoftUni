package p005_Say_Hello_Extend;

public interface Person {
    String getName();

    String sayHello();
}
