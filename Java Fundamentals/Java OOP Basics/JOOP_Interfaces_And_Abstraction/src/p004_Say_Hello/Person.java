package p004_Say_Hello;

public interface Person {
    String getName();

    String sayHello();
}
