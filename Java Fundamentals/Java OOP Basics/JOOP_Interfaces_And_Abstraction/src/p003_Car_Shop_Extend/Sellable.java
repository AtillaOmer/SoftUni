package p003_Car_Shop_Extend;

public interface Sellable extends Car {
    Double getPrice();
}
