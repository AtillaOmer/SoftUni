package p003_Car_Shop_Extend;

public class Audi implements Rentable {
    private String model;
    private String color;
    private Integer horsePower;
    private String countryProduced;
    private Integer minRetDay;
    private Double pricePerDay;

    public Audi(String model, String color, Integer horsePower, String countryProduced, Integer minRetDay, Double pricePerDay) {
        this.model = model;
        this.color = color;
        this.horsePower = horsePower;
        this.countryProduced = countryProduced;
        this.minRetDay = minRetDay;
        this.pricePerDay = pricePerDay;
    }

    public String getCountryProduced() {
        return countryProduced;
    }

    public Integer getMinRetDay() {
        return minRetDay;
    }

    @Override
    public Integer getMinRentDay() {
        return this.minRetDay;
    }

    @Override
    public Double getPricePerDay() {
        return this.pricePerDay;
    }

    @Override
    public String getModel() {
        return this.model;
    }

    @Override
    public String getColor() {
        return this.color;
    }

    @Override
    public Integer getHorsePower() {
        return this.horsePower;
    }

    @Override
    public String toString() {
        return String.format("This is %s produced in %s and have 4 tires", this.getModel(), this.getCountryProduced());
    }
}
