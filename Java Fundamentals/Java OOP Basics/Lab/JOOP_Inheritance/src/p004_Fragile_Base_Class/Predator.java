package p004_Fragile_Base_Class;

public class Predator extends Animal {
    private int health;

    public Predator() {
        super();
    }

    public void feed(Food food) {
        super.eat(food);
        this.health++;
    }

    public int getHealth() {
        return health;
    }
}
