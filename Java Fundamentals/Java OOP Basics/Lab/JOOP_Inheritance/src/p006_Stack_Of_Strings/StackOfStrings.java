package p006_Stack_Of_Strings;

import java.util.ArrayList;
import java.util.List;

public class StackOfStrings {
    private List<String> data;

    public StackOfStrings() {
        data = new ArrayList<>();
    }

    public void push(String item) {
        this.data.add(item);
    }

    public String pop() {
        String element = this.data.get(data.size() - 1);
        this.data.remove(element);
        return element;
    }

    public String peek(){
        String element = this.data.get(data.size() -1);
        return element;
    }

    public boolean isEmpty(){
        return  this.data.isEmpty();
    }
}
