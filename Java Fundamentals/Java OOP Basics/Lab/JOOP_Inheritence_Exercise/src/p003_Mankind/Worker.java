package p003_Mankind;

import java.text.DecimalFormat;

public class Worker extends Human {
    private double wedges;
    private double hpd;

    public Worker(String firstName, String lastName, double wedges, double hpd) {
        super.setFirstName(firstName);
        this.setLastName(lastName);
        this.setWedges(wedges);
        this.setHpd(hpd);
    }

    @Override
    protected void setLastName(String lastName) {
        if (lastName.length() < 4) {
            throw new IllegalArgumentException("Expected length more than 3 symbols!Argument: lastName");
        }
        super.setLastName(lastName);
    }

    public void setWedges(double wedges) {
        if (wedges < 11) {
            throw new IllegalArgumentException("Expected value mismatch!Argument: weekSalary");
        }
        this.wedges = wedges;
    }

    public void setHpd(double hpd) {
        if (hpd < 1 || hpd > 12) {
            throw new IllegalArgumentException("Expected value mismatch!Argument: workHoursPerDay");
        }
        this.hpd = hpd;
    }

    public double salaryPerHour() {
        double salaryPerHour = this.wedges / 7 / hpd;

        return salaryPerHour;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("First Name: ").append(this.getFirstName()).append(System.lineSeparator())
                .append("Last Name: ").append(this.getLastName()).append(System.lineSeparator())
                .append("Week Salary: ").append(String.format("%.2f", this.wedges)).append(System.lineSeparator())
                .append("Hours per day: ").append(String.format("%.2f", this.hpd)).append(System.lineSeparator())
                .append("Salary per hour: ").append(String.format("%.2f", this.salaryPerHour()));

        return stringBuilder.toString();
    }
}
