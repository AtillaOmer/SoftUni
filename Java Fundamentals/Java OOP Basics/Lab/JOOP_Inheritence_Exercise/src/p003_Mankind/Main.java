package p003_Mankind;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String studentString = reader.readLine();
        String workerString = reader.readLine();
        reader.close();

        String[] studentTokens = studentString.split("\\s+");
        String studentFirstName = studentTokens[0];
        String studentLastName = studentTokens[1];
        String studentFacultyNumber = studentTokens[2];

        String[] workerTokens = workerString.split("\\s+");
        String workerFirstName = workerTokens[0];
        String workerLastName = workerTokens[1];
        Double workerWedges = Double.parseDouble(workerTokens[2]);
        Double workerHourPerWeek = Double.parseDouble(workerTokens[3]);

        try {
            Student student = new Student(studentFirstName, studentLastName, studentFacultyNumber);
            Worker worker = new Worker(workerFirstName, workerLastName, workerWedges, workerHourPerWeek);

            System.out.println(student);
            System.out.println(worker);
        } catch (IllegalArgumentException iae) {
            System.out.println(iae.getMessage());
        }
    }
}
