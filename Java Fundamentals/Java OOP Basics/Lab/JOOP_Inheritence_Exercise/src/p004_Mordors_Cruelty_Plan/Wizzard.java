package p004_Mordors_Cruelty_Plan;

import java.util.List;

public class Wizzard {
    private int happiness;
    private String mood;
    private List<Food> foods;

    public Wizzard(List<Food> foods) {
        this.happiness = 0;
        this.setFoods(foods);
    }

    private void setFoods(List<Food> foods) {
        this.foods = foods;
        eat(foods);
    }

    private void eat(List<Food> foods) {
        for (Food food : foods) {
            switch (food.getName().toUpperCase()) {
                case "CRAM":
                    this.happiness += 2;
                    break;
                case "LEMBAS":
                    this.happiness += 3;
                    break;
                case "APPLE":
                case "MELON":
                    this.happiness++;
                    break;
                case "HONEYCAKE":
                    this.happiness += 5;
                    break;
                case "MUSHROOMS":
                    this.happiness -= 10;
                    break;
                default:
                    this.happiness--;
                    break;
            }
        }
        this.setMood(this.happiness);
    }

    private void setMood(int happiness) {
        if (happiness < -5)
            this.mood = "Angry";
        else if (happiness >= -5 && happiness < 0)
            this.mood = "Sad";
        else if (happiness >= 0 && happiness < 15)
            this.mood = "Happy";
        else if (happiness >= 15)
            this.mood = "JavaScript";
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.happiness).append(System.lineSeparator())
                .append(this.mood);
        return stringBuilder.toString();
    }
}
