package p004_Mordors_Cruelty_Plan;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String[] input = reader.readLine().split("\\s+");
        List<Food> foods = new ArrayList<>();

        for (String s : input) {
            foods.add(new Food(s));
        }
        Wizzard gandalf = new Wizzard(foods);
        System.out.println(gandalf);
    }
}
