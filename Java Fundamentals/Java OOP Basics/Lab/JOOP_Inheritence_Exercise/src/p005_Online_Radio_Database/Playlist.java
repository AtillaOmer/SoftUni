package p005_Online_Radio_Database;

import java.util.ArrayList;
import java.util.List;

public class Playlist {
    private List<Song> playlist;

    public Playlist() {
        this.playlist = new ArrayList<>();
    }

    protected void addSong(Song song) {
        playlist.add(song);
        System.out.println("Song added.");
    }

    protected int songCount() {
        return this.playlist.size();
    }

    private String songsLength() {
        int totalSeconds = 0;
        for (Song song : this.playlist) {
            int minutes = song.getSongLength().getMinutes();
            int seconds = song.getSongLength().getSeconds();

            totalSeconds += minutes * 60 + seconds;
        }

        int hours = totalSeconds / 60 / 60;
        totalSeconds -= hours * 60 * 60;
        int minutes = totalSeconds / 60;
        totalSeconds -= minutes * 60;
        int seconds = totalSeconds;

        return String.format("%dh %dm %ds", hours, minutes, seconds);
    }

    @Override
    public String toString() {
        return String.format("Playlist length: %s", this.songsLength());
    }
}
