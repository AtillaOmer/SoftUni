package p005_Online_Radio_Database;

import p005_Online_Radio_Database.exceptions.InvalidArtistNameException;
import p005_Online_Radio_Database.exceptions.InvalidSongNameException;

public class Song {
    private String artistName;
    private String songName;
    private SongLength songLength;

    public Song(String artistName, String songName, SongLength songLength) {
        this.setArtistName(artistName);
        this.setSongName(songName);
        this.setSongLength(songLength);
    }

    protected void setArtistName(String artistName) {
        if (artistName.length() < 3 || artistName.length() > 20) {
            throw new InvalidArtistNameException("Artist name should be between 3 and 20 symbols.");
        }
        this.artistName = artistName;
    }

    protected void setSongName(String songName) {
        if (songName.length() < 3 || songName.length() > 30) {
            throw new InvalidSongNameException("Song name should be between 3 and 30 symbols.");
        }
        this.songName = songName;
    }

    public SongLength getSongLength() {
        return songLength;
    }

    protected void setSongLength(SongLength songLength) {
        this.songLength = songLength;
    }
}
