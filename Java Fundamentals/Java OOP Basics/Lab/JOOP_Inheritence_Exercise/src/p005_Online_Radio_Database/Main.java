package p005_Online_Radio_Database;

import p005_Online_Radio_Database.exceptions.InvalidSongException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Playlist playlist = new Playlist();
        int n = Integer.parseInt(reader.readLine());

        while (n-- > 0) {
            try {
                String[] tokens = reader.readLine().split(";");
                String artistName = tokens[0];
                String songName = tokens[1];
                String songLengthString = tokens[2];

                SongLength songLength = new SongLength(songLengthString);
                Song song = new Song(artistName, songName, songLength);
                playlist.addSong(song);

            } catch (InvalidSongException ise) {
                System.out.println(ise.getMessage());
            }
        }

        System.out.println("Songs added: " + playlist.songCount());
        System.out.println(playlist);
    }
}
