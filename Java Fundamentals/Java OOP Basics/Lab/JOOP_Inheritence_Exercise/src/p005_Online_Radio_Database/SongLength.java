package p005_Online_Radio_Database;

import p005_Online_Radio_Database.exceptions.InvalidSongLengthException;
import p005_Online_Radio_Database.exceptions.InvalidSongMinutesException;
import p005_Online_Radio_Database.exceptions.InvalidSongSecondsException;

public class SongLength {
    private int minutes;
    private int seconds;

    protected SongLength(String songLength) {
        String[] songLengthTokens = songLength.split(":");
        if (songLengthTokens.length < 2 || !songLength.matches("\\d+:\\d+")) {
            throw new InvalidSongLengthException("Invalid song length.");
        }
        int minutes = Integer.parseInt(songLengthTokens[0]);
        int seconds = Integer.parseInt(songLengthTokens[1]);

        this.setSongLength(minutes, seconds);
    }

    private void setSongLength(int minutes, int seconds) {
        if (songLengthInSeconds(minutes, seconds) >= 15 * 60) {
            throw new InvalidSongLengthException("Invalid song length.");
        }

        this.setMinutes(minutes);
        this.setSeconds(seconds);
    }

    public int getMinutes() {
        return minutes;
    }

    protected void setMinutes(int minutes) {
        if (minutes < 0 || minutes > 14) {
            throw new InvalidSongMinutesException("Song minutes should be between 0 and 14.");
        }
        this.minutes = minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    protected void setSeconds(int seconds) {
        if (seconds < 0 || seconds > 59) {
            throw new InvalidSongSecondsException("Song seconds should be between 0 and 59.");
        }
        this.seconds = seconds;
    }

    private int songLengthInSeconds(int mins, int secs) {
        return minutes * 60 + seconds;
    }
}
