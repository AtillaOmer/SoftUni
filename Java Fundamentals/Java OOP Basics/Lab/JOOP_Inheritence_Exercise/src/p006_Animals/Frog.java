package p006_Animals;

public class Frog extends Animal {

    public Frog(String name, Integer age, String gender) {
        super(name, age, gender);
    }

    @Override
    protected String produceSound() {
        return String.format("Frogggg");
    }
}
