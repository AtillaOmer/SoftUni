package p006_Animals;

public abstract class Animal {
    private String name;
    private Integer age;
    private String gender;

    public Animal(String name, Integer age, String gender) {
        this.setName(name);
        this.setAge(age);
        this.setGender(gender);
    }

    protected String getName() {
        return name;
    }

    protected void setName(String name) {
        if (name.trim().isEmpty() || name == null) {
            throw new IllegalArgumentException("Invalid input!");
        }
        this.name = name;
    }

    protected Integer getAge() {
        return age;
    }

    protected void setAge(Integer age) {
        if (age < 1 || age ==null) {
            throw new IllegalArgumentException("Invalid input!");
        }
        this.age = age;
    }

    protected String getGender() {
        return gender;
    }

    protected void setGender(String gender) {
        this.gender = gender;
    }

    protected abstract String produceSound();

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.getClass().getSimpleName()).append(System.lineSeparator())
                .append(String.format("%s %d %s", this.getName(), this.getAge(), this.getGender())).append(System.lineSeparator())
                .append(this.produceSound().toString());
        return stringBuilder.toString();
    }
}
