package p006_Animals;

public class Cat extends Animal {

    public Cat(String name, Integer age, String gender) {
        super(name, age, gender);
    }

    @Override
    protected String produceSound() {
        return String.format("MiauMiau");
    }

}
