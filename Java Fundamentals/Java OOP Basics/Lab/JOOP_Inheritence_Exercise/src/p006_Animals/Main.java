package p006_Animals;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        List<Animal> animals = new ArrayList<>();
        String input;

        while (!"Beast!".equals(input = reader.readLine())) {
            try {
                String[] tokens = reader.readLine().split("\\s+");
                String name = tokens[0];
                Integer age = Integer.parseInt(tokens[1]);
                String gender = tokens[2];
                if (tokens.length < 3) {
                    throw new IllegalArgumentException("Invalid input!");
                }

                switch (input.toUpperCase()) {
                    case "CAT":
                        Animal cat = new Cat(name, age, gender);
                        animals.add(cat);
                        break;
                    case "DOG":
                        Animal dog = new Dog(name, age, gender);
                        animals.add(dog);
                        break;
                    case "FROG":
                        Animal frog = new Frog(name, age, gender);
                        animals.add(frog);
                        break;
                    case "KITTEN":
                        Cat kitten = new Kitten(name, age, gender);
                        animals.add(kitten);
                        break;
                    case "TOMCAT":
                        Cat tomcat = new Tomcat(name, age, gender);
                        animals.add(tomcat);
                        break;
                    default:
                        throw new IllegalArgumentException("Invalid input!");
                }
            } catch (IllegalArgumentException iae) {
                System.out.println(iae.getMessage());
            }
        }

        for (Animal animal : animals) {
            System.out.println(animal);
        }
    }
}
