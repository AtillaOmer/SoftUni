package p006_Animals;

public class Dog extends Animal {

    public Dog(String name, Integer age, String gender) {
        super(name, age, gender);
    }

    @Override
    protected String produceSound() {
        return String.format("BauBau");
    }
}
