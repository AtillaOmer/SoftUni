package p006_Animals;

public class Kitten extends Cat {

    public Kitten(String name, Integer age, String gender) {
        super(name, age, gender);
    }

    @Override
    protected String produceSound() {
        return String.format("Miau");
    }
}
