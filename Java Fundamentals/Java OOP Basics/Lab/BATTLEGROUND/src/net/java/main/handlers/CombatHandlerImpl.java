package net.java.main.handlers;

import net.java.main.interfaces.CombatHandler;
import net.java.main.interfaces.Unit;
import net.java.main.models.units.UnitImpl;

public abstract class CombatHandlerImpl implements CombatHandler {
    private Unit unit;

    protected CombatHandlerImpl() {
    }

    public CombatHandlerImpl(Unit unit) {
        this.setUnit(unit);
    }

    @Override
    public void setUnit(Unit unit) {
        this.unit = unit;
    }
}
