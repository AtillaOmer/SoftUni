package net.java.main.exceptions;

public class GameException extends IllegalArgumentException {

    public GameException(String message) {
        super(message);
    }
}
