package net.java.main.commands;

import net.java.main.exceptions.GameException;
import net.java.main.implementations.PositionImpl;
import net.java.main.interfaces.Command;
import net.java.main.interfaces.Position;
import net.java.main.interfaces.Unit;
import net.java.main.models.units.Marine;
import net.java.main.models.units.UnitImpl;

public class SpawnCommand extends CommandImpl implements Command {
    @Override
    public String execute(String[] args) throws GameException {
        String unitType = args[1];
        String name =  args[2];
        int x = Integer.parseInt(args[3]);
        int y = Integer.parseInt(args[4]);
        Position position = new PositionImpl(x,y);
        Unit unit = new Marine(name, position);
        return super.execute(args);
    }
}
