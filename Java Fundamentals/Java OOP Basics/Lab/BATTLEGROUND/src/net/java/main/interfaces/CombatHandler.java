package net.java.main.interfaces;

import net.java.main.models.spells.SpellImpl;

import java.util.List;

public interface CombatHandler {

    void setUnit(Unit unit);

    Unit pickNextTarget(List<Unit> targetCandidates);

    SpellImpl generateAttack();
}
