package net.java.main.core;

import net.java.main.dispachers.CommandDispatcherImpl;
import net.java.main.exceptions.GameException;
import net.java.main.interfaces.Engine;
import net.java.main.interfaces.InputReader;
import net.java.main.interfaces.OutputWriter;
import net.java.main.io.InputReaderImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class EngineImpl implements Engine {
    private boolean isStarted;
    private InputReader inputReader;
    private OutputWriter outputWriter;
    private CommandDispatcherImpl commandDispatcher;

    public EngineImpl(OutputWriter outputWriter, CommandDispatcherImpl commandDispatcher) {
        this.outputWriter = outputWriter;
        this.inputReader = new InputReaderImpl(new BufferedReader(new InputStreamReader(System.in)));
        this.commandDispatcher = commandDispatcher;
        this.isStarted = true;
    }

    public void start() throws IOException {

        while (true) {
            try {
                String userInput = this.inputReader.readLine();
                String[] args = userInput.split("\\s+");

                String result = this.commandDispatcher.dispatchCommand(args);

                this.outputWriter.writeLine(result);

                if (result.equals("Game over!")) {
                    break;
                }
            } catch (GameException | IOException e) {
                this.outputWriter.writeLine(e.getMessage());
            }
        }
    }

    private void stop() {
        if (isStarted)
            return;
    }
}
