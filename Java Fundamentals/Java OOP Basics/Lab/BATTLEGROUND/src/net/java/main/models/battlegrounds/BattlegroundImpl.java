package net.java.main.models.battlegrounds;

import net.java.main.exceptions.InvalidPositionException;
import net.java.main.interfaces.Battleground;
import net.java.main.interfaces.Position;
import net.java.main.interfaces.Unit;
import net.java.main.models.units.UnitImpl;

import java.util.ArrayList;
import java.util.List;

public class BattlegroundImpl implements Battleground {
    private Unit[][] battleground;

    public BattlegroundImpl() {
        Unit[][] temp = new UnitImpl[5][5];
        for (Unit[] units : temp) {
            for (Unit unit : units) {
                unit = null;
            }
        }
        this.battleground = temp;
    }

    public BattlegroundImpl(Unit[][] battleground) {
        this.battleground = battleground;
    }

    @Override
    public List<Unit> getUnitsInRange(Position position, int range) throws InvalidPositionException {
        validatePostition(position);
        List<Unit> units = new ArrayList<>();

        int x = position.getX();
        int y = position.getY();
        for (int row = x - range; row < x + range; row++) {
            for (int col = y - range; col < y + range; col++) {
                if ((row < 0 || row > 4) || (col < 0 || col > 4))
                    continue;
                if (battleground[row][col] == null) {
                    Unit unit = battleground[row][col];
                    units.add(unit);
                }
            }
        }
        return units;
    }

    @Override
    public void add(Unit unit) throws InvalidPositionException {
        Integer positionX = unit.getPosition().getX();
        Integer positionY = unit.getPosition().getY();

        if ((positionX < 0 || positionX > 5) || (positionY < 0 || positionY > 5))
            throw new InvalidPositionException("Invalid position/");
        if (battleground[positionX][positionY] != null)
            throw new InvalidPositionException("Invalid position");

        battleground[positionX][positionY] = unit;
    }

    @Override
    public void remove(Unit unit) throws InvalidPositionException {
        Integer positionX = unit.getPosition().getX();
        Integer positionY = unit.getPosition().getY();

        if ((positionX < 0 || positionX > 5) || (positionY < 0 || positionY > 5))
            throw new InvalidPositionException("Invalid position/");

        battleground[positionX][positionY] = null;
    }

    @Override
    public void move(Unit unit, Position position) throws InvalidPositionException {
        int x = position.getX();
        int y = position.getY();

        if ((x < 0 || x > 4) || (y < 0 || y > 4))
            throw new InvalidPositionException("Invalid position/");
        if (battleground[x][y] != null)
            throw new InvalidPositionException("Invalid position");

        battleground[x][y] = null;
        battleground[x][y] = unit;
        unit.setPosition(position);
    }

    private void validatePostition(Position position) throws InvalidPositionException {
        int x = position.getX();
        int y = position.getY();

        if ((x < 0 || x > 5) || (y < 0 || y > 5))
            throw new InvalidPositionException("Invalid position");
    }
}
