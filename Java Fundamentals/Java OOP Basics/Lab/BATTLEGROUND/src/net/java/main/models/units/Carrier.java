package net.java.main.models.units;

import net.java.main.interfaces.CombatHandler;
import net.java.main.interfaces.Position;
import net.java.main.interfaces.Unit;

public class Carrier extends UnitImpl implements Unit {
    private static final int RANGE = 2;
    private static final int HEALTH_POINTS = 50;
    private static final int ENERGY_POINTS = 80;
    private static final int ATTACK_POINTS = 15;
    private static final int DEFENCE_POINTS = 5;

    protected Carrier(String name, int range, Position position, int healthPoints, int energyPoints, int attackPoints, int defencePoints, CombatHandler combatHandler) {
        super(name, range, position, healthPoints, energyPoints, attackPoints, defencePoints, combatHandler);
    }
}
