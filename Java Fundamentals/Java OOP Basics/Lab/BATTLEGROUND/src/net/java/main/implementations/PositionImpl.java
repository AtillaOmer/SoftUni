package net.java.main.implementations;

import net.java.main.interfaces.Position;

public class PositionImpl implements Position {
    int X;
    int Y;

    public PositionImpl(int x, int y) {
        this.X = x;
        this.Y = y;
    }

    @Override
    public int getX() {
        return this.X;
    }

    @Override
    public int getY() {
        return this.Y;
    }
}
