package net.java.main.models.units;

import net.java.main.handlers.MarineCombatHandler;
import net.java.main.interfaces.CombatHandler;
import net.java.main.interfaces.Position;
import net.java.main.interfaces.Unit;

public class Marine extends UnitImpl implements Unit {
    private static final int RANGE = 1;
    private static final int HEALTH_POINTS = 50;
    private static final int ENERGY_POINTS = 80;
    private static final int ATTACK_POINTS = 15;
    private static final int DEFENCE_POINTS = 5;

    public Marine(String name, Position position) {
        super(name, RANGE, position, HEALTH_POINTS, ENERGY_POINTS, ATTACK_POINTS, DEFENCE_POINTS, new MarineCombatHandler());
    }
}
