package net.java.main.commands;

import net.java.main.exceptions.GameException;

public class GameOverCommand extends CommandImpl {

    @Override
    public String execute(String[] args) throws GameException {
        return "Game over!";
    }
}
