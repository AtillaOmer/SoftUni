package net.java.main.commands;

import net.java.main.exceptions.GameException;
import net.java.main.interfaces.Battleground;
import net.java.main.interfaces.Command;
import net.java.main.interfaces.Unit;
import net.java.main.repository.UnitRepository;

public abstract class CommandImpl implements Command {
    private Battleground battleground;
    private UnitRepository<Unit> unitRepository;

    public CommandImpl() {
    }

    public CommandImpl(Battleground battleground, UnitRepository<Unit> unitRepository) {
        this.battleground = battleground;
        this.unitRepository = unitRepository;
    }

    protected Battleground getBattleground() {
        return this.battleground;
    }

    private UnitRepository<Unit> getUnitRepository() {
        return this.unitRepository;
    }

    public String execute(String[] args) throws GameException {
        throw new GameException("Invalid command");
    }
}
