package net.java.main.models.spells;

import net.java.main.interfaces.Spell;

public class BulletRain extends SpellImpl implements Spell {
    private static final int ENERGY_COST = 15;

    protected BulletRain(int damage, int energyCost) {
        super(damage, energyCost);
    }
}
