package net.java.main;

import net.java.main.core.EngineImpl;
import net.java.main.dispachers.CommandDispatcherImpl;
import net.java.main.interfaces.OutputWriter;
import net.java.main.io.OutputWriterImpl;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        OutputWriter writer = new OutputWriterImpl();
        CommandDispatcherImpl commandDispatcher = new CommandDispatcherImpl();

        EngineImpl gameEngine = new EngineImpl(writer, commandDispatcher);
        gameEngine.start();
    }
}
