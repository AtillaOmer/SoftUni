package net.java.main.interfaces;

public interface Command {
    String execute(String[] args);
}
