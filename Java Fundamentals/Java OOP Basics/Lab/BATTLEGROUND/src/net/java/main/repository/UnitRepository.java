package net.java.main.repository;

import net.java.main.interfaces.Repository;
import net.java.main.interfaces.Unit;

import java.util.List;

public class UnitRepository implements Repository<Unit, Unit> {
    public List<Unit> units;


    @Override
    public void safe(Unit unit) {
        this.units.add(unit);
    }

    @Override
    public void remove(Unit unit) {
        if (!units.isEmpty()) {
            this.units.remove(unit);
        }

    }

    @Override
    public List<Unit> findAll() {
        return this.units;
    }
}
