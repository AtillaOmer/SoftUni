package net.java.main.models.spells;

import net.java.main.interfaces.Spell;

public class RageShoot extends SpellImpl implements Spell {
    private static final int ENERGY_COST = 10;

    protected RageShoot(int damage, int energyCost) {
        super(damage, energyCost);
    }
}
