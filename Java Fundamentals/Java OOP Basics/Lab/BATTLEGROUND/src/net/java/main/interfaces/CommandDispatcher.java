package net.java.main.interfaces;

public interface CommandDispatcher {
    String dispatchCommand(String[] args);
}
