package net.java.main.handlers;

import net.java.main.interfaces.CombatHandler;
import net.java.main.interfaces.Unit;
import net.java.main.models.spells.SpellImpl;

import java.util.List;

public class CarrierCombatHadler extends CombatHandlerImpl implements CombatHandler {
    public static final int CLOSEST_ENEMY = 0;
    private int spellCount;

    @Override
    public Unit pickNextTarget(List<Unit> targetCandidates) {
        Unit unit = null;
        if (!targetCandidates.isEmpty()) {
            unit = targetCandidates.get(0);
            targetCandidates.remove(0);
        }

        return unit;
    }

    @Override
    public SpellImpl generateAttack() {
        return this.generateAttack();
    }
}
