package net.java.main.interfaces;

import net.java.main.exceptions.InvalidPositionException;
import net.java.main.models.units.UnitImpl;

import java.util.List;

public interface Battleground {

    List<Unit> getUnitsInRange(Position position, int range) throws InvalidPositionException;

    void add(Unit unit) throws InvalidPositionException;

    void remove(Unit unit) throws InvalidPositionException;

    void move(Unit unit, Position position) throws InvalidPositionException;
}
