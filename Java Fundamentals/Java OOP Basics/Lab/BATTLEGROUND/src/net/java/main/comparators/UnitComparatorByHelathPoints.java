package net.java.main.comparators;

import net.java.main.interfaces.Comparator;
import net.java.main.interfaces.Unit;

public class UnitComparatorByHelathPoints implements Comparator<Unit> {
    @Override
    public int compare(Unit unit1, Unit unit2) {
        return Integer.compare(unit1.getHealthPoints(), unit2.getHealthPoints());
    }
}
