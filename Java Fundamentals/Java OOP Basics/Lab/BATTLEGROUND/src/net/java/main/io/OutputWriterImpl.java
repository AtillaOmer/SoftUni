package net.java.main.io;

import net.java.main.interfaces.OutputWriter;

import java.io.IOException;

public class OutputWriterImpl implements OutputWriter {

    public void writeLine(String line) throws IOException {
        System.out.println(line);
    }
}
