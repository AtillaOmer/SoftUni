package net.java.main.io;

import net.java.main.interfaces.InputReader;

import java.io.BufferedReader;
import java.io.IOException;

public class InputReaderImpl implements InputReader {
    private BufferedReader reader;

    public InputReaderImpl(BufferedReader reader) {
        this.reader = reader;
    }

    @Override
    public String readLine() throws IOException {
        return reader.readLine();
    }
}
