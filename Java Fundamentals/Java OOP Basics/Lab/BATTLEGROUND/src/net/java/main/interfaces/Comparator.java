package net.java.main.interfaces;

import net.java.main.interfaces.Unit;

public interface Comparator<Unit> {
    int compare(Unit unit1, Unit unit2);
}
