package net.java.main.handlers;

import net.java.main.interfaces.CombatHandler;
import net.java.main.interfaces.Unit;
import net.java.main.models.spells.SpellImpl;

import java.util.List;

public class MarineCombatHandler extends CombatHandlerImpl implements CombatHandler {
    public static final int CLOSEST_ENEMY = 0;
    public static final int HALF_MARINE_HEALTH = 25;

    public MarineCombatHandler() {
        super();
    }

    @Override
    public Unit pickNextTarget(List<Unit> targetCandidates) {
        Unit unit = null;
        if (!targetCandidates.isEmpty()) {
            unit = targetCandidates.get(0);
            targetCandidates.remove(0);
        }

        return unit;
    }

    private int getAtack(){
        return this.getAtack();
    }

    @Override
    public SpellImpl generateAttack() {
        return this.generateAttack();
    }
}
