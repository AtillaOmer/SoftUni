package net.java.main.dispachers;

import net.java.main.commands.CommandImpl;
import net.java.main.commands.GameOverCommand;
import net.java.main.commands.InvalidCommand;
import net.java.main.commands.SpawnCommand;
import net.java.main.exceptions.GameException;
import net.java.main.interfaces.Battleground;
import net.java.main.interfaces.Unit;
import net.java.main.repository.UnitRepository;

import java.util.Map;

public class CommandDispatcherImpl {
    private static final String UNKNOWN_COMMAND_EXCEPTION_MESSAGE = "Unknown command!";
    private Map<String, CommandImpl> commands;

    private void seedCommands(Battleground battleground, UnitRepository<Unit> repository) {
        //        TODO
    }

    public String dispatchCommand(String[] args) throws GameException {
        switch (args[0]) {
            case "game-over":
                return new GameOverCommand().execute(args);
            case "spawn":
                return new SpawnCommand().execute(args);
            case "status":

            default:
                return new InvalidCommand().execute(args);
        }
    }
}
