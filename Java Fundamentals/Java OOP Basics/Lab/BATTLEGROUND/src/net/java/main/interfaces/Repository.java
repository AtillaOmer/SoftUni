package net.java.main.interfaces;

import java.util.List;

public interface Repository<Т,T> {
    void safe(T element);

    void remove(T element);

    List<T> findAll();

}
