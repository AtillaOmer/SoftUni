package net.java.main.interfaces;

public interface Position {
    int getX();

    int getY();

    String toString();
}
