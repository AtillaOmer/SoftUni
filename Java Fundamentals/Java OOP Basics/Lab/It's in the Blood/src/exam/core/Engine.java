package exam.core;

import exam.controllers.HealthManager;
import exam.io.ConsoleReader;
import exam.io.ConsoleWriter;

import java.io.IOException;

public class Engine {
    private static final String CHECK_CONDITION = "checkCondition";
    private static final String CREATE_ORGANISM = "createOrganism";
    private static final String ADD_CLUSTER = "addCluster";
    private static final String ADD_CELL = "addCell";
    private static final String ACTIVATE_CLUSTER = "activateCluster";

    private ConsoleReader reader;
    private ConsoleWriter writer;
    private HealthManager healthManager;

    public Engine() {
        this.reader = new ConsoleReader();
        this.writer = new ConsoleWriter();
        this.healthManager = new HealthManager();
    }

    public void run() throws IOException {
        String input;

        while (!"BEER IS COMING".equals(input = this.reader.readLine())) {
            String[] args = input.split("\\s+");

            String result;
            switch (args[0]) {
                case CHECK_CONDITION:
                    result = this.healthManager.checkCondition(args[1]);
                    if (!result.equals("")) {
                        this.writer.writeLine(result);
                    }

                    break;
                case CREATE_ORGANISM:
                    result = this.healthManager.createOrganism(args[1]);
                    if (!result.equals("")) {
                        this.writer.writeLine(result);
                    }
                    break;
                case ADD_CLUSTER:
                    result = this.healthManager.addCluster(args[1], args[2], Integer.parseInt(args[3]), Integer.parseInt(args[4]));
                    if (!result.equals("")) {
                        this.writer.writeLine(result);
                    }
                    break;
                case ADD_CELL:
                    result = this.healthManager.addCell(args[1], args[2], args[3], args[4], Integer.parseInt(args[5]),
                            Integer.parseInt(args[6]), Integer.parseInt(args[7]), Integer.parseInt(args[8]));
                    if (!result.equals("")) {
                        this.writer.writeLine(result);
                    }
                    break;
                case ACTIVATE_CLUSTER:
                    result = this.healthManager.activateCluster(args[1]);
                    if (!result.equals("")) {
                        this.writer.writeLine(result);
                    }
                    break;
            }

        }
    }
}
