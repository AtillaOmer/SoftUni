package exam.entities.clusters;

import exam.entities.cells.Cell;

import java.util.ArrayList;
import java.util.List;

public class Cluster {
    private String id;
    private int rows;
    private int cols;
    private Cell[][] cells;

    public Cluster(String id, int rows, int cols) {
        this.id = id;
        this.rows = rows;
        this.cols = cols;
        cells = new Cell[rows][cols];
    }

    public String getId() {
        return id;
    }

    public int getRows() {
        return rows;
    }

    public int getCols() {
        return cols;
    }

    public Cell[][] getCells() {
        return cells;
    }

    public void setCells(Cell[][] cells) {
        this.cells = cells;
    }

    public void addCell(Cell cell) {
        this.getCells()[cell.getPositionRow()][cell.getPositionCol()] = cell;
    }

    public List<Cell> getCellsList() {
        List<Cell> cellList = new ArrayList<>();

        for (Cell[] row : cells) {
            for (Cell cell : row) {
                if (cell != null) {
                    cellList.add(cell);
                }
            }
        }
        return cellList;
    }
}
