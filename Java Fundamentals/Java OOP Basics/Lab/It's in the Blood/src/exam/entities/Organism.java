package exam.entities;

import exam.entities.cells.Cell;
import exam.entities.clusters.Cluster;

import java.util.*;

public class Organism {
    private static final String BLOOD_CELL = "BloodCell";
    private static final String MICROBE = "Microbe";

    private String name;
    private List<Cluster> clusters;

    public Organism(String name) {
        this.name = name;
        this.clusters = new ArrayList<>();
    }

    private String getName() {
        return name;
    }

    private List<Cluster> getClusters() {
        return this.clusters;
    }

    public Cluster getClusterById(String clusterId) {
        for (Cluster cluster : clusters) {
            if (cluster.getId().equals(clusterId))
                return cluster;
        }
        return null;
    }

    public String addCluster(Cluster cluster) {
        if (this.clusters.isEmpty()) {
            this.clusters.add(cluster);
            return String.format("Organism %s: Created cluster %s", this.getName(), cluster.getId());
        }
        for (Cluster currCluster : this.clusters) {
            if (currCluster.getId().equals(cluster.getId())) {
                return "";
            }

        }
        this.clusters.add(cluster);
        return String.format("Organism %s: Created cluster %s", this.getName(), cluster.getId());
    }

    public String activateCluster() {
        if (!this.getClusters().isEmpty()) {
            Cluster cluster = this.getClusters().get(0);
            List<Cell> cells = cluster.getCellsList();
            while (cells.size() > 1) {
                Cell startCell = cells.get(0);
                cells.remove(0);
                Cell secondCell = cells.get(0);
                cells.remove(0);

                Cell winner = this.figthCells(startCell, secondCell);
                cells.add(0, winner);
            }
            Cell[][] resultCellMatrix = new Cell[cluster.getRows()][cluster.getCols()];
            for (Cell cell : cells) {
                resultCellMatrix[cell.getPositionRow()][cell.getPositionCol()] = cell;
            }
            this.getClusters().get(0).setCells(resultCellMatrix);
            String result = String.format("Organism %s: Activated cluster %s. Cells left: %d", this.getName(), this.getClusters().get(0).getId(), this.getClusters().get(0).getCellsList().size());
            this.moveClusterToTheEnd();
            return result;
        }
        return "";
    }

    private Cell figthCells(Cell startCell, Cell secondCell) {
        String cellType = startCell.getClass().getSuperclass().getSimpleName();

        switch (cellType) {
            case BLOOD_CELL:
                startCell.setHealth(startCell.getHealth() + secondCell.getHealth());
                startCell.setPositionRow(secondCell.getPositionRow());
                startCell.setPositionCol(secondCell.getPositionCol());
                return startCell;
            case MICROBE:
                while (true) {

                    secondCell.setHealth(secondCell.getHealth() - startCell.getEnergy());
                    if (secondCell.getHealth() <= 0) {
                        startCell.setPositionRow(secondCell.getPositionRow());
                        startCell.setPositionCol(secondCell.getPositionCol());
                        return startCell;
                    } else
                        startCell.setHealth(startCell.getHealth() - secondCell.getEnergy());

                    if (startCell.getHealth() <= 0) {
                        secondCell.setPositionRow(startCell.getPositionRow());
                        secondCell.setPositionCol(startCell.getPositionCol());
                        return secondCell;
                    }
                }
        }
        return null;
    }

    private void moveClusterToTheEnd() {
        Cluster cluster = this.getClusters().get(0);
        this.getClusters().remove(0);
        this.getClusters().add(cluster);
    }

    private long getCellCounts() {
        int sum = 0;

        for (Cluster cluster : clusters) {
            Cell[][] cells = cluster.getCells();
            for (Cell[] row : cells) {
                for (Cell cell : row) {
                    if (cell != null)
                        sum++;
                }
            }
        }
        return sum;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("Organism - %s", this.getName())).append(System.lineSeparator())
                .append(String.format("--Clusters: %d", this.getClusters().size())).append(System.lineSeparator())
                .append(String.format("--Cells: %d", this.getCellCounts()));

        if (!this.getClusters().isEmpty()) {
            for (Cluster cluster : clusters) {

                stringBuilder.append(System.lineSeparator()).append(String.format("----Cluster %s", cluster.getId()));

                if (cluster.getCells().length > 0) {
                    for (int i = 0; i < cluster.getCells().length; i++) {
                        for (int j = 0; j < cluster.getCells()[i].length; j++) {
                            Cell cell = cluster.getCells()[i][j];
                            if (cell != null) {

                                stringBuilder.append(System.lineSeparator()).append(cell);
                            }
                        }
                    }
                }
            }
        }
        return stringBuilder.toString();
    }
}
