package exam.entities.cells.microbes;

import exam.entities.cells.Cell;

public abstract class Microbe extends Cell {
    private int virulence;

    public Microbe(int virulence) {
        this.virulence = virulence;
    }

    public Microbe(String id, int health, int positionRow, int positionCol, int virulence) {
        super(id, health, positionRow, positionCol);
        this.virulence = virulence;
    }

    public int getVirulence() {
        return virulence;
    }

    public void setVirulence(int virulence) {
        this.virulence = virulence;
    }


    @Override
    public java.lang.String toString() {
        return super.toString();
    }
}
