package exam.entities.cells.bloodcells;

public class WhiteBloodCell extends BloodCell {
    private int size;

    public WhiteBloodCell(String id, int health, int positionRow, int positionCol, int size) {
        super(id, health, positionRow, positionCol);
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    @Override
    public int getEnergy() {
        return (super.getHealth() + this.size) * 2;
    }

    @Override
    public java.lang.String toString() {

        return String.format("%s--------Health: %d | Size: %d | Energy: %d", super.toString(), this.getHealth(), this.getSize(), this.getEnergy());
    }
}
