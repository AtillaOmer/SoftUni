package exam.entities.cells.microbes;

public class Virus extends Microbe {
    public Virus(String id, int health, int positionRow, int positionCol, int virulence) {
        super(id, health, positionRow, positionCol, virulence);
    }

    @Override
    public int getEnergy() {
        return super.getHealth() + this.getVirulence();
    }

    @Override
    public java.lang.String toString() {

        return String.format("%s--------Health: %d | Virulence: %d | Energy: %d", super.toString(), this.getHealth(), this.getVirulence(), this.getEnergy());
    }
}
