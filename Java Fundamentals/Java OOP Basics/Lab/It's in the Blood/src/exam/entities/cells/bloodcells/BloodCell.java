package exam.entities.cells.bloodcells;

import exam.entities.cells.Cell;

public abstract class BloodCell extends Cell {

    public BloodCell() {
    }

    public BloodCell(String id, int health, int positionRow, int positionCol) {
        super(id, health, positionRow, positionCol);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
