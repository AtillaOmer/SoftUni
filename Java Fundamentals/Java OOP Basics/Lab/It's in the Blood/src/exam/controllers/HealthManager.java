package exam.controllers;

import exam.entities.Organism;
import exam.entities.cells.Cell;
import exam.entities.cells.bloodcells.RedBloodCell;
import exam.entities.cells.bloodcells.WhiteBloodCell;
import exam.entities.cells.microbes.Bacteria;
import exam.entities.cells.microbes.Fungi;
import exam.entities.cells.microbes.Virus;
import exam.entities.clusters.Cluster;

import java.util.HashMap;
import java.util.Map;

public class HealthManager {
    private static final String RED_BLOOD_CELL = "RedBloodCell";
    private static final String WHITE_BLOOD_CELL = "WhiteBloodCell";
    private static final String BACTERIA = "Bacteria";
    private static final String FUNGI = "Fungi";
    private static final String VIRUS = "Virus";

    Map<String, Organism> organisms = new HashMap<>();

    public String checkCondition(String organismName) {
        if (organisms.containsKey(organismName)) {
            Organism organism = organisms.get(organismName);

            return organism.toString();
        }
        return null;
    }

    public String createOrganism(String name) {
        if (organisms.containsKey(name)) {
            return String.format("Organism %s already exists", name);
        } else {
            organisms.put(name, new Organism(name));
            return String.format("Created organism %s", name);
        }
    }

    public String addCluster(String organismName, String id, int rows, int cols) {
        if (organisms.containsKey(organismName)) {
            Cluster cluster = new Cluster(id, rows, cols);
            return organisms.get(organismName).addCluster(cluster);
        }
        return "";
    }

    public String addCell(String organismName, String clusterId, String cellType, String cellId, int health, int positionRow, int positionCol, int additionalProperty) {
        if (organisms.containsKey(organismName)) {
            Cluster cluster = organisms.get(organismName).getClusterById(clusterId);
            if (cluster != null) {

                Cell cell = null;

                switch (cellType) {
                    case RED_BLOOD_CELL:
                        cell = new RedBloodCell(cellId, health, positionRow, positionCol, additionalProperty);
                        break;
                    case WHITE_BLOOD_CELL:
                        cell = new WhiteBloodCell(cellId, health, positionRow, positionCol, additionalProperty);
                        break;
                    case BACTERIA:
                        cell = new Bacteria(cellId, health, positionRow, positionCol, additionalProperty);
                        break;
                    case FUNGI:
                        cell = new Fungi(cellId, health, positionRow, positionCol, additionalProperty);
                        break;
                    case VIRUS:
                        cell = new Virus(cellId, health, positionRow, positionCol, additionalProperty);
                        break;
                }

                if (cell != null) {
                    organisms.get(organismName).getClusterById(clusterId).addCell(cell);
                    return String.format("Organism %s: Created cell %s in cluster %s", organismName, cellId, clusterId);
                }
            }
        }
        return "";
    }

    public String activateCluster(String organismName) {
        if (organisms.containsKey(organismName))
            return organisms.get(organismName).activateCluster();
        else
            return "";
    }
}
