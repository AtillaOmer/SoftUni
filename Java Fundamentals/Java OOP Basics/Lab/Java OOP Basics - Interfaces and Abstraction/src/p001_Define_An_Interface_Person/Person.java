package p001_Define_An_Interface_Person;

public interface Person {
    String getName();

    Integer getAge();

}
