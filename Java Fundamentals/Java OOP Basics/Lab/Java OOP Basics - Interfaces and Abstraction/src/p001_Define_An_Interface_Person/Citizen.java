package p001_Define_An_Interface_Person;

public class Citizen implements Person{
    private String name;
    private Integer age;



    public Citizen(String name, Integer age) {
        this.setName(name);
        this.setAge(age);
    }

    private void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public Integer getAge() {
        return this.age;
    }
}
