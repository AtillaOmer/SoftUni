package p008_Military_Elite.service;

public interface Spy extends Soldier {
    String getCodeNumber();
}
