package p008_Military_Elite.service;

import p008_Military_Elite.implementation.Corps;

public interface SpecialisedSoldier extends Private {
    Corps getCorps();
}
