package p008_Military_Elite.service;

import java.util.List;

public interface Enginer extends SpecialisedSoldier {
    List<String> getRepair();
}
