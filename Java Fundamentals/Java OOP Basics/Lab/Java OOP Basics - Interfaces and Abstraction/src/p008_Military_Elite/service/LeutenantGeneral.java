package p008_Military_Elite.service;

import p008_Military_Elite.implementation.PrivateImpl;

import java.util.List;

public interface LeutenantGeneral extends Private {
    List<String> getPrivates();
}
