package p008_Military_Elite.service;

import p008_Military_Elite.implementation.Mission;

import java.util.List;
import java.util.Set;

public interface Commando extends SpecialisedSoldier {
    Set<Mission> getMissions();
}
