package p008_Military_Elite.service;

public interface Soldier {
    String getId();

    String getFirstName();

    String getLastName();
}
