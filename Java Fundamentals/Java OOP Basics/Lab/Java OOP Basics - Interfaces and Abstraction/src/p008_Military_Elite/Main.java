package p008_Military_Elite;

import p008_Military_Elite.implementation.*;
import p008_Military_Elite.service.Private;
import p008_Military_Elite.service.Soldier;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Main {
    public static final String PRIVATE = "Private";
    public static final String SPY = "Spy";
    public static final String LEUTENANT_GENERAL = "LeutenantGeneral";
    public static final String ENGINEER = "Engineer";
    public static final String COMMANDO = "Commando";


    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        List<Soldier> soldiers = new ArrayList<>();
        Map<String, Private> privates = new HashMap<>();

        String input;
        while (true) {
            if ("End".equalsIgnoreCase(input = reader.readLine())) {
                break;
            }

            String[] tokens = input.split("\\s+");
            String soldierType = tokens[0];
            String id = tokens[1];
            String firstName = tokens[2];
            String lastName = tokens[3];


            Soldier soldier = null;
            switch (soldierType) {
                case PRIVATE:
                    Double salary = Double.parseDouble(tokens[4]);
                    Private privateSoldier = new PrivateImpl(id, firstName, lastName, salary);
                    privates.put(id, privateSoldier);
                    soldiers.add(privateSoldier);
                    break;
                case SPY:
                    String codeNumber = tokens[4];
                    soldier = new SpyImpl(id, firstName, lastName, codeNumber);
                    break;
                case LEUTENANT_GENERAL:
                    salary = Double.parseDouble(tokens[4]);
                    List<String> privateList = new ArrayList<>();
                    for (int i = 5; i < tokens.length; i++) {

                        if (privates.containsKey(tokens[i])) {
                            Private pSoldier = privates.get(tokens[i]);
                            privateList.add(pSoldier.toString());
                        }
                    }
                    Collections.sort(privateList);
                    Collections.reverse(privateList);
                    soldier = new LeutenantGeneralImpl(id, firstName, lastName, salary, privateList);
                    break;
                case ENGINEER:
                    salary = Double.parseDouble(tokens[4]);
                    String corpsName = tokens[5];

                    if (corpsName.equals(Corps.AIRFORCES) || corpsName.equals(Corps.MARINES)) {
                        Corps corps = new Corps(corpsName);
                        List<String> repairs = new ArrayList<>();
                        for (int i = 6; i < tokens.length; i += 2) {
                            String partName = tokens[i];
                            Integer hoursWork = Integer.parseInt(tokens[i + 1]);
                            Repair repair = new Repair(partName, hoursWork);
                            repairs.add(repair.toString());
                        }
                        soldier = new EnginerImpl(id, firstName, lastName, salary, corps, repairs);
                    }
                    break;
                case COMMANDO:
                    salary = Double.parseDouble(tokens[4]);
                    corpsName = tokens[5];

                    if (corpsName.equals(Corps.AIRFORCES) || corpsName.equals(Corps.MARINES)) {
                        Corps corps = new Corps(corpsName);
                        String[] missionsTokens = Arrays.stream(tokens).skip(6).toArray(String[]::new);
                        Set<Mission> missions = createMissions(missionsTokens);
                        soldier = new CommandoImpl(id, firstName, lastName, salary, corps, missions);

                    }
                    break;
            }

            if (soldier != null) {
                soldiers.add(soldier);
            }
        }

        soldiers.forEach(soldier -> System.out.println(soldier));
    }

    private static Set<Mission> createMissions(String[] missionsTokens) {
        Set<Mission> missions = new LinkedHashSet<>();
        Mission mission;

        for (int i = 0; i < missionsTokens.length; i += 2) {

            String codeName = missionsTokens[i];
            String state = missionsTokens[i + 1];

            try {
                mission = new Mission(codeName, state);
                missions.add(mission);
            } catch (IllegalArgumentException ignored) {
                ;
            }
        }

        return missions;
    }
}
