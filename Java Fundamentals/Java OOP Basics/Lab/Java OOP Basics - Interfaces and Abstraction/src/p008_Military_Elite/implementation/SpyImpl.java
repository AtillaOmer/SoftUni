package p008_Military_Elite.implementation;

import p008_Military_Elite.service.Spy;

public class SpyImpl extends SoldierImpl implements Spy {
    private String codeNumber;



    public SpyImpl(String id, String firstName, String lastName, String codeNumber) {
        super(id, firstName, lastName);
        this.setCodeNumber(codeNumber);
    }

    private void setCodeNumber(String codeNumber) {
        this.codeNumber = codeNumber;
    }

    @Override
    public String getCodeNumber() {
        return this.codeNumber;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append((String.format("Name: %s %s Id: %s", super.getFirstName(), super.getLastName(), super.getId()))).append(System.lineSeparator())
                .append(String.format("Code Number: %s", this.codeNumber));

        return sb.toString();
    }
}
