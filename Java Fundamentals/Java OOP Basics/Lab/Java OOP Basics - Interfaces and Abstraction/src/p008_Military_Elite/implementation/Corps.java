package p008_Military_Elite.implementation;

public class Corps {
    public static final String AIRFORCES = "Airforces";
    public static final String MARINES = "Marines";

    private String name;

    public Corps(String name) {
        this.setName(name);
    }

    protected String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }
}
