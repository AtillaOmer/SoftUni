package p008_Military_Elite.implementation;

import p008_Military_Elite.service.Commando;

import java.util.List;
import java.util.Set;

public class CommandoImpl extends SpecialisedSoldierImpl implements Commando {
    private Set<Mission> missions;


    public CommandoImpl(String id, String firstName, String lastName, Double salary, Corps corps, Set<Mission> missions) {
        super(id, firstName, lastName, salary, corps);
        this.missions = missions;
    }

    @Override
    public Set<Mission> getMissions() {
        return this.missions;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(String.format("Name: %s %s Id: %s Salary: %.2f", super.getFirstName(), super.getLastName(), super.getId(), super.getSalary())).append(System.lineSeparator())
                .append(String.format("Corps: %s", this.getCorps().getName())).append(System.lineSeparator())
                .append("Missions:");
        for (Mission mission : missions) {
            sb.append(System.lineSeparator())
                    .append("  " + mission);
        }

        return sb.toString();
    }
}
