package p008_Military_Elite.implementation;

import p008_Military_Elite.service.Private;

public class PrivateImpl extends SoldierImpl implements Private {
    private Double salary;

    public PrivateImpl(String id, String firstName, String lastName, Double salary) {
        super(id, firstName, lastName);
        this.setSalary(salary);
    }

    private void setSalary(Double salary) {
        this.salary = salary;
    }

    @Override
    public Double getSalary() {
        return this.salary;
    }

    @Override
    public String toString() {
        return String.format("Name: %s %s Id: %s Salary: %.2f", super.getFirstName(), super.getLastName(), super.getId(), this.getSalary());
    }
}
