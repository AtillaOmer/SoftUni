package p008_Military_Elite.implementation;

import p008_Military_Elite.service.SpecialisedSoldier;

public abstract class SpecialisedSoldierImpl extends PrivateImpl implements SpecialisedSoldier {
    private Corps corp;


    protected SpecialisedSoldierImpl(String id, String firstName, String lastName, Double salary, Corps corps) {
        super(id, firstName, lastName, salary);
        this.corp = corps;
    }


    @Override
    public Corps getCorps() {
        return this.corp;
    }
}
