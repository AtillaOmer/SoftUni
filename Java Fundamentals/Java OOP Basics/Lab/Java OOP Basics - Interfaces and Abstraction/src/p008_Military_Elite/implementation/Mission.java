package p008_Military_Elite.implementation;

public class Mission {
    public static final String IN_PROGRESS = "inProgress";
    public static final String FINISHED = "Finished";

    private String codeName;
    private String state;

    public Mission() {
    }

    public Mission(String codeName, String state) {
        this.setCodeName(codeName);
        this.setState(state);
    }

    protected String getCodeName() {
        return codeName;
    }

    private void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public String getState() {
        return state;
    }

    private void setState(String state) {
        if (!FINISHED.equals(state) && !IN_PROGRESS.equals(state)) {
            throw new IllegalArgumentException();
        }
        this.state = state;
    }

    protected void CompleteMission() {
        this.state = FINISHED;
    }

    @Override
    public String toString() {
        return String.format("Code Name: %s State: %s", this.getCodeName(), this.getState());
    }
}
