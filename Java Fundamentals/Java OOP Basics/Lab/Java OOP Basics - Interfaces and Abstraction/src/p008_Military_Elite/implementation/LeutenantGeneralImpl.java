package p008_Military_Elite.implementation;

import p008_Military_Elite.service.LeutenantGeneral;

import java.util.List;

public class LeutenantGeneralImpl extends PrivateImpl implements LeutenantGeneral {
    private List<String> privates;

    public LeutenantGeneralImpl(String id, String firstName, String lastName, Double salary, List<String> privatesList) {
        super(id, firstName, lastName, salary);
        this.privates = privatesList;
    }

    @Override
    public List<String> getPrivates() {
        return this.privates;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(String.format("Name: %s %s Id: %s Salary: %.2f",
                super.getFirstName(),
                super.getLastName(),
                super.getId(),
                super.getSalary())).append(System.lineSeparator())
                .append("Privates:");
        for (String aPrivate : privates) {
            sb.append(System.lineSeparator())
                    .append("  " + aPrivate);
        }

        return sb.toString();
    }
}
