package p008_Military_Elite.implementation;

import p008_Military_Elite.service.Enginer;

import java.util.List;

public class EnginerImpl extends SpecialisedSoldierImpl implements Enginer {
    private List<String> repairs;

    public EnginerImpl(String id, String firstName, String lastName, Double salary, Corps corps, List<String> repairs) {
        super(id, firstName, lastName, salary, corps);
        this.repairs = repairs;
    }

    @Override
    public List<String> getRepair() {
        return this.repairs;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append(String.format("Name: %s %s Id: %s Salary: %.2f", super.getFirstName(), super.getLastName(), super.getId(), super.getSalary())).append(System.lineSeparator())
                .append(String.format("Corps: %s", this.getCorps().getName())).append(System.lineSeparator())
                .append("Repairs:");
        for (String repair : repairs) {
            sb.append(System.lineSeparator())
                    .append("  " + repair);
        }

        return sb.toString();
    }
}
