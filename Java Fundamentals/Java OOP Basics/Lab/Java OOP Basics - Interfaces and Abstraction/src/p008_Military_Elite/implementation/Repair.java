package p008_Military_Elite.implementation;

public class Repair {
    private String name;
    private Integer hoursWork;

    public Repair(String name, Integer hoursWork) {
        this.setName(name);
        this.setHoursWork(hoursWork);
    }

    protected String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    protected Integer getHoursWork() {
        return hoursWork;
    }

    private void setHoursWork(Integer hoursWork) {
        this.hoursWork = hoursWork;
    }

    @Override
    public String toString() {
        return String.format("Part Name: %s Hours Worked: %d", this.getName(), this.getHoursWork());
    }
}
