package p004_Telephony;

class Smartphone implements Callable, Browsable {

    public Smartphone() {
    }

    @Override
    public String call(String phoneNumber) {
        if (!phoneNumber.matches("\\d+")) {
            throw new IllegalArgumentException("Invalid number!");
        }
        return String.format("Calling... %s", phoneNumber);
    }


    @Override
    public String browse(String webSite) {
        if (webSite.matches(".*\\d.*")) {
            throw new IllegalArgumentException("Invalid URL!");
        }
        return String.format("Browsing: %s!", webSite);
    }
}
