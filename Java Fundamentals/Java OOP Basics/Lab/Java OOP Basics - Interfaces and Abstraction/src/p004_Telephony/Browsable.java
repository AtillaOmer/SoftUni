package p004_Telephony;

public interface Browsable {
    String browse(String website);
}
