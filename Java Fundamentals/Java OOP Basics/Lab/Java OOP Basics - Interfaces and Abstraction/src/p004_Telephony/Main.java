package p004_Telephony;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        List<String> phoneNumbers = new ArrayList<>(Arrays.asList(reader.readLine().split(" ")));
        List<String> webSites = new ArrayList<>(Arrays.asList(reader.readLine().split(" ")));

        Smartphone smartphone = new Smartphone();

        phoneNumbers.stream().forEach(phoneNumber -> {
            try{
                System.out.println(smartphone.call(phoneNumber));
            } catch (IllegalArgumentException iae) {
                System.out.println(iae.getMessage());
            }
        });

        webSites.stream().forEach(webSite -> {
            try{
                System.out.println(smartphone.browse(webSite));
            } catch (IllegalArgumentException iae) {
                System.out.println(iae.getMessage());
            }
        });
    }
}
