package p004_Telephony;

public interface Callable {
    String call(String phoneNumbers);
}
