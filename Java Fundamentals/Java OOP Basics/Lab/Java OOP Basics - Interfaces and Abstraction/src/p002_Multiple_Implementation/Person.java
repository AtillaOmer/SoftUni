package p002_Multiple_Implementation;

public interface Person {
    String getName();

    Integer getAge();

}
