package p002_Multiple_Implementation;

public interface Identifiable {
    String getId();
}
