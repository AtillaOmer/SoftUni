package p002_Multiple_Implementation;

public class Citizen implements Person, Identifiable, Birthable {
    private String name;
    private Integer age;
    private String id;
    private String birthdate;


    protected Citizen(String name, Integer age, String id, String birthdate) {
        this.setName(name);
        this.setAge(age);
        this.setId(id);
        this.setBirthdate(birthdate);
    }

    protected void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    protected void setAge(Integer age) {
        this.age = age;
    }

    @Override
    public Integer getAge() {
        return this.age;
    }

    @Override
    public String getId() {
        return null;
    }

    protected void setId(String id) {
        this.id = id;
    }

    @Override
    public String getBirthdate() {
        return this.birthdate;
    }

    protected void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }
}
