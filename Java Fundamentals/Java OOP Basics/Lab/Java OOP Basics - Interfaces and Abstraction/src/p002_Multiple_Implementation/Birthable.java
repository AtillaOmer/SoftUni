package p002_Multiple_Implementation;

public interface Birthable {
    String getBirthdate();
}
