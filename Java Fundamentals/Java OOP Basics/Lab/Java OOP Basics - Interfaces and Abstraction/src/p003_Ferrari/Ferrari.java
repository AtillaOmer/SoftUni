package p003_Ferrari;

public class Ferrari implements Car {
    protected static final String MODEL = "488-Spider";
    private String driverName;

    public Ferrari(String driverName) {
        this.driverName = driverName;
    }

    protected String getDriverName() {
        return driverName;
    }

    protected void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    @Override
    public String useBreak() {
        return "Brakes!";
    }

    @Override
    public String pushGas() {
        return "Zadu6avam sA!";
    }

    public static String getMODEL() {
        return MODEL;
    }

    @Override
    public String toString() {
        return String.format("%s/%s/%s/%s", Ferrari.getMODEL(), this.useBreak(), this.pushGas(), this.getDriverName());
    }
}
