package p003_Ferrari;

public interface Car {
    String useBreak();

    String pushGas();
}
