package p006_Birthday_Celebrations;

public class Citizen implements Birthday {
    private String name;
    private Integer age;
    private String id;
    private String birthday;

    public Citizen(String name, Integer age, String id, String birthdate) {
        this.name = name;
        this.age = age;
        this.id = id;
        this.birthday = birthdate;
    }

    @Override
    public String getBirthday() {
        return this.birthday;
    }
}
