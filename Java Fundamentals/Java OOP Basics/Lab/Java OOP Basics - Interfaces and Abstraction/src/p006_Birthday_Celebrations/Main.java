package p006_Birthday_Celebrations;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        List<Birthday> objects = new ArrayList<>();
        String input;
        while (true) {
            if ("End".equals(input = reader.readLine()))
                break;

            String[] tokens = input.split("\\s+");

            Birthday obj = null;
            switch (tokens[0]) {
                case "Citizen":
                    obj = new Citizen(tokens[1], Integer.parseInt(tokens[2]), tokens[3], tokens[4]);
                    objects.add(obj);
                    break;
                case "Pet":
                    obj = new Pet(tokens[1], tokens[2]);
                    objects.add(obj);
                    break;
                default:
                    break;
            }
        }

        String birthday = reader.readLine();

        objects.stream().filter( obj -> obj.getBirthday().endsWith(birthday))
                .forEach(unit -> {
                    System.out.println(unit.getBirthday());
                });
    }
}
