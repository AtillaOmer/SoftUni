package p006_Birthday_Celebrations;

public class Robot implements Birthday {
    private String model;
    private String id;

    public Robot(String model, String id) {
        this.model = model;
        this.id = id;
    }

    @Override
    public String getBirthday() {
        return this.id;
    }
}
