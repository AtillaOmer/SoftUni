package p005_Border_Control;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        List<Initialable> units = new ArrayList<>();
        String input;
        while (true){
            if ("End".equals(input = reader.readLine()))
                break;

            String[] tokens = input.split("\\s+");

            Initialable unit = null;
            if (tokens.length == 2){
                unit = new Robot(tokens[0], tokens[1]);
            }
            if (tokens.length == 3){
                unit = new Citizen(tokens[0], Integer.parseInt(tokens[1]), tokens[2]);
            }

            units.add(unit);
        }

        String endWith = reader.readLine();

        units.stream().filter(unit -> unit.getId().endsWith(endWith))
                .forEach(unit -> {
                    System.out.println(unit.getId());
                });
    }
}
