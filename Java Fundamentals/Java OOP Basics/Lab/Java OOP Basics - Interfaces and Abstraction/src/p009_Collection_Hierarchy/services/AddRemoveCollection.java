package p009_Collection_Hierarchy.services;

public interface AddRemoveCollection extends AddCollection{
    String remove();
}
