package p009_Collection_Hierarchy.implementation;

import p009_Collection_Hierarchy.services.AddCollection;

import java.util.ArrayList;
import java.util.List;

public class AddCollectionImpl implements AddCollection {
    private List<String> list;


    public AddCollectionImpl() {
        this.list = new ArrayList<>();
    }

    public AddCollectionImpl(List<String> list) {
        this.setList(list);
    }


    protected List<String> getList() {
        return list;
    }

    protected void setList(List<String> list) {
        this.list = list;
    }

    @Override
    public Integer add(String element) {
        this.list.add(element);
        
        return list.lastIndexOf(element);
    }
}
