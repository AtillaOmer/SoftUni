package p009_Collection_Hierarchy.implementation;

import p009_Collection_Hierarchy.services.AddRemoveCollection;

public class AddRemoveCollectionImpl extends AddCollectionImpl implements AddRemoveCollection {

    public AddRemoveCollectionImpl() {
    }

    @Override
    public Integer add(String element) {
        super.getList().add(0, element);

        return super.getList().indexOf(element);
    }

    @Override
    public String remove() {
        int lastIndex = super.getList().size() - 1;
        String removedElement = super.getList().get(lastIndex);
        super.getList().remove(lastIndex);
        return removedElement;
    }
}
