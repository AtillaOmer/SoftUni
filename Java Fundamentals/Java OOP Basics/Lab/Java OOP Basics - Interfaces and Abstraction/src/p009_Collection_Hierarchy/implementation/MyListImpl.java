package p009_Collection_Hierarchy.implementation;

import p009_Collection_Hierarchy.services.MyList;

public class MyListImpl extends AddRemoveCollectionImpl implements MyList {
    @Override
    public Integer used() {
        return super.getList().size();
    }

    @Override
    public Integer add(String element) {
        super.getList().add(0, element);

        return super.getList().indexOf(element);
    }

    @Override
    public String remove() {
        String removedElement = super.getList().get(0);
        super.getList().remove(0);
        return removedElement;
    }
}
