package p009_Collection_Hierarchy;

import p009_Collection_Hierarchy.implementation.AddCollectionImpl;
import p009_Collection_Hierarchy.implementation.AddRemoveCollectionImpl;
import p009_Collection_Hierarchy.implementation.MyListImpl;
import p009_Collection_Hierarchy.services.AddCollection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String[] words = reader.readLine().split("\\s+");
        Integer n = Integer.parseInt(reader.readLine());
        reader.close();

        AddCollection addCollection = new AddCollectionImpl();
        AddCollection addRemoveCollection = new AddRemoveCollectionImpl();
        AddCollection myList = new MyListImpl();

        addElements(words, addCollection, addRemoveCollection, myList);
        removeElements(n, (AddRemoveCollectionImpl) addRemoveCollection, (MyListImpl) myList);
    }

    private static void removeElements(Integer n, AddRemoveCollectionImpl addRemoveCollection, MyListImpl myList) {
        for (int i = 0; i < n; i++)
            System.out.print(addRemoveCollection.remove() + " ");
        System.out.println();
        for (int i = 0; i < n; i++)
            System.out.print(myList.remove() + " ");
    }

    private static void addElements(String[] words, AddCollection addCollection, AddCollection addRemoveCollection, AddCollection myList) {
        for (String word : words) {
            System.out.print(addCollection.add(word) + " ");
        }
        System.out.println();
        for (String word : words) {
            System.out.print(addRemoveCollection.add(word) + " ");
        }
        System.out.println();
        for (String word : words) {
            System.out.print(myList.add(word) + " ");
        }
        System.out.println();
    }
}
