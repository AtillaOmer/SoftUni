package p007_Food_Shortage;

public interface Buyer {

    void buyFood();

    Integer getFood();
}
