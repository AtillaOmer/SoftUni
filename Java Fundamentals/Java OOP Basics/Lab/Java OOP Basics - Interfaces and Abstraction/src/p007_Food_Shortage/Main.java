package p007_Food_Shortage;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Map<String, Buyer> buyers = new LinkedHashMap<>();

        Integer n = Integer.parseInt(reader.readLine());
        while (n-- > 0) {
            String[] tokens = reader.readLine().split("\\s+");
            String name = tokens[0];

            if (!buyers.containsKey(name)) {
                Buyer buyer = null;
                if (tokens.length == 4) {
                    buyer = new Citizen(name, Integer.parseInt(tokens[1]), tokens[2], tokens[3]);
                }
                if (tokens.length == 3) {
                    buyer = new Rebel(name, Integer.parseInt(tokens[1]), tokens[2]);
                }

                buyers.put(name, buyer);
            }
        }

        String input;
        while (true) {
            if ("End".equals(input = reader.readLine()))
                break;

            String name = input;

            if (buyers.containsKey(name)) {
                Buyer buyer = buyers.get(name);
                buyer.buyFood();
            }
        }

        System.out.println(buyers.entrySet().stream()
                .mapToInt(buyer -> buyer.getValue().getFood())
                .sum());

    }
}
