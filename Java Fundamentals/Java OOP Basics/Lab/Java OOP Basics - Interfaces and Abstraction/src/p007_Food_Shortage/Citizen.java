package p007_Food_Shortage;

public class Citizen implements Buyer {
    private String name;
    private Integer age;
    private String id;
    private String birthday;
    private Integer food;

    public Citizen(String name, Integer age, String id, String birthdate) {
        this.name = name;
        this.age = age;
        this.id = id;
        this.birthday = birthdate;
        this.food = 0;
    }


    @Override
    public void buyFood() {
        this.food += 10;
    }

    @Override
    public Integer getFood() {
        return this.food;
    }
}
