package p010_MooD_3;

import java.text.DecimalFormat;

public class Demon extends CharecterImpl {
    private Double energy;

    public Demon(String username, Integer level, Double energy) {
        super(username, level);
        this.energy = energy;
    }

    public Double getEnergy() {
        return energy;
    }

    public void setEnergy(Double energy) {
        this.energy = energy;
    }

    @Override
    public String toString() {
        DecimalFormat decimalFormat = new DecimalFormat("#.0#################");
        return String.format("%s%n%s", super.toString(), decimalFormat.format(this.getEnergy() * this.getLevel()));
    }
}