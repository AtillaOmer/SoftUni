package p010_MooD_3;

public class Archangel extends CharecterImpl {
    private Integer mana;

    public Archangel(String username, Integer level, Integer mana) {
        super(username, level);
        this.setMana(mana);
    }

    protected Integer getMana() {
        return mana;
    }

    protected void setMana(Integer mana) {
        this.mana = mana;
    }

    @Override
    public String toString() {
        return String.format("%s%n%s", super.toString(), this.getMana() * this.getLevel());
    }
}
