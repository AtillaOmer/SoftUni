package p010_MooD_3;

public class CharecterImpl implements Charecter {
    public static final String ARCHANGEL = "Archangel";
    public static final String DEMON = "Demon";

    private String username;
    private String hashedPassword;
    private Integer level;

    protected CharecterImpl(String username, Integer level) {
        this.setUsername(username);
        this.setLevel(level);
        this.setHashedPassword(this);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getHashedPassword() {
        return hashedPassword;
    }

    public <T> void setHashedPassword(T character) {

        if (character.getClass().getSimpleName().equalsIgnoreCase(ARCHANGEL)) {
            StringBuilder sb = new StringBuilder(this.username);
            sb.reverse();
            sb.append(this.username.length() * 21);
            this.hashedPassword = sb.toString();
        } else if (character.getClass().getSimpleName().equalsIgnoreCase(DEMON)) {
            this.hashedPassword = String.valueOf(this.username.length() * 217);
        }
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return String.format("\"%s\" | \"%s\" -> %s", this.getUsername(), this.getHashedPassword(), this.getClass().getSimpleName());
    }
}
