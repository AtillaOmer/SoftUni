package p010_MooD_3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static final String ARCHANGEL = "Archangel";
    public static final String DEMON = "Demon";

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        String[] tokens = reader.readLine().split(" \\| ");
        String username = tokens[0];
        String characterType = tokens[1];
        Integer level = Integer.parseInt(tokens[3]);

        Charecter charecter = null;
        switch (characterType){
            case ARCHANGEL:
                Integer mana = Integer.parseInt(tokens[2]);
                charecter = new Archangel(username, level, mana);
                break;
            case DEMON:
                Double energy = Double.parseDouble(tokens[2]);
                charecter = new Demon(username, level, energy);
                break;
        }

        System.out.println(charecter);
    }
}
