package exam.core;

import exam.controller.WarManager;
import exam.factories.AntiHeroFactory;
import exam.factories.ArenaFactory;
import exam.factories.HeroFactory;
import exam.factories.PowerFactory;
import exam.interfaces.InputReader;
import exam.interfaces.Manager;
import exam.interfaces.OutputWriter;
import exam.io.ConsoleReader;
import exam.io.ConsoleWriter;
import exam.utils.Commands;
import exam.utils.Constants;

import java.io.IOException;

public class Engine {
    InputReader reader;
    OutputWriter writer;
    Manager manager;

    public Engine() {
        this.reader = new ConsoleReader();
        this.writer = new ConsoleWriter();
        this.manager = new WarManager();
    }


    public void run() throws IOException {



        while (true) {
            try {
                String[] args = this.reader.readLine().split(" ");
                String result = null;
                String command = args[0];

                if ("WAR_IS_OVER".equals(command))
                    break;

                switch (command) {
                    case Commands.CHECK_CHARACTER:

                        String characterName = args[1];
                        result = this.manager.checkComicCharacter(characterName);
                        if (!Constants.EMPTY_STRING.equals(result)) {
                            this.writer.writeLine(result);
                        }
                        break;

                    case Commands.REGISTER_HERO:

                        String heroName = args[1];
                        String type = args[2];
                        int energy = Integer.parseInt(args[3]);
                        double health = Double.parseDouble(args[4]);
                        double intelligence = Double.parseDouble(args[5]);
                        double heroism = Double.parseDouble(args[6]);

                        result = this.manager.addHero(HeroFactory.createHero(heroName, type, energy, health, intelligence, heroism));
                        if (!Constants.EMPTY_STRING.equals(result)) {
                            this.writer.writeLine(result);
                        }
                        break;
                    case Commands.REGISTER_ANTI_HERO:

                        heroName = args[1];
                        type = args[2];
                        energy = Integer.parseInt(args[3]);
                        health = Double.parseDouble(args[4]);
                        intelligence = Double.parseDouble(args[5]);
                        double evilness = Double.parseDouble(args[6]);

                        result = this.manager.addAntiHero(AntiHeroFactory.createAntiHero(heroName, type, energy, health, intelligence, evilness));
                        if (!Constants.EMPTY_STRING.equals(result)) {
                            this.writer.writeLine(result);
                        }
                        break;
                    case Commands.BUILD_ARENA:
                        String arenaName = args[1];
                        int capacity = Integer.parseInt(args[2]);

                        result = this.manager.addArena(ArenaFactory.createArena(arenaName, capacity));
                        if (!Constants.EMPTY_STRING.equals(result) || result != null) {
                            this.writer.writeLine(result);
                        }
                        break;
                    case Commands.SEND_HERO:
                        arenaName = args[1];
                        heroName = args[2];
                        result = this.manager.addHeroToArena(arenaName, heroName);
                        if (!Constants.EMPTY_STRING.equals(result) || result != null) {
                            this.writer.writeLine(result);
                        }
                        break;
                    case Commands.SEND_ANTI_HERO:
                        arenaName = args[1];
                        heroName = args[2];
                        result = this.manager.addAntiHeroToArena(arenaName, heroName);
                        if (!Constants.EMPTY_STRING.equals(result) || result != null) {
                            this.writer.writeLine(result);
                        }
                        break;
                    case Commands.SUPER_POWER:
                        String superPowerName = args[1];
                        double powerPoints = Double.parseDouble(args[2]);

                        result = this.manager.loadSuperPowerToPool(PowerFactory.createPower(superPowerName, powerPoints));
                        if (!Constants.EMPTY_STRING.equals(result) || result != null) {
                            this.writer.writeLine(result);
                        }
                        break;
                    case Commands.ASSIGN_POWER:

                        String comicCharacterName = args[1];
                        superPowerName = args[2];
                        result = this.manager.assignSuperPowerToComicCharacter(comicCharacterName, superPowerName);
                        if (!Constants.EMPTY_STRING.equals(result) || result != null) {
                            this.writer.writeLine(result);
                        }
                        break;
                    case Commands.UNLEASH:
                        characterName = args[1];
                        result = this.manager.usePowers(characterName);
                        if (!Constants.EMPTY_STRING.equals(result) || result != null) {
                            this.writer.writeLine(result);
                        }
                        break;
                    case Commands.COMICS_WAR:
                        arenaName = args[1];
                        result = this.manager.startBattle(arenaName);
                        if (!Constants.EMPTY_STRING.equals(result) || result != null) {
                            this.writer.writeLine(result);
                        }
                        break;
                    case Commands.WAR_IS_OVER:
                        break;
                }
            } catch (IllegalArgumentException iae){
                this.writer.writeLine(iae.getMessage());
            }
        }
    }
}
