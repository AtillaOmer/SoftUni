package exam.controller;

import exam.entities.arenas.ArenaImpl;
import exam.entities.charcters.ComicCharacterImpl;
import exam.entities.charcters.antiheroes.AntiHero;
import exam.entities.charcters.heroes.Hero;
import exam.interfaces.Arena;
import exam.interfaces.ComicCharacter;
import exam.interfaces.Manager;
import exam.interfaces.SuperPower;

import java.util.LinkedHashMap;
import java.util.Map;

public class WarManager implements Manager {
    private Map<String, ComicCharacter> characters;
    private Map<String, ComicCharacter> heroes;
    private Map<String, ComicCharacter> antiHeroes;
    private Map<String, Arena> arenas;
    private Map<String, SuperPower> powers;
    private int warCounter;

    public WarManager() {
        this.characters = new LinkedHashMap<>();
        this.heroes = new LinkedHashMap<>();
        this.antiHeroes = new LinkedHashMap<>();
        this.arenas = new LinkedHashMap<>();
        this.powers = new LinkedHashMap<>();
        this.warCounter = 0;
    }

    @Override
    public String checkComicCharacter(String characterName) {
        if (characters.containsKey(characterName)) {
            ComicCharacter character = characters.get(characterName);
            if (character.getHealth() < 0) {
                return String.format("%s has fallen in battle!", character.getName());
            } else {
                return character.toString();
            }
        } else {

            return String.format("Sorry, fans! %s doesn't exist in our comics!", characterName);
        }
    }

    @Override
    public String addHero(ComicCharacter hero) {
        if (!characters.containsKey(hero.getName())) {
            characters.put(hero.getName(), hero);
            heroes.put(hero.getName(), hero);

            return String.format("%s is ready for battle!", hero.getName());
        } else {
            characters.get(hero.getName()).evolve(hero.getEnergy(), hero.getHealth(), hero.getIntelligence());
            return String.format("%s evolved!", hero.getName());
        }
    }

    @Override
    public String addAntiHero(ComicCharacter antiHero) {
        if (!characters.containsKey(antiHero.getName())) {
            characters.put(antiHero.getName(), antiHero);
            antiHeroes.put(antiHero.getName(), antiHero);

            return String.format("%s is ready for destruction!", antiHero.getName());
        } else {
            characters.get(antiHero.getName()).evolve(antiHero.getEnergy(), antiHero.getHealth(), antiHero.getIntelligence());
            return String.format("%s is getting stronger!", characters.get(antiHero.getName()));
        }
    }

    @Override
    public String addArena(Arena arena) {
        if (arenas.containsKey(arena.getArenaName())) {
            return "A battle is about to start there!";
        } else {
            arenas.put(arena.getArenaName(), arena);
            return String.format("%s is becoming a fighting ground!", arena.getArenaName());
        }
    }

    @Override
    public String addHeroToArena(String arena, String hero) {
        if (arenas.containsKey(arena)) {
            if (heroes.containsKey(hero)) {
                if (arenas.get(arena) instanceof ArenaImpl) {

                    ArenaImpl arena1 = (ArenaImpl) arenas.get(arena);
                    if (arena1.getHeroes().containsKey(hero)) {
                        if (arena1.getAntiHeroes().get(hero).getHealth() <= 0)
                            return String.format("%s is dead!", arena1.getAntiHeroes().get(hero).getName());
                        return String.format("%s is fighting!", arena1.getAntiHeroes().get(hero).getName());
                    } else {
                        if (arena1.isArenaFull()) {
                            return "Arena is full!";
                        } else {
                            arenas.get(arena).addHero(heroes.get(hero));

                            return String.format("%s is fighting for your freedom in %s!",
                                    heroes.get(hero).getName(), arenas.get(arena).getArenaName());
                        }
                    }
                }
            }
        }
        return null;
    }

    @Override
    public String addAntiHeroToArena(String arena, String antiHero) {
        if (arenas.containsKey(arena)) {
            if (antiHeroes.containsKey(antiHero)) {
                if (arenas.get(arena) instanceof ArenaImpl) {

                    ArenaImpl arena1 = (ArenaImpl) arenas.get(arena);
                    if (arena1.getAntiHeroes().containsKey(antiHero)) {
                        if (arena1.getAntiHeroes().get(antiHero).getHealth() <= 0)
                            return String.format("%s is dead!", arena1.getAntiHeroes().get(antiHero).getName());
                        return String.format("%s is fighting!", arena1.getAntiHeroes().get(antiHero).getName());
                    } else {
                        if (arena1.isArenaFull()) {
                            return "Arena is full!";
                        } else {
                            arenas.get(arena).addAntiHero(antiHeroes.get(antiHero));

                            return String.format("%s is fighting for your freedom in %s!",
                                    antiHeroes.get(antiHero).getName(), arenas.get(arena).getArenaName());
                        }
                    }
                }
            }
        }
        return null;
    }

    @Override
    public String loadSuperPowerToPool(SuperPower superPower) {
        if (powers.containsKey(superPower.getName())) {
            return "This super power already exists!";
        } else {
            powers.put(superPower.getName(), superPower);
            return String.format("%s added to pool!", superPower.getName());
        }
    }

    @Override
    public String assignSuperPowerToComicCharacter(String comicCharacter, String superPower) {
        if (characters.containsKey(comicCharacter)) {
            if (characters.get(comicCharacter) instanceof ComicCharacterImpl) {
                ComicCharacterImpl character = (ComicCharacterImpl) characters.get(comicCharacter);
                if (character.getPowers().containsKey(superPower)) {
                    return String.format("%s already assigned!", superPower);
                } else {
                    characters.get(comicCharacter).addSuperPower(powers.get(superPower));
                    powers.remove(superPower);
                    return String.format("%s has a new super power!", characters.get(comicCharacter).getName());
                }
            }
        }
        return null;
    }

    @Override
    public String usePowers(String characterName) {
        if (characters.containsKey(characterName)) {
            if (characters.get(characterName) instanceof ComicCharacterImpl) {
                ComicCharacterImpl comicCharacter = (ComicCharacterImpl) characters.get(characterName);
                if (comicCharacter.getPowers().size() == 0) {
                    return String.format("%s has no super powers!", comicCharacter.getName());
                } else {
                    return String.format("%s used his super powers!", comicCharacter.getName());
                }
            }
        }
        return null;
    }

    @Override
    public String startBattle(String arena) {
        if (arenas.containsKey(arena)) {
            if (arenas.get(arena) instanceof ArenaImpl) {
                ArenaImpl arena1 = (ArenaImpl) arenas.get(arena);
                if (arena1.getCapacity() <= 0) {
                    return "SAFE ZONE!";
                } else {
                    arenas.get(arena).fightHeroes();
                }
            }
        }
        return null;
    }

    @Override
    public String endWar() {
        return null;
    }
}
