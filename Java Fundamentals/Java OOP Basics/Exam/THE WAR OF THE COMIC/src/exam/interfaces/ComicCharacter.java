package exam.interfaces;

public interface ComicCharacter {

    //The Comic Character health is reduce from the damage
    void takeDamage(double damage);

    //Changes Comic Character energy,health and inteligence with the new values but first you have to see if they are correct.
    void boostCharacter(int energy, double health, double inteligence);

    //Returns the Comic Character name
    String getName();

    //Returns the Comic Character energy
    int getEnergy();


    //Returns the Comic Character health
    double getHealth();

    //Returns the Comic Character intelligence
    double getIntelligence();

    //Add superPower in collections.
    void addSuperPower(SuperPower superPower);


    //    If the Comic character doesn't have a super power , return "<comic character name> has no super powers!"
    //    If the character has powers , iterate over the colection and for every power:
    //       - add to characters energy the Super power points;
    //	     - add to characters health the Super power points multiplied by two;
    //    Else returns "<comic character name> used his super powers!"
    String useSuperPowers();

    void evolve(int energy, double health, double intelligence);
}
