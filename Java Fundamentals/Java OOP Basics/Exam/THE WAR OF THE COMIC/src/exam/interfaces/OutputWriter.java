package exam.interfaces;

public interface OutputWriter {
    void writeLine(String line);
}
