package exam.interfaces;

public interface SuperPower {
    String getName();

    double getPowerPoints();

}
