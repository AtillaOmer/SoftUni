package exam.factories;

import exam.entities.arenas.ArenaImpl;
import exam.interfaces.Arena;

public class ArenaFactory {
    public ArenaFactory() {
    }

    public static Arena createArena(String arenaName, int capacity) {
        return new ArenaImpl(arenaName, capacity);
    }
}
