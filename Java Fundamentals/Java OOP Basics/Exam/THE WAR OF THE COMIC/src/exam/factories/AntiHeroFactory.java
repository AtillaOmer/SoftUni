package exam.factories;

import exam.entities.charcters.antiheroes.AntiHero;
import exam.entities.charcters.antiheroes.Titan;
import exam.entities.charcters.antiheroes.Villain;
import exam.utils.AntiHeroTypes;

public class AntiHeroFactory {
    public AntiHeroFactory() {
    }

    public static AntiHero createAntiHero(String heroName, String type, int energy, double health, double intelligence, double evilness) {
        AntiHero character = null;
        switch (type) {
            case AntiHeroTypes.VILLAIN:
                character = new Villain(heroName, energy, health, intelligence, evilness);
                break;
            case AntiHeroTypes.TITAN:
                character = new Titan(heroName, energy, health, intelligence, evilness);
                break;
        }
        return character;
    }
}
