package exam.factories;

import exam.entities.charcters.heroes.DCHero;
import exam.entities.charcters.heroes.Hero;
import exam.entities.charcters.heroes.MarvelHero;
import exam.interfaces.ComicCharacter;
import exam.utils.HeroTypes;

public class HeroFactory {
    public HeroFactory() {
    }

    public static Hero createHero(String heroName, String type, int energy, double health, double intelligence, double heroism) {
        Hero character = null;
        switch (type) {
            case HeroTypes.MARVEL_HERO:
                character = new MarvelHero(heroName, energy, health, intelligence, heroism);
                break;
            case HeroTypes.DC_HERO:
                character = new DCHero(heroName, energy, health, intelligence, heroism);
                break;
        }
        return character;
    }
}
