package exam.factories;

import exam.entities.Power;

public class PowerFactory {
    public PowerFactory() {
    }

    public static Power createPower(String powerName, double powerPoints) {
        return new Power(powerName, powerPoints);
    }
}
