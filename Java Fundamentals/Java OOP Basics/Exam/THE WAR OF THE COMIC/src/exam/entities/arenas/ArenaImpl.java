package exam.entities.arenas;

import exam.interfaces.Arena;
import exam.interfaces.ComicCharacter;

import java.util.LinkedHashMap;
import java.util.Map;

public class ArenaImpl implements Arena {
    private String arenaName;
    private Map<String, ComicCharacter> heroes;
    private Map<String, ComicCharacter> antiHeroes;
    private int capacity;

    public ArenaImpl(String arenaName, int capacity) {
        this.arenaName = arenaName;
        this.capacity = capacity;
        this.heroes = new LinkedHashMap<>();
        this.antiHeroes = new LinkedHashMap<>();
    }

    @Override
    public String getArenaName() {
        return this.arenaName;
    }

    public Map<String, ComicCharacter> getHeroes() {
        return heroes;
    }

    public Map<String, ComicCharacter> getAntiHeroes() {
        return antiHeroes;
    }

    public int getCapacity() {
        return capacity;
    }

    @Override
    public boolean isArenaFull() {
        if (heroes.size() + antiHeroes.size() < capacity) {
            return false;
        } else
            return true;
    }


    @Override
    public void addHero(ComicCharacter hero) {
        this.heroes.put(hero.getName(), hero);
    }

    @Override
    public void addAntiHero(ComicCharacter antiHero) {
        this.antiHeroes.put(antiHero.getName(), antiHero);
    }

    @Override
    public boolean fightHeroes() {
        return false;
    }
}
