package exam.entities;

import exam.interfaces.SuperPower;

public class Power implements SuperPower {
    private String name;
    private double powerPoints;

    public Power(String name, double powerPoints) {
        this.setName(name);
        this.setPowerPoints(powerPoints);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (!name.matches("^@\\w+@$")) {
            throw new IllegalArgumentException("Super power name not in the correct format!");
        }
        this.name = name;
    }

    public double getPowerPoints() {
        return powerPoints;
    }

    public void setPowerPoints(double powerPoints) {
        if (powerPoints < 1) {
            throw new IllegalArgumentException("Power points should be a possitive number!");
        }
        this.powerPoints = powerPoints;
    }
}
