package exam.entities.charcters.antiheroes;

import exam.entities.charcters.ComicCharacterImpl;

public abstract class AntiHero extends ComicCharacterImpl {
    private double evilness;

    public AntiHero(String name, int energy, double health, double intelligence, double evilness) {
        super(name, energy, health, intelligence);
        this.setEvilness(evilness);
    }

    public double getEvilness() {
        return evilness;
    }

    private void setEvilness(double evilness) {
        if (evilness < 1) {
            throw new IllegalArgumentException("Evilness should be a positive number!");
        }
        this.evilness = evilness;
    }

    @Override
    public void evolve(int energy, double health, double intelligence) {
        this.setEnergy(this.getEnergy() + energy);
        this.setHealth(this.getHealth() + health);
        this.setIntelligence(this.getIntelligence() + intelligence);
    }

    @Override
    protected abstract double getSpecial();

    @Override
    public void takeDamage(double damage) {

    }

    @Override
    public void boostCharacter(int energy, double health, double inteligence) {

    }

    @Override
    public String useSuperPowers() {
        return null;
    }

    @Override
    public String toString() {
        return String.format("%s%n###Evilness: %.2f", super.toString(), this.getEvilness());
    }
}
