package exam.entities.charcters.heroes;

import exam.interfaces.SuperPower;

public class DCHero extends Hero {
    public DCHero(String name, int energy, double health, double intelligence, double heroism) {
        super(name, energy, health, intelligence, heroism);
    }

    @Override
    double attack() {
        return 0;
    }

    @Override
    protected double getSpecial() {
        return 0;
    }

    //DCHero: attack power  = energy / 1.5 + special + intelligence.
    @Override
    protected double getAttackPower() {
        return this.getEnergy() / 1.5 + this.getSpecial() + this.getIntelligence();
    }

    @Override
    public void takeDamage(double damage) {

    }

    @Override
    public void boostCharacter(int energy, double health, double inteligence) {

    }

    @Override
    public void addSuperPower(SuperPower superPower) {

    }

    @Override
    public String useSuperPowers() {
        return null;
    }

    @Override
    public String toString() {
        return String.format("%s%n####DC Attack Power: %.2f", super.toString(), this.getAttackPower());
    }
}
