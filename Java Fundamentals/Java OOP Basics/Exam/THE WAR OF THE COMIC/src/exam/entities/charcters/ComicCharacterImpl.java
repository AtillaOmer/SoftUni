package exam.entities.charcters;

import exam.interfaces.ComicCharacter;
import exam.interfaces.SuperPower;

import java.util.*;

public abstract class ComicCharacterImpl implements ComicCharacter {
    private String name;
    private int energy;
    private double health;
    private double intelligence;
    private Map<String, SuperPower> powers;

    public ComicCharacterImpl(String name, int energy, double health, double intelligence) {
        this.setName(name);
        this.setEnergy(energy);
        this.setHealth(health);
        this.setIntelligence(intelligence);
        this.powers = new LinkedHashMap<>();
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        if ((name.length() < 2 || name.length() > 12) || !name.matches("\\w+")) {
            throw new IllformedLocaleException("Comic Character name is not in the correct format!");
        }


        this.name = name;
    }

    public int getEnergy() {
        return energy;
    }

    protected void setEnergy(int energy) {
        if (energy < 0 || energy > 300) {
            throw new IllegalArgumentException("Energy is not in the correct range!");
        }

        this.energy = energy;
    }



    public double getHealth() {
        return health;
    }

    protected void setHealth(double health) {
        if (health < 1) {
            throw new IllegalArgumentException("Health should be a possitive number!");
        }
        this.health = health;
    }

    public double getIntelligence() {
        return intelligence;
    }

    protected void setIntelligence(double intelligence) {
        if (intelligence > 200) {
            throw new IllegalArgumentException("Intelligence is not in the correct range!");
        }
        this.intelligence = intelligence;
    }

    public Map<String, SuperPower> getPowers() {
        return powers;
    }

    //Returns the special of Hero/AntiHero => heroism/evilness
    protected abstract double getSpecial();

    protected abstract double getAttackPower();

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("#Name: {name}, this.getName()")).append(System.lineSeparator())
                .append(String.format("##Health: %.2f// Energy: %d// Intelligence: %.2f", this.getHealth(), this.getEnergy(), this.getIntelligence()));

        return stringBuilder.toString();
    }
}
