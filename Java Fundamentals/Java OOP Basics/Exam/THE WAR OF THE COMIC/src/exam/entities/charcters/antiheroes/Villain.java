package exam.entities.charcters.antiheroes;

import exam.interfaces.SuperPower;

public class Villain extends AntiHero {
    public Villain(String name, int energy, double health, double intelligence, double evilness) {
        super(name, energy, health, intelligence, evilness);
    }

    @Override
    protected double getSpecial() {
        return this.getEvilness();
    }

    @Override
    public void addSuperPower(SuperPower superPower) {

    }

    //Villain: attack power  = (intelligence * special) / energy
    @Override
    protected double getAttackPower() {
        return (this.getIntelligence() * this.getSpecial()) / this.getEnergy();
    }

    @Override
    public String toString() {
        return String.format("%s%n####Villain Attack Power: %.2f", super.toString(), this.getAttackPower());
    }
}
