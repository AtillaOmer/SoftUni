package exam.entities.charcters.heroes;

import exam.interfaces.SuperPower;

public class MarvelHero extends Hero {
    public MarvelHero(String name, int energy, double health, double intelligence, double heroism) {
        super(name, energy, health, intelligence, heroism);
    }

    @Override
    double attack() {
        return 0;
    }

    @Override
    protected double getSpecial() {
        return 0;
    }

    //•	MarvelHero: attack power = ((energy + special) * intelligence) / 2.5
    @Override
    protected double getAttackPower() {
        return ((this.getEnergy() + this.getSpecial()) * this.getIntelligence()) / 2.5;
    }

    @Override
    public void takeDamage(double damage) {

    }

    @Override
    public void boostCharacter(int energy, double health, double inteligence) {

    }

    @Override
    public void addSuperPower(SuperPower superPower) {

    }

    @Override
    public String useSuperPowers() {
        return null;
    }

    @Override
    public String toString() {
        return String.format("%s%n####Marvel Attack Power: %.2f", super.toString(), this.getAttackPower());
    }
}
