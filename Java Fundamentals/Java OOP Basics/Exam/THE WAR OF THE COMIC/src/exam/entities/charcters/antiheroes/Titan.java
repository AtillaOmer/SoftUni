package exam.entities.charcters.antiheroes;

import exam.interfaces.SuperPower;

public class Titan extends AntiHero {
    public Titan(String name, int energy, double health, double intelligence, double evilness) {
        super(name, energy, health, intelligence, evilness);
    }

    @Override
    protected double getSpecial() {
        return this.getEvilness();
    }

    @Override
    public void addSuperPower(SuperPower superPower) {

    }

    //Titan: attack power  = (energy + intelligence + special)  * 3
    @Override
    protected double getAttackPower() {
        return (this.getEnergy() + this.getIntelligence() + this.getSpecial()) * 3;
    }

    @Override
    public String toString() {
        return String.format("%s%n####Titan Attack Power: %.2f", super.toString(), this.getAttackPower());
    }
}
