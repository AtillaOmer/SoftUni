package exam.entities.charcters.heroes;

import exam.entities.charcters.ComicCharacterImpl;

public abstract class Hero extends ComicCharacterImpl {
    private double heroism;

    public Hero(String name, int energy, double health, double intelligence, double heroism) {
        super(name, energy, health, intelligence);
        this.setHeroism(heroism);
    }

    public double getHeroism() {
        return heroism;
    }

    private void setHeroism(double heroism) {
        if (heroism < 1) {
            throw new IllegalArgumentException("Heroism should be a positive number!");
        }
        this.heroism = heroism;
    }

    abstract double attack();

    @Override
    public void evolve(int energy, double health, double intelligence) {
        this.setEnergy(this.getEnergy() + energy);
        this.setHealth(this.getHealth() + health);
        this.setIntelligence(this.getIntelligence() + intelligence);
    }

    @Override
    public String toString() {
        return String.format("%s%n###Heroism: %.2f", super.toString(), this.getHeroism());
    }
}
