package exam.utils;

public class HeroTypes {
    public static final String MARVEL_HERO = "MarvelHero";
    public static final String DC_HERO = "DCHero";
}
