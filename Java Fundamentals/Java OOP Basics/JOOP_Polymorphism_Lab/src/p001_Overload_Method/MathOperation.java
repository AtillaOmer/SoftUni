package p001_Overload_Method;

public class MathOperation {
    public String add(int a, int b) {
        return String.format("%d", a + b);
    }

    public String add(int a, int b, int c) {
        return String.format("%d", a + b + c);
    }

    public String add(int a, int b, int c, int d) {
        return String.format("%d", a + b + c + d);
    }

}
