package p002_Method_Overriding;

public class Rectangle {
    protected Double sideA;
    protected Double sideB;

    public Rectangle(Double sideA, Double sideB) {
        this.setSideA(sideA);
        this.setSideB(sideB);
    }

    protected void setSideA(Double sideA) {
        this.sideA = sideA;
    }

    protected void setSideB(Double sideB) {
        this.sideB = sideB;
    }

    public double area() {
        return sideA * sideB;
    }
}
