package p002_Method_Overriding;

public class Square extends Rectangle {

    public Square(Double sideA) {
        super(sideA, sideA);
    }

    @Override
    public double area() {
        return this.sideA * this.sideA;
    }
}
