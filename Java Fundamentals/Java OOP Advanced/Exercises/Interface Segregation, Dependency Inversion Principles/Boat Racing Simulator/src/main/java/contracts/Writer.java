package contracts;

public interface Writer {
    void writeLine(String string);
}
