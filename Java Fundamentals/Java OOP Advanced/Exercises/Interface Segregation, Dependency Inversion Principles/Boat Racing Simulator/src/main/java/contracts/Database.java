package contracts;

import models.MotorBoat;

public interface Database {

    Repository<MotorBoat> getBoats();

    Repository<Modelable> getEngines();

}
