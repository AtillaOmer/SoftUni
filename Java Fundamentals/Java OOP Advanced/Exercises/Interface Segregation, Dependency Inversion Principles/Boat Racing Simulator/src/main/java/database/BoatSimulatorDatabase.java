package database;

import contracts.Database;
import contracts.Modelable;
import contracts.Repository;
import models.MotorBoat;

public class BoatSimulatorDatabase implements Database {
    private Repository<MotorBoat> boats;
    private Repository<Modelable> engines;

    public BoatSimulatorDatabase(Repository<MotorBoat> boatRepository, Repository<Modelable> engineRepository) {
        this.setBoats(boatRepository);
        this.setEngines(engineRepository);
    }

    @Override
    public Repository<MotorBoat> getBoats() {
        return this.boats;
    }


    private void setBoats(Repository<MotorBoat> boats) {
        this.boats = boats;
    }

    @Override
    public Repository<Modelable> getEngines() {
        return this.engines;
    }


    private void setEngines(Repository<Modelable> engines) {
        this.engines = engines;
    }
}
