package contracts;

import exeptions.DuplicateModelException;

public interface Executable {
    String execute() throws DuplicateModelException;
}
