package core;

import contracts.BoatSimulatorController;
import contracts.CommandHandler;
import contracts.Executable;
import exeptions.DuplicateModelException;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class CommandHandlerImpl implements CommandHandler {
    private static final String COMMANDS_PATH = "core.commands.";
    private static final String COMMAND_SUFFIX = "Command";

    private BoatSimulatorController controller;

    public CommandHandlerImpl(BoatSimulatorController controller) {
        this.setController(controller);
    }

    public BoatSimulatorController getController() {
        return this.controller;
    }

    private void setController(BoatSimulatorController controller) {
        this.controller = controller;
    }

    public String executeCommand(String name, List<String> parameters) {

        String commandClassName = COMMANDS_PATH + name + COMMAND_SUFFIX;
        try {
            Class<?> commandClass = Class.forName(commandClassName);
            Constructor<?> declaredConstructor = commandClass.getDeclaredConstructor(List.class, BoatSimulatorController.class);
            Executable command = (Executable) declaredConstructor.newInstance(parameters, this.getController());

            return command.execute();

        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException | DuplicateModelException e) {
            e.printStackTrace();
        }


//        switch (name) {
//            case "CreateBoatEngine":
//                EngineType engineType;
//                if (parameters.get(3).equals("Jet")) {
//                    return this.getController().CreateBoatEngine(
//                            parameters.get(0),
//                            Integer.parseInt(parameters.get(1)),
//                            Integer.parseInt(parameters.get(2)),
//                            "Jet");
//                } else if (parameters.get(3).equals("Sterndrive")) {
//                    return this.getController().CreateBoatEngine(
//                            parameters.get(0),
//                            Integer.parseInt(parameters.get(1)),
//                            Integer.parseInt(parameters.get(2)),
//                            "Sterndrive");
//                }
//
//                throw new IllegalArgumentException(Constants.IncorrectEngineTypeMessage);
//
//            case "CreateRowBoat":
//                return this.getController().CreateRowBoat(
//                        parameters.get(0),
//                        Integer.parseInt(parameters.get(1)),
//                        Integer.parseInt(parameters.get(2)));
//            case "CreateSailBoat":
//                return this.getController().CreateSailBoat(
//                        parameters.get(0),
//                        Integer.parseInt(parameters.get(1)),
//                        Integer.parseInt(parameters.get(2)));
//            case "CreatePowerBoat":
//                return this.getController().CreatePowerBoat(
//                        parameters.get(0),
//                        Integer.parseInt(parameters.get(1)),
//                        parameters.get(2),
//                        parameters.get(3));
//            case "CreateYacht":
//                return this.getController().CreateYacht(
//                        parameters.get(0),
//                        Integer.parseInt(parameters.get(1)),
//                        parameters.get(2),
//                        Integer.parseInt(parameters.get(3)));
//            case "OpenRace":
//                return this.getController().OpenRace(
//                        Integer.parseInt(parameters.get(0)),
//                        Integer.parseInt(parameters.get(1)),
//                        Integer.parseInt(parameters.get(2)),
//                        Boolean.parseBoolean(parameters.get(3)));
//            case "SignUpBoat":
//                return this.getController().SignUpBoat(parameters.get(0));
//            case "StartRace":
//                return this.getController().StartRace();
//            case "GetStatistic":
//                return this.getController().GetStatistic();
//            default:
//                throw new IllegalArgumentException();
//        }
        return null;
    }
}
