package core;

import contracts.*;
import contracts.Runnable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Engine implements Runnable {
    private Writer writer;
    private Reader reader;
    private CommandHandler commandHandler;

    public Engine(Writer writer, Reader reader, CommandHandler commandHandler) {
        this.writer = writer;
        this.reader = reader;
        this.commandHandler = commandHandler;
    }

    public CommandHandler getCommandHandler() {
        return commandHandler;
    }

    @Override
    public void run() throws IOException {
        while (true) {
            String line = reader.readLine();
            String name = "";
            List<String> parameters = new ArrayList<>();

            if (line.equals("End")) {
                break;
            }

            List<String> tokens = Arrays.asList(line.split("\\\\"));
            name = tokens.get(0);
            parameters = tokens.stream().skip(1).collect(Collectors.toList());

            try {
                String commandResult = this.getCommandHandler().executeCommand(name, parameters);
                writer.writeLine(commandResult);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }

            line = reader.readLine();
        }
    }
}
