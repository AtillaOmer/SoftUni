import contracts.*;
import contracts.Runnable;
import controllers.BoatSimulatorControllerImpl;
import core.CommandHandlerImpl;
import core.Engine;
import database.BoatSimulatorDatabase;
import database.RepositoryImpl;
import io.ConsoleReader;
import io.ConsoleWriter;
import models.MotorBoat;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        Writer writer = new ConsoleWriter();
        Reader reader = new ConsoleReader();
        Repository<MotorBoat> modelableRepository = new RepositoryImpl<>();
        Repository<Modelable> engineRepository = new RepositoryImpl<>();
        Database database = new BoatSimulatorDatabase(modelableRepository, engineRepository);
        BoatSimulatorController controller = new BoatSimulatorControllerImpl(database);
        CommandHandler commandHandler = new CommandHandlerImpl(controller);

        Runnable engine = new Engine(writer, reader, commandHandler);

        engine.run();
    }
}
