package core.commands;

import contracts.BoatSimulatorController;
import contracts.Database;
import contracts.Executable;
import enumeration.EngineType;
import exeptions.DuplicateModelException;

import java.util.List;

public class CreateBoatEngineCommand implements Executable {
    List<String> parameters;
    private BoatSimulatorController controller;

    public CreateBoatEngineCommand(List<String> parameters, BoatSimulatorController controller) {
        this.controller = controller;
        this.parameters = parameters;
    }

    public BoatSimulatorController getController() {
        return controller;
    }


    public List<String> getParameters() {
        return parameters;
    }


    @Override
    public String execute() throws DuplicateModelException {
        String model = this.getParameters().get(0);
        int horsepower = Integer.parseInt(this.getParameters().get(1));
        int displacement = Integer.parseInt(this.getParameters().get(2));
        EngineType engineType = Enum.valueOf(EngineType.class, this.getParameters().get(3));

        return this.getController().CreateBoatEngine(model, horsepower, displacement, engineType);
    }
}
