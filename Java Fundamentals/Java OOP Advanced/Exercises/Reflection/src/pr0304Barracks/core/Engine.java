package pr0304Barracks.core;

import jdk.jshell.spi.ExecutionControl;
import pr0304Barracks.contracts.*;
import pr0304Barracks.contracts.Runnable;
import pr0304Barracks.core.annotations.Inject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class Engine implements Runnable {
    public static final String COMMAND_PATH = "pr0304Barracks.core.commands.";
    private static final String COMMAND_SUFFIX = "Command";
    private Repository repository;
    private UnitFactory unitFactory;

    public Engine(Repository repository, UnitFactory unitFactory) {
        this.repository = repository;
        this.unitFactory = unitFactory;
    }

    @Override
    public void run() {
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(System.in));
        while (true) {
            try {
                String input = reader.readLine();
                String[] data = input.split("\\s+");
                String commandName = data[0];
                String result = interpredCommand(data, commandName);
                if (result.equals("fight")) {
                    break;
                }
                System.out.println(result);
            } catch (RuntimeException e) {
                System.out.println(e.getMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String interpredCommand(String[] data, String commandName) {
        String commandClassName = Character.toUpperCase(commandName.charAt(0)) + commandName.substring(1) + COMMAND_SUFFIX;
        try {
            Class<?> commandClass = Class.forName(COMMAND_PATH + commandClassName);
            Constructor<?> declaredConstructor = commandClass.getConstructor(String[].class);
            Executable command = (Executable) declaredConstructor.newInstance((Object) data);

            injectDependencies(command);
            return command.execute();
        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            return null;
        }
    }

    private <T> void injectDependencies(T source) throws IllegalAccessException {
        Field[] commandFields = source.getClass().getDeclaredFields();
        Field[] engineFields = Engine.class.getDeclaredFields();

        for (Field commandField : commandFields) {
            commandField.setAccessible(true);
            if (commandField.isAnnotationPresent(Inject.class)) {
                for (Field engineField : engineFields) {
                    engineField.setAccessible(true);
                    if (commandField.getType().equals(engineField.getType()) && commandField.getName().equals(engineField.getName())) {
                        commandField.set(source, engineField.get(this));
                    }
                }
            }
        }
    }
}
