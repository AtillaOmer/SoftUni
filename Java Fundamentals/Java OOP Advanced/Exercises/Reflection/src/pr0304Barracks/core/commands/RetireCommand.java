package pr0304Barracks.core.commands;

import jdk.jshell.spi.ExecutionControl;
import pr0304Barracks.contracts.Repository;
import pr0304Barracks.core.annotations.Inject;

public class RetireCommand extends Command {

    @Inject
    private Repository repository;

    public RetireCommand(String[] data) {
        super(data);
    }

    @Override
    public String execute() {
        String unitType = this.getData()[1];
        try {
            this.repository.removeUnit(unitType);
            String output = unitType + " retired!";
            return output;
        } catch (IllegalArgumentException | ExecutionControl.NotImplementedException iae) {
            return iae.getMessage();
        }
    }
}
