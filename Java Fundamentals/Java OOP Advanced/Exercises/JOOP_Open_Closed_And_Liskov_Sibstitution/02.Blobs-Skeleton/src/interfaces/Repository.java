package interfaces;

import models.Blob;

import java.util.List;

public interface Repository<T> {
    void addBlob(String name, Blob blob);

    String getStatus();

    T findByName(String name);

    List<T> findAll();

    void removeBlob(String name);
}
