package models.commands;

import core.annotations.Inject;
import interfaces.Repository;
import models.Blob;

public class AttackCommand extends AbstractCommand {

    @Inject
    private Repository<Blob> repository;

    public AttackCommand(String[] data) {
        super(data);
    }

    @Override
    public void execute() {
        String sourceBlobName = super.getData()[1];
        String targetBlobName = super.getData()[2];

        Blob source = this.repository.findByName(sourceBlobName);
        Blob target = this.repository.findByName(targetBlobName);

        source.attack(target);

    }
}
