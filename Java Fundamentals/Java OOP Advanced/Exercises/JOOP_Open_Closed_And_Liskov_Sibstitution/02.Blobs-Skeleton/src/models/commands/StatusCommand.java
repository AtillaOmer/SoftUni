package models.commands;

import core.annotations.Inject;
import interfaces.Repository;
import interfaces.Writer;
import models.Blob;

public class StatusCommand extends AbstractCommand {

    @Inject
    Writer writer;

    @Inject
    private Repository<Blob> repository;

    public StatusCommand(String[] data) {
        super(data);
    }

    @Override
    public void execute() {
        this.repository.findAll()
                .stream()
                .forEach(blob -> writer.writeLine(blob.toString()));
    }
}
