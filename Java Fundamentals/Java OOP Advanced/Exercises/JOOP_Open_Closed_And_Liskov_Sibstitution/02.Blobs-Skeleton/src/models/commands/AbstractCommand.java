package models.commands;

import interfaces.Executable;

public abstract class AbstractCommand implements Executable {
    private String[] data;

    public AbstractCommand(String[] data) {
        this.data = data;
    }

    public String[] getData() {
        return data;
    }
}
