package models.commands;

import core.annotations.Inject;
import interfaces.*;
import models.Blob;
import observers.Subject;
import utils.MutateBoolean;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class CreateCommand extends AbstractCommand {
    public static final String BEHAVIORS_PACKAGE_PATH = "models.behavors.";
    public static final String ATTACKS_PACKAGE_PATH = "models.attacks.";

    @Inject
    private Repository repository;

    @Inject
    private BlobFactory blobFactory;

    @Inject
    private Subject subject;

    @Inject
    private MutateBoolean mutateBoolean;

    public CreateCommand(String[] data) {
        super(data);
    }

    @Override
    public void execute() {
        String name = super.getData()[1];
        int health = Integer.parseInt(super.getData()[2]);
        int damage = Integer.parseInt(super.getData()[3]);
        String behaviorName = super.getData()[4];
        String attackName = super.getData()[5];

        Behavior behavior = null;
        Attack attack = null;

        try {
            Class<?> behaviorClass = Class.forName(BEHAVIORS_PACKAGE_PATH + behaviorName);
            Constructor<?> behaviorClassDeclaredConstructor = behaviorClass.getDeclaredConstructor();
            behavior = (Behavior) behaviorClassDeclaredConstructor.newInstance();

            Class<?> attackClass = Class.forName(ATTACKS_PACKAGE_PATH + attackName);
            Constructor<?> attackClassDeclaredConstructor = attackClass.getDeclaredConstructor();
            attack = (Attack) attackClassDeclaredConstructor.newInstance();
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }

        this.repository.addBlob(name, new Blob(name, health, damage, behavior, attack, this.subject, this.mutateBoolean.getFlag()));
    }
}
