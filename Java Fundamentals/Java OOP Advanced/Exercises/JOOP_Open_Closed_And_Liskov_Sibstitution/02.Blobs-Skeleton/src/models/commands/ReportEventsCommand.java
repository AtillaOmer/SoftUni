package models.commands;

import core.annotations.Inject;
import interfaces.Executable;
import utils.MutateBoolean;

public class ReportEventsCommand implements Executable {

    @Inject
    private MutateBoolean mutateBoolean;

    @Override
    public void execute() {
        this.mutateBoolean.setFlag(true);
    }
}
