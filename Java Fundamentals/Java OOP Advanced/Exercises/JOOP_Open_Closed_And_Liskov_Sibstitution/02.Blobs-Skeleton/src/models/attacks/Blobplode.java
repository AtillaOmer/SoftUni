package models.attacks;

import models.Blob;

public class Blobplode extends AbstractAttack {

    @Override
    public void execute(Blob attacker, Blob target) {
        
    public void execute(Blob source, Blob target) {
        int sourceHealthAfterAttack = source.getHealth() - (source.getHealth() / 2);
        if (sourceHealthAfterAttack >= 1) {
            source.setHealth(sourceHealthAfterAttack);
        }
        target.setHealth(target.getHealth() - (source.getDamage() * 2));
    }
}
