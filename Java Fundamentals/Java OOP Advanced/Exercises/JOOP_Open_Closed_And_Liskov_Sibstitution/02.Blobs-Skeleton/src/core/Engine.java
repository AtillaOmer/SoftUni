package core;

import core.annotations.Inject;
import interfaces.*;
import interfaces.Runnable;
import models.Blob;
import observers.Subject;
import utils.MutateBoolean;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class Engine implements Runnable {
    public static final String TERMINATE_COMMAND = "drop";
    private static final String COMMAND_SUFFIX = "Command";
    private static final String COMMAND_PATH = "models.commands.";

    private Reader reader;
    private Writer writer;
    private Repository<Blob> repository;
    private BlobFactory blobFactory;
    private Subject subject;
    private MutateBoolean mutateBoolean;


    public Engine(Reader reader, Writer writer, Repository repository, Subject subject, MutateBoolean mutateBoolean) {
        this.reader = reader;
        this.writer = writer;
        this.repository = repository;
        this.subject = subject;
        this.mutateBoolean = mutateBoolean;
    }

    @Override
    public void run() {
        while (true) {
            try {
                String[] data = reader.readLine().split("\\s+");
                String commandName = data[0];

                if (TERMINATE_COMMAND.equals(commandName)) {
                    break;
                }
                interpredCommand(data, commandName);

                this.subject.notifyAllObservers();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void interpredCommand(String[] data, String commandName) {
        String commandClassName = Character.toUpperCase(commandName.charAt(0)) + commandName.substring(1) + COMMAND_SUFFIX;
        try {
            Class<?> commandClass = Class.forName(COMMAND_PATH + commandClassName);
            Constructor<?> declaredConstructor = commandClass.getConstructor(String[].class);
            Executable command = (Executable) declaredConstructor.newInstance((Object) data);

            injectDependencies(command);
            command.execute();

        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.getStackTrace();
        }
    }

    private <T> void injectDependencies(T source) throws IllegalAccessException {
        Field[] commandFields = source.getClass().getDeclaredFields();
        Field[] engineFields = Engine.class.getDeclaredFields();

        for (Field commandField : commandFields) {
            commandField.setAccessible(true);
            if (commandField.isAnnotationPresent(Inject.class)) {
                for (Field engineField : engineFields) {
                    engineField.setAccessible(true);
                    if (commandField.getType().equals(engineField.getType()) && commandField.getName().equals(engineField.getName())) {
                        commandField.set(source, engineField.get(this));
                    }
                }
            }
        }
    }
}
