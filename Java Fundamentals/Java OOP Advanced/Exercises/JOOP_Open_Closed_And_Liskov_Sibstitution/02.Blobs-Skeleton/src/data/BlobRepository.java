package data;

import interfaces.Repository;
import models.Blob;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class BlobRepository implements Repository<Blob> {
    private Map<String, Blob> repository;

    public BlobRepository() {
        this.repository = new LinkedHashMap<>();
    }

    @Override
    public void addBlob(String name, Blob blob) {

        repository.put(name, blob);
    }

    @Override
    public String getStatus() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Map.Entry<String, Blob> blob : repository.entrySet()) {
            stringBuilder.append(blob.getValue()).append(System.lineSeparator());
        }

        return stringBuilder.toString();
    }

    @Override
    public Blob findByName(String name) {
        return this.repository.get(name);
    }

    @Override
    public List<Blob> findAll() {
        List<Blob> blobs = new ArrayList<>();
        for (Map.Entry<String, Blob> stringBlobEntry : repository.entrySet()) {
            blobs.add(stringBlobEntry.getValue());
        }
        return blobs;
    }


    @Override
    public void removeBlob(String name) {

    }
}
