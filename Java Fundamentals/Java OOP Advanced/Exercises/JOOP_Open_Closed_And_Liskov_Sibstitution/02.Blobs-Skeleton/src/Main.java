import core.Engine;
import data.BlobRepository;
import interfaces.Reader;
import interfaces.Repository;
import interfaces.Runnable;
import interfaces.Writer;
import io.ConsoleReader;
import io.ConsoleWriter;
import models.Blob;
import observers.Subject;
import utils.MutateBoolean;

public class Main {
    public static void main(String[] args) {
        Reader reader = new ConsoleReader();
        Writer writer = new ConsoleWriter();
        Repository<Blob> repository = new BlobRepository();
        Subject subject = new Subject();
        MutateBoolean mutateBoolean = new MutateBoolean(false);

        Runnable engine = new Engine(reader, writer, repository, subject, mutateBoolean);

        engine.run();
    }
}
