package impl.loggers;

import enums.ReportLevel;
import interfaces.Appender;
import interfaces.Logger;

import java.io.IOException;
import java.util.List;

public class MessageLogger implements Logger {

    private List<Appender> appenders;

    public MessageLogger(List<Appender> appenders) {
        this.setAppenders(appenders);
    }

    private void setAppenders(List<Appender> appenders) {
        if (appenders == null) {
            throw new IllegalStateException("Appenders cannot be null.");
        }
        this.appenders = appenders;
    }

    public void logInfo(String time, String message) throws IOException {
        for (Appender appender : appenders) {
            appender.append(ReportLevel.INFO, time, message);
        }
    }

    public void logWarning(String time, String message) throws IOException {
        for (Appender appender : appenders) {
            appender.append(ReportLevel.WARNING, time, message);
        }
    }

    public void logError(String time, String message) throws IOException {
        for (Appender appender : appenders) {
            appender.append(ReportLevel.ERROR, time, message);
        }
    }

    public void logCritical(String time, String message) throws IOException {
        for (Appender appender : appenders) {
            appender.append(ReportLevel.CRITICAL, time, message);
        }
    }

    public void logFatal(String time, String message) throws IOException {
        for (Appender appender : appenders) {
            appender.append(ReportLevel.FATAL, time, message);
        }
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Logger info").append(System.lineSeparator());
        for (Appender appender : appenders) {
            stringBuilder.append(appender.toString());
        }
        return stringBuilder.toString();
    }
}
