import core.Engine;
import interfaces.InputReader;
import interfaces.OutputWriter;
import interfaces.Runnable;
import io.ConsoleReader;
import io.ConsoleWriter;

public class Main {
    public static void main(String[] args) {
        InputReader reader = new ConsoleReader();
        OutputWriter writer = new ConsoleWriter();

        Runnable engine = new Engine(reader, writer);

        engine.run();
    }
}
