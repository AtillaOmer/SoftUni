package impl.appenders;

import enums.ReportLevel;
import interfaces.Appender;
import interfaces.Layout;
import interfaces.OutputWriter;
import io.ConsoleWriter;

public class ConsoleAppender implements Appender {
    private Layout layout;
    private OutputWriter writer;
    private ReportLevel reportLevel;
    private int messageNumber;

    public ConsoleAppender(Layout layout) {
        this.layout = layout;
        this.reportLevel = ReportLevel.INFO;
        this.messageNumber = 0;
        this.writer = new ConsoleWriter();
    }

    @Override
    public Layout getLayout() {
        return layout;
    }

    @Override
    public ReportLevel getReportLevel() {
        return reportLevel;
    }

    @Override
    public void setReportLevel(ReportLevel reportLevel) {
        this.reportLevel = reportLevel;
    }

    @Override
    public int getMessageNumber() {
        return messageNumber;
    }

    @Override
    public void append(ReportLevel reportLevel, String time, String message) {
        if (reportLevel.ordinal() >= this.reportLevel.ordinal()) {
            writer.writeLine(layout.format(reportLevel, time, message));
            this.messageNumber++;
        }
    }

    @Override
    public String toString() {
        return String.format("Appender type: %s, Layout type: %s, Report level: %s, Messages appended: %d%n",
                this.getClass().getSimpleName(),
                this.getLayout().getClass().getSimpleName(),
                this.getReportLevel(),
                this.getMessageNumber()
        );
    }
}
