package factories;

import enums.ReportLevel;
import impl.appenders.ConsoleAppender;
import impl.appenders.FileAppender;
import interfaces.Appender;

public class AppenderFactory {

    public AppenderFactory() {
    }

    public static Appender createAppender(String... args) {

        String appenderType = args[0];
        String layoutType = args[1];

        Appender appender = null;
        switch (appenderType) {
            case "ConsoleAppender":
                appender = new ConsoleAppender(LayoutFactory.createLayout(layoutType));
                if (args.length == 3) {
                    ReportLevel reportLevel = Enum.valueOf(ReportLevel.class, args[2].toUpperCase());
                    appender.setReportLevel(reportLevel);
                }
                break;
            case "FileAppender":
                appender = new FileAppender(LayoutFactory.createLayout(layoutType));
                if (args.length == 3) {
                    ReportLevel reportLevel = Enum.valueOf(ReportLevel.class, args[2].toUpperCase());
                    appender.setReportLevel(reportLevel);
                }
            default:
                break;
        }

        return appender;
    }
}
