package impl.layouts;

import enums.ReportLevel;
import interfaces.Layout;

public class SimpleLayout implements Layout {

    public SimpleLayout() {
    }

    public String format(ReportLevel level, String date, String message) {
        return String.format("%s - %s - %s",
                date, level.name(), message);
    }
}
