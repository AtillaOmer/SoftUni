package impl.appenders;

import entitities.LogFile;
import enums.ReportLevel;
import interfaces.Appender;
import interfaces.File;
import interfaces.Layout;

public class FileAppender implements Appender {
    private File file;
    private Layout layout;
    private ReportLevel reportLevel;
    private int messageNumber;

    public FileAppender(Layout layout) {
        this.file = new LogFile();
        this.layout = layout;
        this.reportLevel = ReportLevel.INFO;
        this.messageNumber = 0;
    }

    private File getFile() {
        return file;
    }

    public Layout getLayout() {
        return layout;
    }

    public ReportLevel getReportLevel() {
        return reportLevel;
    }

    public void setReportLevel(ReportLevel reportLevel) {
        this.reportLevel = reportLevel;
    }

    @Override
    public int getMessageNumber() {
        return messageNumber;
    }

    @Override
    public void append(ReportLevel reportLevel, String time, String message) {
        if (reportLevel.ordinal() >= this.reportLevel.ordinal()) {

            file.write(this.layout.format(reportLevel, time, message));
            this.messageNumber++;

        }
    }

    @Override
    public String toString() {
        return String.format("Appender type: %s, Layout type: %s, Report level: %s, Messages appended: %d, File size: %d%n",
                this.getClass().getSimpleName(),
                this.getLayout().getClass().getSimpleName(),
                this.getReportLevel(),
                this.getMessageNumber(),
                this.getFile().getSize()
        );
    }
}
