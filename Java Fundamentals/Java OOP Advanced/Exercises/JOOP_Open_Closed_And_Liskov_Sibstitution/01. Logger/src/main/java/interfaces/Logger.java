package interfaces;

import java.io.IOException;

public interface Logger {
    void logInfo(String time, String message) throws IOException;

    void logWarning(String time, String message) throws IOException;

    void logError(String time, String message) throws IOException;

    void logCritical(String time, String message) throws IOException;

    void logFatal(String time, String message) throws IOException;
}
