package entitities;

import interfaces.File;

public class LogFile implements File {
    private StringBuilder stringBuilder;

    public LogFile() {
        this.stringBuilder = new StringBuilder();
    }

    public void write(String text) {
        stringBuilder.append(text).append(System.lineSeparator());

    }

    public int getSize() {
        int size = 0;
        for (char ch : this.stringBuilder.toString().toCharArray()) {
            if (String.valueOf(ch).matches("[A-Za-z]")) {
                size += ch;
            }
        }
        return size;
    }
}
