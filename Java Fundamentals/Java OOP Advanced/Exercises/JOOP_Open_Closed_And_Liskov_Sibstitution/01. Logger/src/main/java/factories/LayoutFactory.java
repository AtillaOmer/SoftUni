package factories;

import impl.layouts.SimpleLayout;
import impl.layouts.XmlLayout;
import interfaces.Layout;

public class LayoutFactory {
    private static final String SIMPLE_LAYOUT = "SimpleLayout";
    private static final String XML_LAYOUT = "XmlLayout";

    public LayoutFactory() {
    }

    public static Layout createLayout(String layoutType) {
        Layout layout = null;

        switch (layoutType) {
            case SIMPLE_LAYOUT:
                layout = new SimpleLayout();
                break;
            case XML_LAYOUT:
                layout = new XmlLayout();
                break;
            default:
                break;
        }

        return layout;
    }
}
