package io;

import interfaces.OutputWriter;

public class ConsoleWriter implements OutputWriter {
    public ConsoleWriter() {
    }

    @Override
    public void writeLine(String line) {
        System.out.println(line);
    }
}
