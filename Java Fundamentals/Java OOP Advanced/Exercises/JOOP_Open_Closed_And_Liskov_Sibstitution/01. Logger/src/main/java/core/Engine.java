package core;

import enums.ReportLevel;
import factories.AppenderFactory;
import impl.loggers.MessageLogger;
import interfaces.*;
import interfaces.Runnable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Engine implements Runnable {
    private final String TERMINATE_PROGRAM = "END";

    private InputReader reader;
    private OutputWriter writer;
    private Logger logger;
    private boolean isRun;

    public Engine(InputReader reader, OutputWriter writer) {
        this.reader = reader;
        this.writer = writer;
        this.isRun = true;
    }

    @Override
    public void run() {
        while (isRun) {
            try {
                int n = Integer.parseInt(this.reader.readLine());

                List<Appender> appenders = new ArrayList<>();
                while (n-- > 0) {
                    String[] args = reader.readLine().split(" ");
                    appenders.add(AppenderFactory.createAppender(args));
                }

                logger = new MessageLogger(appenders);
                String input;
                while (true) {
                    if (TERMINATE_PROGRAM.equals(input = reader.readLine())) {
                        isRun = false;
                        break;
                    }

                    String[] args = input.split("\\|");
                    ReportLevel reportLevel = Enum.valueOf(ReportLevel.class, args[0].toUpperCase());
                    String time = args[1];
                    String message = args[2];


                    switch (reportLevel) {
                        case INFO:
                            logger.logInfo(time, message);
                            break;
                        case WARNING:
                            logger.logWarning(time, message);
                            break;
                        case ERROR:
                            logger.logError(time, message);
                            break;
                        case CRITICAL:
                            logger.logCritical(time, message);
                            break;
                        case FATAL:
                            logger.logFatal(time, message);
                            break;
                        default:
                            break;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.print(logger);
        }

    }
}
