package interfaces;

public interface File {
    void write(String time);

    int getSize();
}
