package interfaces;

import enums.ReportLevel;

import java.io.IOException;

public interface Appender {
    Layout getLayout();

    ReportLevel getReportLevel();

    int getMessageNumber();

    void setReportLevel(ReportLevel reportLevel);

    void append(ReportLevel info, String time, String message) throws IOException;
}
