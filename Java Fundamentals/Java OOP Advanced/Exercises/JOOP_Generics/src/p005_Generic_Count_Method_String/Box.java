package p005_Generic_Count_Method_String;

import java.util.List;

public class Box<T> {

    private T value;

    public Box() {
    }

    public Box(T value) {
        this.setValue(value);
    }

    public void setValue(T value) {
        this.value = value;
    }

    public static <T> void swap(List<T> list, int firsIndex, int secondIndex) {

        T firstElement = list.get(firsIndex);
        T secondElement = list.get(secondIndex);
        T temp = firstElement;
        list.set(firsIndex, secondElement);
        list.set(secondIndex, temp);
    }

    public static <T extends Comparable> int countOfElements(List<T> list, T value) {
        int counter = 0;
        for (T t : list) {
            if (value.compareTo(t) < 0)
                counter++;
        }
        return counter;
    }

    @Override
    public String toString() {
        return String.format("%s: %s", this.value.getClass().getName(), this.value);
    }
}
