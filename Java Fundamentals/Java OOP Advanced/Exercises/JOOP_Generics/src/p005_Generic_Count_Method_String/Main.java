package p005_Generic_Count_Method_String;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());

        List<String> boxes = new ArrayList<>();

        while (n-- > 0) {
            boxes.add(reader.readLine());
        }

        String controlValue = reader.readLine();

        System.out.println(Box.countOfElements(boxes, controlValue));
    }
}