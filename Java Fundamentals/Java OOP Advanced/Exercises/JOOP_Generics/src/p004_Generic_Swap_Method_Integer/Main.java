package p004_Generic_Swap_Method_Integer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());

        List<Box<Integer>> boxes = new ArrayList<>();

        while (n-- > 0) {
            boxes.add(new Box<Integer>(Integer.parseInt(reader.readLine())));
        }

        int[] indices = Arrays.stream(reader.readLine().split("\\s+")).mapToInt(Integer::parseInt).toArray();
        Box.swap(boxes, indices[0], indices[1]);
        for (Box<Integer> box : boxes) {
            System.out.println(box);
        }
    }
}
