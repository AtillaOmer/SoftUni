package p006_Generic_Count_Method_Double;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(reader.readLine());

        List<Double> boxes = new ArrayList<>();

        while (n-- > 0) {
            boxes.add(Double.parseDouble(reader.readLine()));
        }

        Double controlValue = Double.parseDouble(reader.readLine());

        System.out.println(Box.countOfElements(boxes, controlValue));
    }
}