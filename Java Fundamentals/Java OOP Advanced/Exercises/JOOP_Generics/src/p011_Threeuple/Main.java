package p011_Threeuple;

import p010_Tuple.Tuple;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Threeuple<String, String, String> nameAddress = new Threeuple<>() ;
        Tuple<String, Integer> nameBeer = new Tuple<>();
        Tuple<Integer, Double> integerDouble = new Tuple<>();

        String[] personAndAddress = reader.readLine().split("\\s+");
        String personName = new StringBuilder().append(personAndAddress[0]).append(" ").append(personAndAddress[1]).toString();
        String address = personAndAddress[2];
        nameAddress.put(personName, address);

        String[] personBeer = reader.readLine().split("\\s+");
        String person = personBeer[0];
        int beers = Integer.parseInt(personBeer[1]);
        nameBeer.put(person, beers);

        String[] nums = reader.readLine().split("\\s+");
        int integerNum = Integer.parseInt(nums[0]);
        double doubleNum = Double.parseDouble(nums[1]);
        integerDouble.put(integerNum, doubleNum);

        System.out.println(nameAddress);
        System.out.println(nameBeer);
        System.out.println(integerDouble);
    }
}
