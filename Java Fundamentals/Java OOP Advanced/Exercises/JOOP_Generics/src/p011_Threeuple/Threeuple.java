package p011_Threeuple;

public class Threeuple<K, V, E> {
    private K item1;
    private V item2;
    private E item3;

    public Threeuple() {
    }

    public void put(K key, V value, E element) {
        this.setItem1(key);
        this.setItem2(value);
        this.setItem3(element);
    }

    private K getItem1() {
        return item1;
    }

    private void setItem1(K item1) {
        this.item1 = item1;
    }

    private V getItem2() {
        return item2;
    }

    private void setItem2(V item2) {
        this.item2 = item2;
    }

    public E getItem3() {
        return item3;
    }

    private void setItem3(E item3) {
        this.item3 = item3;
    }

    @Override
    public String toString() {
        return getItem1() + " -> " + getItem2() + " -> " + getItem3();
    }
}
