package p008_Custom_List_Sorter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@SuppressWarnings("unchecked")
public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ListUtils<String> list = new MyList<>();

        while (true) {
            String[] input = reader.readLine().split("\\s+");

            if ("END".equals(input[0]))
                break;

            switch (input[0]) {
                case "Add":
                    list.add(input[1]);
                    break;
                case "Remove":
                    list.remove(Integer.parseInt(input[1]));
                    break;
                case "Contains":
                    System.out.println(list.contains(input[1]));
                    break;
                case "Swap":
                    list.swap(Integer.parseInt(input[1]), Integer.parseInt(input[2]));
                    break;
                case "Greater":
                    System.out.println(list.countGreaterThan(input[1]));
                    break;
                case "Max":
                    System.out.println(list.getMax());
                    break;
                case "Min":
                    System.out.println(list.getMin());
                    break;
                case "Print":
                    for (String s : list.getElements()) {
                        System.out.println(s);
                    }
                    break;
                case "Sort":
                    Sort.sort(list);
                    break;
            }
        }
    }
}
