package p008_Custom_List_Sorter;

public interface ListUtils<T extends Comparable<T>>{
    void sort();

    void add(T element);

    T remove(int index);

    boolean contains(T element);

    void swap(int firstIndex, int secondIndex);

    int countGreaterThan(T element);

    T getMax();

    T getMin();

    Iterable<T> getElements();
}
