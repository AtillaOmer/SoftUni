package p010_Tuple;

import java.util.LinkedHashMap;
import java.util.Map;

public class Tuple<K, V> {
    private K item1;
    private V item2;

    public Tuple() {
    }

    public void put(K key, V value) {
        this.setItem1(key);
        this.setItem2(value);
    }

    public K getItem1() {
        return item1;
    }

    private void setItem1(K item1) {
        this.item1 = item1;
    }

    public V getItem2() {
        return item2;
    }

    private void setItem2(V item2) {
        this.item2 = item2;
    }

    @Override
    public String toString() {
        return getItem1() + " -> " + getItem2();
    }
}
