package p003_Generic_Swap_Method_Strings;

import java.util.List;

public class Box<T> {

    private T value;

    public Box() {
    }

    public Box(T value) {
        this.setValue(value);
    }

    public void setValue(T value) {
        this.value = value;
    }

    public static <T> void swap(List<T> list, int firsIndex, int secondIndex) {

        T firstElement = list.get(firsIndex);
        T secondElement = list.get(secondIndex);
        T temp = firstElement;
        list.set(firsIndex, secondElement);
        list.set(secondIndex, temp);
    }

    @Override
    public String toString() {
        return String.format("%s: %s", this.value.getClass().getName(), this.value);
    }
}
