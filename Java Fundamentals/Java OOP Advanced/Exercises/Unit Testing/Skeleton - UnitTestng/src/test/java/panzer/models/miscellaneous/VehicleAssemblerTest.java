package panzer.models.miscellaneous;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import panzer.contracts.Assembler;
import panzer.contracts.AttackModifyingPart;
import panzer.contracts.DefenseModifyingPart;
import panzer.contracts.HitPointsModifyingPart;

import java.math.BigDecimal;

public class VehicleAssemblerTest {
    private static final double ATTACK_MODIFYING_PART = 10.0;
    private static final BigDecimal ATTACK_MODIFYING_PRICE = new BigDecimal(10.0);
    private static final int ATTACK_MODIFIER = 25;
    private static final double DEFENSE_MODIFYING_PART = 20.0;
    private static final BigDecimal DEFENSE_MODIFYING_PRICE = new BigDecimal(20.0);
    private static final int DEFENSE_MODIFIER = 40;
    private static final double HIT_POINT_MODIFYING_PART = 30.0;
    private static final BigDecimal HIT_POINT_MODIFYING_PRICE = new BigDecimal(30.0);
    private static final int HIT_POINT_MODIFIER = 46;
    private static final double EXPECTED_TOTAL_WEIGHT = 60.0;
    private static final int EXPECTED_ATTACK_MODIFIER = 25;
    private static final int EXPECTED_DEFENCE_MODIFIER = 40;
    private static final int EXPECTED_HIT_POINTS_MODIFIER = 46;
    private static final BigDecimal EXPECTED_TOTAL_PRICE = BigDecimal.ZERO.add(ATTACK_MODIFYING_PRICE).add(DEFENSE_MODIFYING_PRICE).add(HIT_POINT_MODIFYING_PRICE);
    private static final int EXPECTED_PARTS_COUNT = 1;

    private Assembler vehicleAssembler;
    private AttackModifyingPart attackModifyingPart;
    private DefenseModifyingPart defenseModifyingPart;
    private HitPointsModifyingPart hitPointsModifyingPart;

    @Before
    public void setUp() throws Exception {
        this.vehicleAssembler = new VehicleAssembler();
        this.attackModifyingPart = Mockito.mock(AttackModifyingPart.class);
        this.defenseModifyingPart = Mockito.mock(DefenseModifyingPart.class);
        this.hitPointsModifyingPart = Mockito.mock(HitPointsModifyingPart.class);
        this.vehicleAssembler.addArsenalPart(this.attackModifyingPart);
        this.vehicleAssembler.addShellPart(this.defenseModifyingPart);
        this.vehicleAssembler.addEndurancePart(this.hitPointsModifyingPart);
    }

    @Test
    public void getTotalWeight() {
        //Arrange
        Mockito.when(this.attackModifyingPart.getWeight()).thenReturn(ATTACK_MODIFYING_PART);
        Mockito.when(this.defenseModifyingPart.getWeight()).thenReturn(DEFENSE_MODIFYING_PART);
        Mockito.when(this.hitPointsModifyingPart.getWeight()).thenReturn(HIT_POINT_MODIFYING_PART);

        //Act
        double totalWeight = this.vehicleAssembler.getTotalWeight();

        // Assert
        Assert.assertEquals("Wrong total price", EXPECTED_TOTAL_WEIGHT, this.vehicleAssembler.getTotalWeight(), 0.1);
    }

    @Test
    public void getTotalPrice() {
        //Arrange
        Mockito.when(this.attackModifyingPart.getPrice()).thenReturn(ATTACK_MODIFYING_PRICE);
        Mockito.when(this.defenseModifyingPart.getPrice()).thenReturn(DEFENSE_MODIFYING_PRICE);
        Mockito.when(this.hitPointsModifyingPart.getPrice()).thenReturn(HIT_POINT_MODIFYING_PRICE);

        //Act
        BigDecimal totalPrice = this.vehicleAssembler.getTotalPrice();

        //Assert
        Assert.assertEquals("Wrong total price!", EXPECTED_TOTAL_PRICE, totalPrice);
    }

    @Test
    public void getTotalAttackModification() {
        //Arrange
        Mockito.when(this.attackModifyingPart.getAttackModifier()).thenReturn(ATTACK_MODIFIER);

        //Act
        long attackModifier = this.vehicleAssembler.getTotalAttackModification();

        //Assert
        Assert.assertEquals("Wrong attack modifier", EXPECTED_ATTACK_MODIFIER, attackModifier);
    }

    @Test
    public void getTotalDefenseModification() {
        //Arrange
        Mockito.when(this.defenseModifyingPart.getDefenseModifier()).thenReturn(DEFENSE_MODIFIER);

        //Act
        long totalDefence = this.vehicleAssembler.getTotalDefenseModification();

        //Assert
        Assert.assertEquals("Wrong total defence modifier", EXPECTED_DEFENCE_MODIFIER, totalDefence);
    }

    @Test
    public void getTotalHitPointModification() {
        //Arrange
        Mockito.when(this.hitPointsModifyingPart.getHitPointsModifier()).thenReturn(HIT_POINT_MODIFIER);

        //Act
        long hitPoints = this.vehicleAssembler.getTotalHitPointModification();

        //Assert
        Assert.assertEquals("Wrong hit points modifier", EXPECTED_HIT_POINTS_MODIFIER, hitPoints);
    }

    @Test
    public void addArsenalPart() {
        // Arrange

        Assembler assembler = new VehicleAssembler();

        AttackModifyingPart part = Mockito.mock(AttackModifyingPart.class);

        AttackModifyingPart part1 = Mockito.mock(AttackModifyingPart.class);

        Mockito.when(part.getAttackModifier()).thenReturn(Integer.MAX_VALUE);

        Mockito.when(part1.getAttackModifier()).thenReturn(Integer.MAX_VALUE);



        // Act

        assembler.addArsenalPart(part);

        assembler.addArsenalPart(part1);



        // Act

        long actualTotalAttackModification = assembler.getTotalAttackModification();

        long expectedTotalAttackModification = (long) Integer.MAX_VALUE + Integer.MAX_VALUE;



        //  Assert

        Assert.assertEquals(expectedTotalAttackModification, actualTotalAttackModification);
    }

    @Test
    public void addShellPart() {
        // Arrange

        Assembler assembler = new VehicleAssembler();

        DefenseModifyingPart part = Mockito.mock(DefenseModifyingPart.class);

        DefenseModifyingPart part1 = Mockito.mock(DefenseModifyingPart.class);

        Mockito.when(part.getDefenseModifier()).thenReturn(Integer.MAX_VALUE);

        Mockito.when(part1.getDefenseModifier()).thenReturn(Integer.MAX_VALUE);



        // Act

        assembler.addShellPart(part);

        assembler.addShellPart(part1);



        // Act

        long totalDefenseModification = assembler.getTotalDefenseModification();

        long expectedTotalAttackModification = (long) Integer.MAX_VALUE + Integer.MAX_VALUE;



        //  Assert

        Assert.assertEquals(expectedTotalAttackModification, totalDefenseModification);
    }

    @Test
    public void addEndurancePart() {
        // Arrange

        Assembler assembler = new VehicleAssembler();

        HitPointsModifyingPart part = Mockito.mock(HitPointsModifyingPart.class);

        HitPointsModifyingPart part1 = Mockito.mock(HitPointsModifyingPart.class);

        Mockito.when(part.getHitPointsModifier()).thenReturn(Integer.MAX_VALUE);

        Mockito.when(part1.getHitPointsModifier()).thenReturn(Integer.MAX_VALUE);



        // Act

        assembler.addEndurancePart(part);

        assembler.addEndurancePart(part1);



        // Act

        long totalHitPointModification = assembler.getTotalHitPointModification();

        long expectedTotalAttackModification = (long) Integer.MAX_VALUE + Integer.MAX_VALUE;



        //  Assert

        Assert.assertEquals(expectedTotalAttackModification, totalHitPointModification);
    }
}