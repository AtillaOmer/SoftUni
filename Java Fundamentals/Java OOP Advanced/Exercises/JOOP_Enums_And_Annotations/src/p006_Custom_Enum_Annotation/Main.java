package p006_Custom_Enum_Annotation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
	private static final String OUTPUT = "Type = %s, Description = %s";
	private static final String RANK = "Rank";
	private static final String SUIT = "Suit";

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		String input = reader.readLine();

		if (RANK.equals(input)) {
			Class<CardRank> rank = CardRank.class;
			if (rank.isAnnotationPresent(CustomAnnotation.class)) {
				CustomAnnotation annotation = rank.getAnnotation(CustomAnnotation.class);
				System.out.println(String.format(OUTPUT, annotation.type(), annotation.description()));
			}
		} else if (SUIT.equals(input)) {
			Class<CardSuit> suit = CardSuit.class;
			if (suit.isAnnotationPresent(CustomAnnotation.class)) {
				CustomAnnotation annotation = suit.getAnnotation(CustomAnnotation.class);
				System.out.println(String.format(OUTPUT, annotation.type(), annotation.description()));
			}
		}
	}
}
