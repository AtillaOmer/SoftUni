package p006_Custom_Enum_Annotation;

@CustomAnnotation(category = "Suit", description = "Provides suit constants for a Card class.")
public enum CardSuit {
	CLUBS(0), DIAMONDS(13), HEARTS(26), SPADES(39);

	private int suit;

	private CardSuit(int suit) {
		this.setSuit(suit);
	}

	public int getSuit() {
		return suit;
	}

	public void setSuit(int suit) {
		this.suit = suit;
	}

	@Override
	public String toString() {
		return super.name();
	}
}
