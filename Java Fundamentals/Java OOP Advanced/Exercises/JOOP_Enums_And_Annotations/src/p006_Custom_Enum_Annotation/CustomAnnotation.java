package p006_Custom_Enum_Annotation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target(TYPE)
public @interface CustomAnnotation {
	String type() default "Enumeration";

	String category();

	String description();
}
