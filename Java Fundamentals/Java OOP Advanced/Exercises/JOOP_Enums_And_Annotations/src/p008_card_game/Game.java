package p008_card_game;

public class Game {
	private Player firstPlayer;
	private Player secondPlayer;

	public Game(Player firstPlayer, Player secondPlayer) {
		super();
		this.firstPlayer = firstPlayer;
		this.secondPlayer = secondPlayer;
	}

	public Player getWinner() {
		return this.firstPlayer.highestCard().compareTo(this.secondPlayer.highestCard()) > 0 ? this.firstPlayer
				: this.secondPlayer;
	}

}
