package p008_card_game;

import java.util.ArrayList;
import java.util.List;

public class Deck {
	private List<Card> cards;

	public Deck() {
		this.cards = new ArrayList<>();
		this.fillDeck();
	}

	private void fillDeck() {
		for (Suit suit : Suit.values()) {
			for (Rank rank : Rank.values()) {
				Card card = new Card(rank, suit);
				this.cards.add(card);
			}
		}
	}

	public boolean contains(Card card) {
		return this.cards.stream().anyMatch(innerCard -> innerCard.getRank().name().equals(card.getRank().name())
				&& innerCard.getSuit().name().equals(card.getSuit().name()));
	}

	public void remove(Card card) {
		this.cards.removeIf(
				innerCard -> innerCard.getRank().equals(card.getRank()) && innerCard.getSuit().equals(card.getSuit()));
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();

		for (Card card : cards) {
			stringBuilder.append(card).append(System.lineSeparator());
		}

		return stringBuilder.toString();
	}

}
