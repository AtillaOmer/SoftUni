package p008_card_game;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		Player firstPlayer = new Player(reader.readLine());
		Player secondPlayer = new Player(reader.readLine());
		Deck deck = new Deck();

		while (true) {

			if (firstPlayer.getCards().size() == 5 && secondPlayer.getCards().size() == 5) {
				break;
			}

			String[] cardArgs = reader.readLine().split("\\s+");
			try {
				Rank rank = Enum.valueOf(Rank.class, cardArgs[0].toUpperCase());
				Suit suit = Enum.valueOf(Suit.class, cardArgs[2].toUpperCase());
				Card card = new Card(rank, suit);

				if (deck.contains(card)) {
					if (firstPlayer.getCards().size() < 5) {
						firstPlayer.addCard(card);
					} else if (secondPlayer.getCards().size() < 5) {
						secondPlayer.addCard(card);
					}
					deck.remove(card);
				} else {
					System.out.println("Card is not in the deck.");
				}

			} catch (IllegalArgumentException iae) {
				System.out.println("No such card exists.");
			}
		}

		Game game = new Game(firstPlayer, secondPlayer);
		System.out.println(game.getWinner());
	}
}
