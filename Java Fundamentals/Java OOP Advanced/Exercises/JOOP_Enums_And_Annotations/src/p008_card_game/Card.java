package p008_card_game;

public class Card implements Comparable<Card> {
	private Rank rank;
	private Suit suit;

	public Card(Rank rank, Suit suit) {
		this.rank = rank;
		this.suit = suit;
	}

	private int getPower() {
		return this.rank.getRank() + this.suit.getSuit();
	}

	public Rank getRank() {
		return rank;
	}

	public Suit getSuit() {
		return suit;
	}

	@Override
	public String toString() {
		return String.format("%s of %s", this.getRank(), this.getSuit());
	}

	@Override
	public int compareTo(Card card) {
		return Integer.compare(this.getPower(), card.getPower());
	}
}
