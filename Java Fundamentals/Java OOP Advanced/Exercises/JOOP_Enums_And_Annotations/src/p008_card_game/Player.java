package p008_card_game;

import java.util.ArrayList;
import java.util.List;

public class Player {
	private String name;
	private List<Card> cards;

	public Player(String name) {
		this.name = name;
		this.cards = new ArrayList<>();
	}

	private String getName() {
		return this.name;
	}

	public List<Card> getCards() {
		return cards;
	}

	public void addCard(Card card) {
		this.cards.add(card);
	}

	public Card highestCard() {
		return this.cards.stream().max(Card::compareTo).get();
	}

	@Override
	public String toString() {
		return String.format("%s wins with %s.", this.getName(), this.highestCard());
	}
}
