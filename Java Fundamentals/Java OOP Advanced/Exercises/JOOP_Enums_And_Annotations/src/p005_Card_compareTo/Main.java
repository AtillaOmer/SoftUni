package p005_Card_compareTo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		String rank = reader.readLine();
		String suit = reader.readLine();

		Card firstCard = new Card(rank, suit);

		rank = reader.readLine();
		suit = reader.readLine();

		Card secondCard = new Card(rank, suit);

		if (firstCard.compareTo(secondCard) >= 0) {
			System.out.println(firstCard);
		} else {
			System.out.println(secondCard);
		}
	}
}
