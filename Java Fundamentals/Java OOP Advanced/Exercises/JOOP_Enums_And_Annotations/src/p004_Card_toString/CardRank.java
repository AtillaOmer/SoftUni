package p004_Card_toString;

public enum CardRank {
	ACE(14), TWO(2), THREE(3), FOUR(4), FIVE(5), SIX(6), SEVEN(7), EIGHT(8), NINE(9), TEN(10), JACK(11), QUEEN(
			12), KING(13);

	private int rank;

	private CardRank(int rank) {
		this.setRank(rank);
	}

	public int getRank() {
		return rank;
	}

	private void setRank(int rank) {
		this.rank = rank;
	}

	@Override
	public String toString() {
		return super.name();
	}
}
