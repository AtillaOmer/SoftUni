package p010_Inferno_Infinity;

import java.util.ArrayList;
import java.util.List;

public class Weapon {
	private WeaponType type;
	private String name;
	private List<Ruby> sockets;

	public Weapon(WeaponType type, String name) {
		this.type = type;
		this.name = name;
		this.sockets = new ArrayList<>();
	}

	public WeaponType getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public List<Ruby> getSockets() {
		return sockets;
	}

	private int socketCount() {
		return this.type.getSockets();
	}

	public void addRuby(int index, String rubyName) {
		if (index < this.socketCount() && index >= 0) {
			Ruby ruby = Enum.valueOf(Ruby.class, rubyName);
			if (index >= this.getSockets().size()) {
				this.sockets.add(ruby);
			} else {
				this.sockets.set(index, ruby);
			}
		}
	}

	public void removeRuby(int index) {
		if (index < this.socketCount() && index >= 0) {
			if (!this.getSockets().isEmpty()) {
				this.getSockets().remove(index);
			}
		}
	}

	private int getMinDamage() {
		return this.getType().getMinDamage() + this.getStrength() * 2 + this.getAgility();
	}

	private int getMaxDamage() {
		return this.getType().getMaxDamage() + this.getStrength() * 3 + this.getAgility() * 4;
	}

	private int getStrength() {
		return this.sockets.stream().mapToInt(Ruby::getStrength).sum();
	}

	private int getAgility() {
		return this.sockets.stream().mapToInt(Ruby::getAgility).sum();
	}

	private int getVitality() {
		return this.sockets.stream().mapToInt(Ruby::getVitality).sum();
	}

	@Override
	public String toString() {
		return String.format("%s: %d-%d Damage, +%d Strength, +%d Agility, +%d Vitality", this.getName(),
				this.getMinDamage(), this.getMaxDamage(), this.getStrength(), this.getAgility(), this.getVitality());
	}
}
