package p014_refactoring.io;

import p014_refactoring.interfaces.OutputWriter;

public class ConsoleWriter implements OutputWriter {

	@Override
	public void writeLine(String line) {
		System.out.println(line);
	}

}
