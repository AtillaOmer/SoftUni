package p014_refactoring.repository;

import java.util.LinkedHashMap;
import java.util.Map;

import p014_refactoring.entities.Weapon;
import p014_refactoring.interfaces.Repository;

public class WeaponRepository implements Repository<Weapon> {

	private Map<String, Weapon> weapons;

	public WeaponRepository() {
		this.weapons = new LinkedHashMap<>();
	}

	@Override
	public void addWeapon(String name, Weapon weapon) {
		this.weapons.putIfAbsent(name, weapon);
	}

	@Override
	public void removeWeapon(String name) {
		this.removeWeapon(name);
	}

	@Override
	public Weapon findWeaponByName(String name) {
		return this.weapons.get(name);
	}

}
