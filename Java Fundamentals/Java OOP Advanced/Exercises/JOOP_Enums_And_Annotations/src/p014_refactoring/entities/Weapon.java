package p014_refactoring.entities;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import p014_refactoring.annotations.CustomAnnotation;
import p014_refactoring.enums.Ruby;
import p014_refactoring.enums.WeaponType;

@CustomAnnotation
public class Weapon implements Comparable<Weapon> {
	private WeaponType type;
	private String name;
	@SuppressWarnings("unused")
	private double level;
	private List<Ruby> sockets;

	public Weapon(WeaponType type, String name) {
		this.type = type;
		this.name = name;
		this.level = 0d;
		this.sockets = new ArrayList<>();
	}

	public WeaponType getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public double getLevel() {
		return ((this.getMinDamage() + this.getMaxDamage()) / 2d) + this.getStrength() + this.getAgility()
				+ this.getVitality();
	}

	public List<Ruby> getSockets() {
		return sockets;
	}

	private int socketCount() {
		return this.type.getSockets();
	}

	public void addRuby(int index, String rubyName) {
		if (index < this.socketCount() && index >= 0) {
			Ruby ruby = Enum.valueOf(Ruby.class, rubyName);
			if (index >= this.getSockets().size()) {
				this.sockets.add(ruby);
			} else {
				this.sockets.set(index, ruby);
			}
		}
	}

	public void removeRuby(int index) {
		if (index < this.socketCount() && index >= 0) {
			if (!this.getSockets().isEmpty()) {
				this.getSockets().remove(index);
			}
		}
	}

	private int getMinDamage() {
		return this.getType().getMinDamage() + this.getStrength() * 2 + this.getAgility();
	}

	private int getMaxDamage() {
		return this.getType().getMaxDamage() + this.getStrength() * 3 + this.getAgility() * 4;
	}

	private int getStrength() {
		return this.sockets.stream().mapToInt(Ruby::getStrength).sum();
	}

	private int getAgility() {
		return this.sockets.stream().mapToInt(Ruby::getAgility).sum();
	}

	private int getVitality() {
		return this.sockets.stream().mapToInt(Ruby::getVitality).sum();
	}

	public String print() {
		DecimalFormat df = new DecimalFormat(".0");
		df.setRoundingMode(RoundingMode.DOWN); // Note this extra step
		return String.format("%s: %d-%d Damage, +%d Strength, +%d Agility, +%d Vitality (Item Level: %s)",
				this.getName(), this.getMinDamage(), this.getMaxDamage(), this.getStrength(), this.getAgility(),
				this.getVitality(), df.format(this.getLevel()));
	}

	@Override
	public int compareTo(Weapon weapon) {
		return Double.compare(this.getLevel(), weapon.getLevel());
	}

	@Override
	public String toString() {
		return String.format("%s: %d-%d Damage, +%d Strength, +%d Agility, +%d Vitality", this.getName(),
				this.getMinDamage(), this.getMaxDamage(), this.getStrength(), this.getAgility(), this.getVitality());
	}
}
