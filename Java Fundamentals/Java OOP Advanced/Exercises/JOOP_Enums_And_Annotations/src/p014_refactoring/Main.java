package p014_refactoring;

import java.io.IOException;

import p014_refactoring.core.Engine;
import p014_refactoring.entities.Weapon;
import p014_refactoring.interfaces.InputReader;
import p014_refactoring.interfaces.OutputWriter;
import p014_refactoring.interfaces.Repository;
import p014_refactoring.io.ConsoleReader;
import p014_refactoring.io.ConsoleWriter;
import p014_refactoring.repository.WeaponRepository;

public class Main {

	public static void main(String[] args) throws IOException {
		InputReader reader = new ConsoleReader();
		OutputWriter writer = new ConsoleWriter();
		Repository<Weapon> repository = new WeaponRepository();

		Engine engine = new Engine(reader, writer, repository);
		engine.run();
	}
}
