package p014_refactoring.core;

import java.io.IOException;

import p014_refactoring.annotations.CustomAnnotation;
import p014_refactoring.constants.AnnotationOutputFormats;
import p014_refactoring.entities.Weapon;
import p014_refactoring.factories.WeaponFactory;
import p014_refactoring.interfaces.InputReader;
import p014_refactoring.interfaces.OutputWriter;
import p014_refactoring.interfaces.Repository;

public class Engine {
	private InputReader reader;
	private OutputWriter writer;
	private Repository<Weapon> repository;

	public Engine(InputReader reader, OutputWriter writer, Repository<Weapon> repository) {
		this.reader = reader;
		this.writer = writer;
		this.repository = repository;
	}

	public void run() throws IOException {

		while (true) {
			String input = this.reader.readLine();
			if (input.equals("END")) {
				break;
			}

			String[] tokens = input.split(";");
			String command = tokens[0];

			switch (command) {
			case "Create":
				String name = tokens[2];
				String type = tokens[1];

				this.repository.addWeapon(name, WeaponFactory.createWeapon(tokens));
				break;
			case "Add":
				name = tokens[1];
				int index = Integer.parseInt(tokens[2]);
				String rubyName = tokens[3];

				Weapon weapon = repository.findWeaponByName(name);
				if (weapon != null) {
					weapon.addRuby(index, rubyName);
				}
				break;
			case "Remove":
				name = tokens[1];
				index = Integer.parseInt(tokens[2]);
				weapon = repository.findWeaponByName(name);
				if (weapon != null) {
					weapon.removeRuby(index);
				}
				break;
			case "Compare":
				Weapon firstWeapon = repository.findWeaponByName(tokens[1]);
				Weapon secondWeapon = repository.findWeaponByName(tokens[2]);

				if (firstWeapon != null && secondWeapon != null) {
					if (firstWeapon.compareTo(secondWeapon) >= 0) {
						this.writer.writeLine(firstWeapon.print());
					} else {
						this.writer.writeLine(secondWeapon.print());
					}
				}
				break;
			case "Print":
				name = tokens[1];
				weapon = repository.findWeaponByName(name);

				if (weapon != null) {
					System.out.println(weapon);
				}
				break;
			case "Author":
				Class<Weapon> weaponClass = Weapon.class;
				if (weaponClass.isAnnotationPresent(CustomAnnotation.class)) {
					CustomAnnotation annotation = weaponClass.getAnnotation(CustomAnnotation.class);
					this.writer.writeLine(String.format(AnnotationOutputFormats.AUTHOR_OUTPUT, annotation.author()));
				}
				break;
			case "Revision":
				weaponClass = Weapon.class;
				if (weaponClass.isAnnotationPresent(CustomAnnotation.class)) {
					CustomAnnotation annotation = weaponClass.getAnnotation(CustomAnnotation.class);
					this.writer
							.writeLine(String.format(AnnotationOutputFormats.REVISION_OUTPUT, annotation.revision()));
				}
				break;
			case "Description":
				weaponClass = Weapon.class;
				if (weaponClass.isAnnotationPresent(CustomAnnotation.class)) {
					CustomAnnotation annotation = weaponClass.getAnnotation(CustomAnnotation.class);
					this.writer.writeLine(
							String.format(AnnotationOutputFormats.DESCRIPTION_OUTPUT, annotation.description()));
				}
				break;
			case "Reviewers":
				weaponClass = Weapon.class;
				if (weaponClass.isAnnotationPresent(CustomAnnotation.class)) {
					CustomAnnotation annotation = weaponClass.getAnnotation(CustomAnnotation.class);
					this.writer.writeLine(String.format(AnnotationOutputFormats.REVIWERS_OUTPUT,
							annotation.reviewers()[0], annotation.reviewers()[1]));
				}
				break;
			}
		}
	}
}