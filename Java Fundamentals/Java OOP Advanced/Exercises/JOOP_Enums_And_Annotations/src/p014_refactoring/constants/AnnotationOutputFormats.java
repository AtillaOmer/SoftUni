package p014_refactoring.constants;

public class AnnotationOutputFormats {
	public static final String AUTHOR_OUTPUT = "Author: %s";
	public static final String REVISION_OUTPUT = "Revision: %s";
	public static final String DESCRIPTION_OUTPUT = "Class description: %s";
	public static final String REVIWERS_OUTPUT = "Reviewers: %s, %s";
}
