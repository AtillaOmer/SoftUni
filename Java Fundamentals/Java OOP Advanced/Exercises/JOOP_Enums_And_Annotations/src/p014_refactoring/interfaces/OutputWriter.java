package p014_refactoring.interfaces;

public interface OutputWriter {
	void writeLine(String line);
}
