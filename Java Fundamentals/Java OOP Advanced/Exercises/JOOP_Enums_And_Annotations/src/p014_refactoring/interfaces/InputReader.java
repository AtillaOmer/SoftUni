package p014_refactoring.interfaces;

import java.io.IOException;

public interface InputReader {
	String readLine() throws IOException;
}
