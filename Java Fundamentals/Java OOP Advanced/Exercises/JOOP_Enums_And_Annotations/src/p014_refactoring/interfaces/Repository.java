package p014_refactoring.interfaces;

public interface Repository<T> {
	void addWeapon(String name, T weapon);

	void removeWeapon(String name);

	T findWeaponByName(String name);
}
