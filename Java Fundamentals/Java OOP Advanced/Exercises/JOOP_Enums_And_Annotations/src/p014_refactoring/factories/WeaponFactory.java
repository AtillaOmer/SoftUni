package p014_refactoring.factories;

import p014_refactoring.entities.Weapon;
import p014_refactoring.enums.WeaponType;

public class WeaponFactory {
	public WeaponFactory() {
	}

	public static Weapon createWeapon(String... args) {
		WeaponType type = Enum.valueOf(WeaponType.class, args[1]);
		String name = args[2];

		return new Weapon(type, name);
	}
}
