package p014_refactoring.annotations;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Retention(RUNTIME)
@Target(TYPE)
public @interface CustomAnnotation {
	String author() default "Pesho";

	int revision() default 3;

	String description() default "Used for Java OOP Advanced course - Enumerations and Annotations.";

	String[] reviewers() default { "Pesho", "Svetlio" };

}
