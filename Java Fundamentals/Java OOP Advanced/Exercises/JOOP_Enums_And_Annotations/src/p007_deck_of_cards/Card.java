package p007_deck_of_cards;

public class Card implements Comparable<Card> {
	private Rank rank;
	private Suit suit;

	public Card(Rank rank, Suit suit) {
		this.rank = rank;
		this.suit = suit;
	}

	private int getPower() {
		return this.rank.getRank() + this.suit.getSuit();
	}

	private Rank getRank() {
		return rank;
	}

	private Suit getSuit() {
		return suit;
	}

	@Override
	public String toString() {
		return String.format("%s of %s", this.getRank(), this.getSuit());
	}

	@Override
	public int compareTo(Card card) {
		return Integer.compare(this.getPower(), card.getPower());
	}
}
