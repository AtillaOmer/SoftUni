package p007_deck_of_cards;

import java.util.ArrayList;
import java.util.List;

public class Deck {
	private List<Card> cards;

	public Deck() {
		this.cards = new ArrayList<>();
		this.fillDeck();
	}

	private void fillDeck() {
		for (Suit suit : Suit.values()) {
			for (Rank rank : Rank.values()) {
				Card card = new Card(rank, suit);
				this.cards.add(card);
			}
		}
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();

		for (Card card : cards) {
			stringBuilder.append(card).append(System.lineSeparator());
		}

		return stringBuilder.toString();
	}

}
