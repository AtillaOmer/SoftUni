package p007_deck_of_cards;

@CustomAnnotation(category = "Suit", description = "Provides suit constants for a Card class.")
public enum Suit {
	CLUBS(0), DIAMONDS(13), HEARTS(26), SPADES(39);

	private int suit;

	private Suit(int suit) {
		this.setSuit(suit);
	}

	public int getSuit() {
		return suit;
	}

	public void setSuit(int suit) {
		this.suit = suit;
	}

	@Override
	public String toString() {
		return super.name();
	}
}
