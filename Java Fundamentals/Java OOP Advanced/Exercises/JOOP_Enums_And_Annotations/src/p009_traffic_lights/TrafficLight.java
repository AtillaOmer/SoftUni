package p009_traffic_lights;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TrafficLight {
	private List<Status> status;

	public TrafficLight(String... statuses) {
		this.setStatus(statuses);
	}

	public List<Status> getStatus() {
		return status;
	}

	private void setStatus(String... status) {
		if (status.length == 0) {
			this.status = new ArrayList<>();
		} else {
			this.status = Arrays.asList(status).stream().map(x -> Enum.valueOf(Status.class, x.toUpperCase()))
					.collect(Collectors.toList());
		}
	}

	public void update() {
		for (int counter = 0; counter < this.status.size(); counter++) {
			if (this.status.get(counter).getStatusCode() == 1) {
				this.status.set(counter, Status.YELLOW);
			} else if (this.status.get(counter).getStatusCode() == 2) {
				this.status.set(counter, Status.RED);
			} else if (this.status.get(counter).getStatusCode() == 3) {
				this.status.set(counter, Status.GREEN);
			}
		}
	}

	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();

		this.status.stream().forEach(s -> stringBuilder.append(s).append(" "));

		return stringBuilder.toString();
	}

}
