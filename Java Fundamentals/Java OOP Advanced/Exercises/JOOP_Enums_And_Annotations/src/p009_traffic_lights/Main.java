package p009_traffic_lights;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		String[] input = reader.readLine().split("\\s+");
		int n = Integer.parseInt(reader.readLine());

		TrafficLight trafficLight = new TrafficLight(input);

		while (n-- > 0) {
			trafficLight.update();
			System.out.println(trafficLight);
		}
	}
}
