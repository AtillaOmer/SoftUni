package p009_traffic_lights;

public enum Status {
	GREEN(1), YELLOW(2), RED(3);

	private int statusCode;

	private Status(int statusCode) {
		this.statusCode = statusCode;
	}

	public int getStatusCode() {
		return statusCode;
	}

	@Override
	public String toString() {
		return super.name();
	}

}
