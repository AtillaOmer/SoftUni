package p001_Card_Suit;

public class Main {

	public static void main(String[] args) {

		System.out.println("Card Suits:");
		for (CardSuit cardSuit : CardSuit.values()) {
			System.out.println(cardSuit);
		}
	}

}
