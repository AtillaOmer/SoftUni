package p012_inferno_infinity_compareTo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class Main {
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		Map<String, Weapon> weapons = new LinkedHashMap<>();

		while (true) {
			String input = reader.readLine();
			if (input.equals("END")) {
				break;
			}

			String[] tokens = input.split(";");
			String command = tokens[0];

			switch (command) {
			case "Create":
				String name = tokens[2];
				String type = tokens[1];

				weapons.putIfAbsent(name, new Weapon(Enum.valueOf(WeaponType.class, type), name));
				break;
			case "Add":
				name = tokens[1];
				int index = Integer.parseInt(tokens[2]);
				String rubyName = tokens[3];

				if (weapons.containsKey(name)) {
					Weapon weapon = weapons.get(name);
					weapon.addRuby(index, rubyName);
				}
				break;
			case "Remove":
				name = tokens[1];
				index = Integer.parseInt(tokens[2]);
				if (weapons.containsKey(name)) {
					Weapon weapon = weapons.get(name);
					weapon.removeRuby(index);
				}
				break;
			case "Compare":
				String firstWeaponName = tokens[1];
				String secondWeaponName = tokens[2];

				if (weapons.containsKey(firstWeaponName) && weapons.containsKey(secondWeaponName)) {
					Weapon firstWeapon = weapons.get(firstWeaponName);
					Weapon secondWeapon = weapons.get(secondWeaponName);
					if (firstWeapon.compareTo(secondWeapon) >= 0) {
						System.out.println(firstWeapon.print());
					} else {
						System.out.println(secondWeapon.print());
					}
				}
				break;
			case "Print":
				name = tokens[1];
				if (weapons.containsKey(name)) {
					Weapon weapon = weapons.get(name);
					System.out.println(weapon);
				}
				break;
			}
		}
	}
}
