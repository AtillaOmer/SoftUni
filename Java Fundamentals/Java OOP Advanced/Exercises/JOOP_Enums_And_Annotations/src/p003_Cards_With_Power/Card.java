package p003_Cards_With_Power;

public class Card {
	private CardRank rank;
	private CardSuit suit;

	public Card(String rank, String suit) {
		this.rank = Enum.valueOf(CardRank.class, rank.toUpperCase());
		this.suit = Enum.valueOf(CardSuit.class, suit.toUpperCase());
	}

	private int getPower() {
		return this.rank.getRank() + this.suit.getSuit();
	}

	private CardRank getRank() {
		return rank;
	}

	private CardSuit getSuit() {
		return suit;
	}

	@Override
	public String toString() {
		return String.format("Card name: %s of %s; Card power: %d", this.getRank(), this.getSuit(), this.getPower());
	}
}
