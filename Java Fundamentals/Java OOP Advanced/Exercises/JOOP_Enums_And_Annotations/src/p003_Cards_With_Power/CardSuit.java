package p003_Cards_With_Power;

public enum CardSuit {
	CLUBS(0), DIAMONDS(13), HEARTS(26), SPADES(39);

	private int suit;

	private CardSuit(int suit) {
		this.setSuit(suit);
	}

	public int getSuit() {
		return suit;
	}

	public void setSuit(int suit) {
		this.suit = suit;
	}

	@Override
	public String toString() {
		return super.name();
	}
}
