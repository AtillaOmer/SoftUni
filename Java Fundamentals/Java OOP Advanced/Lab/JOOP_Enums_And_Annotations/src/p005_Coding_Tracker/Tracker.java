package p005_Coding_Tracker;

public class Tracker {
	@Author(name = "Pesho")
	public static void printMethodsByAuthor(Class<?> cl) {
		System.out.println(cl);
	}

	@Author(name = "Pesho")
	public static void main(String[] args) {
		Tracker.printMethodsByAuthor(Tracker.class);
	}
}
