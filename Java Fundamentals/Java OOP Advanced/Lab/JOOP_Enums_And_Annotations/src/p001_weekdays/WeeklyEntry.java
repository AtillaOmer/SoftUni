package p001_weekdays;

import java.util.Comparator;

public class WeeklyEntry {
	public final static Comparator<WeeklyEntry> BY_WEEKDAY = getCompByDay();
	private Weekday weekday;
	private String notes;

	public WeeklyEntry(String weekday, String notes) {
		this.weekday = Enum.valueOf(Weekday.class, weekday.toUpperCase());
		this.notes = notes;
	}

	private Weekday getWeekday() {
		return weekday;
	}

	private String getNotes() {
		return notes;
	}

	private static Comparator<WeeklyEntry> getCompByDay() {
		return (e1, e2) -> e1.weekday.compareTo(e2.weekday);
	}

	@Override
	public String toString() {
		return String.format("%s - %s", this.getWeekday(), this.getNotes());
	}
}
