package p003_Coffee_Machine;

import p003_Coffee_Machine.enums.CoffeeSize;
import p003_Coffee_Machine.enums.CoffeeType;

public class Coffee {
	private CoffeeSize size;
	private CoffeeType type;

	public Coffee(CoffeeSize size, CoffeeType type) {
		this.size = size;
		this.type = type;
	}

	public int getPrice() {
		return this.size.getPrice();
	}

	@Override
	public String toString() {
		return String.format("%s %s", this.size, this.type);
	}
}
