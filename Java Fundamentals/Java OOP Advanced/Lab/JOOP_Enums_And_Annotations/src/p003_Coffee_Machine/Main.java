package p003_Coffee_Machine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import p003_Coffee_Machine.enums.Coin;

public class Main {

	@SuppressWarnings("unlikely-arg-type")
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		String input = reader.readLine();
		CoffeeMachine coffeeMachine = new CoffeeMachine();

		while (true) {
			if (input.equals("END")) {
				break;
			}

			String[] tokens = input.split("\\s+");
			List<Coin> coinValues = new ArrayList<>();
			coinValues.addAll(Arrays.asList(Coin.values()));
			try {
				if (coinValues.contains(Enum.valueOf(Coin.class, tokens[0].toUpperCase()))) {
					String coin = tokens[0];
					coffeeMachine.insertCoin(coin);
				}
			} catch (Exception ex) {
				String size = tokens[0];
				String type = tokens[1];

				coffeeMachine.buyCoffee(size, type);
			}
			input = reader.readLine();
		}

		for (Coffee coffee : coffeeMachine.coffeesSold()) {
			System.out.println(coffee);
		}
	}

}
