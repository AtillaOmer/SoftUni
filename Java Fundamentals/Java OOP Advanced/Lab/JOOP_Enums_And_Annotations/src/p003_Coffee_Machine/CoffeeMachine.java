package p003_Coffee_Machine;

import java.util.ArrayList;
import java.util.List;

import p003_Coffee_Machine.enums.CoffeeSize;
import p003_Coffee_Machine.enums.CoffeeType;
import p003_Coffee_Machine.enums.Coin;

public class CoffeeMachine {
	private List<Coin> coins;
	private List<Coffee> coffeeList;

	public CoffeeMachine() {
		this.coins = new ArrayList<>();
		this.coffeeList = new ArrayList<>();
	}

	public void buyCoffee(String size, String type) {
		CoffeeSize coffeeSize = Enum.valueOf(CoffeeSize.class, size.toUpperCase());
		CoffeeType coffeeType = Enum.valueOf(CoffeeType.class, type.toUpperCase());

		Coffee coffee = new Coffee(coffeeSize, coffeeType);

		int price = coffee.getPrice();
		int currentSum = this.coins.stream().mapToInt(Coin::getValue).sum();
		if (currentSum > price) {
			this.coffeeList.add(coffee);
			this.coins.clear();
		}
	}

	public void insertCoin(String coin) {
		this.coins.add(Enum.valueOf(Coin.class, coin.toUpperCase()));
	}

	public Iterable<Coffee> coffeesSold() {
		return this.coffeeList;
	}
}
