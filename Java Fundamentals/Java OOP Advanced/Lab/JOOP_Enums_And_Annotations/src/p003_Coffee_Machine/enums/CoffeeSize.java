package p003_Coffee_Machine.enums;

public enum CoffeeSize {
	SMALL(50, 50), NORMAL(100, 75), DOUBLE(200, 100);

	private int size;
	private int price;

	private CoffeeSize(int size, int price) {
		this.size = size;
		this.price = price;
	}

	public int getSize() {
		return size;
	}

	public int getPrice() {
		return price;
	}

	@Override
	public String toString() {
		return String.format("%d - %d", this.size, this.price);
	}
}
