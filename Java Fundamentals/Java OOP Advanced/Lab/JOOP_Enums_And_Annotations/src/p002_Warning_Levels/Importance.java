package p002_Warning_Levels;

public enum Importance {
	LOW, NORMAL, MEDIUM, HIGH;
}
