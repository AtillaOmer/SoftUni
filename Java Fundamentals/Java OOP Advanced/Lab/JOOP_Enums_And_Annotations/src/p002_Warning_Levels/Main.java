package p002_Warning_Levels;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

		String level = reader.readLine();
		Logger logger = new Logger(Enum.valueOf(Importance.class, level.toUpperCase()));

		String message = reader.readLine();
		while (!"END".equals(message)) {
			String[] messageArgs = message.split(":\\s+");
			String messageLevel = messageArgs[0];
			String messageContent = messageArgs[1];

			Message currMessage = new Message(Enum.valueOf(Importance.class, messageLevel.toUpperCase()),
					messageContent);

			logger.log(currMessage);

			message = reader.readLine();
		}

		for (Message m : logger.getMessages()) {
			System.out.println(m);
		}

	}

}
