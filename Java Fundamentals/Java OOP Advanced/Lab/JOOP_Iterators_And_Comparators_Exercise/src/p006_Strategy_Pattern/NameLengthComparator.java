package p006_Strategy_Pattern;

import java.util.Comparator;

public class NameLengthComparator implements Comparator<Person> {
    @Override
    public int compare(Person firstPerson, Person secondPerson) {
        if (Integer.compare(firstPerson.getName().length(), secondPerson.getName().length()) == 0) {
            return String.valueOf(firstPerson.getName().charAt(0)).compareToIgnoreCase(String.valueOf(secondPerson.getName().charAt(0)));
        } else {
            return Integer.compare(firstPerson.getName().length(), secondPerson.getName().length());
        }
    }
}
