package p006_Strategy_Pattern;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Set<Person> personSetOne = new TreeSet<>(new NameLengthComparator());
        Set<Person> personSetTwo = new TreeSet<>(new AgeComparator());

        int n = Integer.parseInt(reader.readLine());

        while (n-- > 0) {
            String[] input = reader.readLine().split(" ");
            String name = input[0];
            int age = Integer.parseInt(input[1]);
            Person person = new Person(name, age);

            personSetOne.add(person);
            personSetTwo.add(person);
        }

        personSetOne.stream().forEach(System.out::println);
        personSetTwo.stream().forEach(System.out::println);
    }
}
