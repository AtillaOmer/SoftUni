package p003_Stack_Iterator;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Stack<T> implements Iterable<Integer> {
    List<Integer> elements;

    public Stack() {
        this.elements = new ArrayList<>();
    }

    @Override
    public Iterator<Integer> iterator() {
        return new StackIterator();
    }

    public Integer pop() {
        return this.elements.remove(elements.size() - 1);
    }

    public void push(Integer item) {
        this.elements.add(item);
    }


    private final class StackIterator implements Iterator<Integer> {
        int counter = elements.size();

        @Override
        public boolean hasNext() {
            return counter > 0;
        }

        @Override
        public Integer next() {
            counter--;
            return elements.get(counter);
        }
    }
}
