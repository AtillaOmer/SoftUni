package p003_Stack_Iterator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Stack<Integer> stack = new Stack<>();
        while (true){
            try {
                String[] input = reader.readLine().split(" ", 2);
                String command = input[0];

                if ("END".equals(command)) {
                    break;
                }

                switch (command) {
                    case "Push":
                        for (String s : input[1].split(", ")) {
                            stack.push(Integer.parseInt(s.trim()));
                        }
                        break;
                    case "Pop":
                        stack.pop();
                        break;
                }
            }catch (Exception ex){
                System.out.println("No elements");
                return;
            }
        }

        for (Integer integer : stack) {
            System.out.println(integer);
        }

        for (Integer integer : stack) {
            System.out.println(integer);
        }
    }
}
