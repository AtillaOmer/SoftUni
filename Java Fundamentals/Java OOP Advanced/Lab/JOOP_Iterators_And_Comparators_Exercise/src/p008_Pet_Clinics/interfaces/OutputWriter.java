package p008_Pet_Clinics.interfaces;

public interface OutputWriter {
    void writeLine(String line);
}
