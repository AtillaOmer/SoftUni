package p008_Pet_Clinics.interfaces;

import java.io.IOException;

public interface InputReader {
    String readLine() throws IOException;
}
