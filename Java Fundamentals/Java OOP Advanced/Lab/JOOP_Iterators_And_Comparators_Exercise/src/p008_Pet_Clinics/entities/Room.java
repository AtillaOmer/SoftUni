package p008_Pet_Clinics.entities;

import java.util.ArrayList;
import java.util.List;

public class Room {
    private int number;
    private List<Pet> pets;

    public Room(int number) {
        this.setNumber(number);
        this.pets = new ArrayList<>();
    }

    private int getNumber() {
        return number;
    }

    private void setNumber(int number) {
        this.number = number;
    }

    private List<Pet> getPets() {
        return pets;
    }

    private void setPets(List<Pet> pets) {
        this.pets = pets;
    }
}
