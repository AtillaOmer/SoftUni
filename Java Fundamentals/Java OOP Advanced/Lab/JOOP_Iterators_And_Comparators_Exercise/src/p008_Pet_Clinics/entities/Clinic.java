package p008_Pet_Clinics.entities;

import java.util.NoSuchElementException;

public class Clinic {

    private String name;
    private Pet[] rooms;
    private int addStartingNumber;
    private int removeStartingNumber;

    public Clinic(String name, int roomsCount) {
        this.name = name;
        this.setRooms(roomsCount);
        this.addStartingNumber = roomsCount / 2;
        this.removeStartingNumber = roomsCount / 2;
    }

    public Pet[] getRooms() {
        return this.rooms;
    }

    public boolean addPet(Pet pet) {

        boolean isPetAdded = false;

        CustomIterator<Pet> addIterator = new AddIterator();
        while (addIterator.hasNext()) {
            if (addIterator.next() == null) {
                this.rooms[addIterator.getIndex()] = pet;
                isPetAdded = true;
                break;
            }
        }

        return isPetAdded;

    }

    public boolean removePet() {

        boolean isPetRemoved = false;

        CustomIterator<Pet> removeIterator = new RemoveIterator();
        while (removeIterator.hasNext()) {
            if (removeIterator.next() != (null)) {
                this.rooms[removeIterator.getIndex()] = null;
                isPetRemoved = true;
                break;
            }
        }

        return isPetRemoved;

    }

    public boolean hasEmptyRooms() {
        for (Pet pet : getRooms()) {
            if (pet == null) {
                return true;
            }
        }

        return false;
    }

    private void setRooms(int roomsCount) {
        if (roomsCount % 2 == 1) {
            this.rooms = new Pet[roomsCount];
        } else {
            throw new IllegalArgumentException("Invalid Operation!");
        }
    }

    private class AddIterator implements CustomIterator<Pet> {

        private int temp;
        private int step;
        private int currentIndex;

        private AddIterator() {
            this.temp = 0;
            this.step = 0;
            this.currentIndex = addStartingNumber ;
        }

        @Override
        public boolean hasNext() {
            return this.currentIndex >= 0 && this.currentIndex < getRooms().length;
        }

        @Override
        public Pet next() {
            if (hasNext()) {
                temp = this.currentIndex;
                if (step % 2 == 0) {
                    this.currentIndex -= (++step);
                } else {
                    this.currentIndex += (++step);
                }
                return rooms[temp];
            }

            throw new NoSuchElementException("Invalid Operation!");
        }

        public int getIndex() {
            return temp;
        }
    }

    private class RemoveIterator implements CustomIterator<Pet> {
        private int temp;
        private int step;
        private int currentIndex;
        private boolean movingLeft;

        private RemoveIterator() {
            this.temp = 0;
            this.step = 0;
            this.currentIndex = removeStartingNumber;
            this.movingLeft = false;
        }

        @Override
        public boolean hasNext() {

            if (this.currentIndex >= getRooms().length) {
                this.currentIndex = removeStartingNumber - 1;
                this.step = 1;
                this.movingLeft = true;
                return true;
            } else return currentIndex > 0;
        }

        @Override
        public Pet next() {
            if (hasNext()) {
                temp = this.currentIndex;
                if (this.movingLeft) {
                    this.currentIndex -= (++step);
                } else {
                    this.currentIndex += (++step);
                }
                return rooms[temp];
            }

            throw new NoSuchElementException("Invalid Operation!");
        }

        @Override
        public int getIndex() {
            return temp;
        }
    }
}