package p008_Pet_Clinics.entities;


import java.util.Iterator;

public interface CustomIterator<Pet> extends Iterator<Pet> {
    int getIndex();
}
