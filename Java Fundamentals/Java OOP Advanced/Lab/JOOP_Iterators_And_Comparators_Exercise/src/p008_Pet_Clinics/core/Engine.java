package p008_Pet_Clinics.core;

import p008_Pet_Clinics.controller.CommandManager;
import p008_Pet_Clinics.interfaces.InputReader;
import p008_Pet_Clinics.interfaces.OutputWriter;
import p008_Pet_Clinics.io.ConsoleReader;
import p008_Pet_Clinics.io.ConsoleWriter;

import java.io.IOException;

public class Engine {
    InputReader reader;
    OutputWriter writer;
    CommandManager manager;

    public Engine() {
        this.reader = new ConsoleReader();
        this.writer = new ConsoleWriter();
        this.manager = new CommandManager();
    }

    public void run() throws IOException {
        int n = Integer.parseInt(reader.readLine());

        while (n-- > 0) {
            String[] args = reader.readLine().split("\\s+");
            String command = args[0];

            switch (command) {
                case "Add":
                    System.out.println(this.manager.addPet(args));
                    break;
                case "Create":
                    try {
                        if (args[1].equals("Pet")) {
                            this.manager.createPet(args);
                        } else if (args[1].equals("Clinic")) {
                            this.manager.createClinic(args);
                        }
                    } catch (IllegalArgumentException iae){
                        System.out.println(iae.getMessage());
                    }
                    break;
                case "Release":
                    System.out.println(this.manager.releaseAnimal(args[1]));
                    break;
                case "HasEmptyRooms":
                    System.out.println(this.manager.hasEmptyRooms(args[1]));
                    break;
                case "Print":
                    this.manager.print(args);
                    break;
            }
        }
    }
}
