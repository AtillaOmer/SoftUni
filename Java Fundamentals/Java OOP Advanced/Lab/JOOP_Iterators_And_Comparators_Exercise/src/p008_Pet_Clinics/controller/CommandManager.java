package p008_Pet_Clinics.controller;

import p008_Pet_Clinics.entities.Clinic;
import p008_Pet_Clinics.entities.Pet;
import p008_Pet_Clinics.factories.ClinicFactory;
import p008_Pet_Clinics.factories.PetFactory;

import java.util.LinkedHashMap;
import java.util.Map;

public class CommandManager {
    private Map<String, Pet> pets;
    private Map<String, Clinic> clinics;

    public CommandManager() {
        this.pets = new LinkedHashMap<>();
        this.clinics = new LinkedHashMap<>();
    }

    public void createPet(String... args) {
        this.pets.put(args[2], PetFactory.createPet(args));
    }

    public void createClinic(String... args) {
        this.clinics.put(args[2], ClinicFactory.createClinic(args));
    }

    public boolean addPet(String... args) {
        String petName = args[1];
        String clinicName = args[2];
        if (pets.containsKey(petName)) {
            if (clinics.containsKey(clinicName)) {
                Pet pet = pets.get(petName);
                Clinic clinic = clinics.get(clinicName);

                return clinic.addPet(pet);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public boolean releaseAnimal(String clinicName) {
        if (clinics.containsKey(clinicName)) {
            Clinic clinic = clinics.get(clinicName);

            return clinic.removePet();
        } else {
            return false;
        }
    }

    public boolean hasEmptyRooms(String clinicName) {
        if (clinics.containsKey(clinicName)) {
            Clinic clinic = clinics.get(clinicName);

            return clinic.hasEmptyRooms();
        }
        return false;
    }

    public void print(String... args) {
        String clinicName = args[1];
        Clinic clinic = clinics.get(clinicName);
        if (args.length == 2) {
            for (Pet pet : clinic.getRooms()) {
                if (pet == null) {
                    System.out.println("Room empty");
                } else {
                    System.out.println(pet.toString());
                }
            }
        } else if (args.length == 3) {
            int roomNumber = Integer.valueOf(args[2]) - 1;
            System.out.println(clinic.getRooms()[roomNumber]);
        }
    }
}
