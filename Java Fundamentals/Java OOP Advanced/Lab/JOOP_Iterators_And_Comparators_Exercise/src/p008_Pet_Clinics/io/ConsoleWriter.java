package p008_Pet_Clinics.io;

import p008_Pet_Clinics.interfaces.OutputWriter;

public class ConsoleWriter implements OutputWriter {
    @Override
    public void writeLine(String line) {
        System.out.println(line);
    }
}
