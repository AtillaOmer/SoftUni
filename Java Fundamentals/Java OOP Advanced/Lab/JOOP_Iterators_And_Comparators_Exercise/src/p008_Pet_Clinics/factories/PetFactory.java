package p008_Pet_Clinics.factories;

import p008_Pet_Clinics.entities.Pet;

public class PetFactory {
    public PetFactory() {
    }

    public static Pet createPet(String... args) {
        String name = args[2];
        int age = Integer.parseInt(args[3]);
        String kind = args[4];

        return new Pet(name, age, kind);
    }
}
