package p008_Pet_Clinics.factories;

import p008_Pet_Clinics.entities.Clinic;

public class ClinicFactory {
    public ClinicFactory() {
    }

    public static Clinic createClinic(String... args) {
        String name = args[2];
        int rooms = Integer.parseInt(args[3]);

        return new Clinic(name, rooms);
    }
}
