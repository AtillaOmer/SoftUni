package p002_Collection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        ListyIterator<String> listyIterator = null;
        while (true) {
            String[] input = reader.readLine().split(" ", 2);
            String command = input[0];

            if ("END".equals(command))
                break;


            try {

                switch (command) {
                    case "Create":
                        listyIterator = new ListyIterator<>(input[1].split(" "));
                        break;
                    case "Move":
                        System.out.println(listyIterator.move());
                        break;
                    case "Print":
                        listyIterator.print();
                        break;
                    case "HasNext":
                        System.out.println(listyIterator.hasNext());
                        break;
                    case "PrintAll":
                        listyIterator.printAll();
                        break;
                }
            } catch (Exception ex) {
                System.out.println("Invalid Operation!");
                return;
            }


        }
    }
}
