package p002_Collection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class ListyIterator<T> implements Iterable<T> {
    private List<String> elements;
    private int index;

    public ListyIterator(String... elements) {
        this.setElements(elements);
        this.setIndex(0);
    }

    public List<String> getElements() {
        return elements;
    }

    public void setElements(String... elements) {
        if (elements.length == 0) {
            this.elements = new ArrayList<>();
        } else {
            this.elements = new ArrayList<>(Arrays.asList(elements));
        }
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean move() {
        if (hasNext()) {
            index++;
            return true;
        } else {
            return false;
        }
    }

    public boolean hasNext() {
        return index < elements.size() - 1;
    }

    public void print() {
        System.out.println(this.getElements().get(this.getIndex()));
    }

    public void printAll() {
        for (String list : elements) {
            System.out.print(list + " ");
        }
        System.out.println();
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }
}
