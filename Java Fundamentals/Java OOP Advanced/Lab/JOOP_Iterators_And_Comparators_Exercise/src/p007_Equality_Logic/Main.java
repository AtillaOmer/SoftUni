package p007_Equality_Logic;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        Set<Person> personTreeSet = new TreeSet<>();
        Set<Person> personHashSet = new HashSet<>();

        int n = Integer.parseInt(reader.readLine());

        while (n-- > 0) {
            String[] inputArgs = reader.readLine().split("\\s+");
            String name = inputArgs[0];
            int age = Integer.parseInt(inputArgs[1]);

            Person person = new Person(name, age);

            personTreeSet.add(person);
            personHashSet.add(person);
        }

        System.out.println(personTreeSet.size());
        System.out.println(personHashSet.size());
    }
}
