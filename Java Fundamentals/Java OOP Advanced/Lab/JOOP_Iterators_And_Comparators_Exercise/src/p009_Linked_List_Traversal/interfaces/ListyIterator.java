package p009_Linked_List_Traversal.interfaces;

public interface ListyIterator<E> {

    boolean move();

    boolean listyHasNext();

    void print();

    int getSize();

    void printAll();
}
