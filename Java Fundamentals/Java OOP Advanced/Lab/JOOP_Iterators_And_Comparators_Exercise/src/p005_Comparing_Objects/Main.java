package p005_Comparing_Objects;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        List<Person> personList = new ArrayList<>();

        while (true) {
            String[] input = reader.readLine().split(" ");

            if (input[0].equals("END")) {
                break;
            }

            String name = input[0];
            int age = Integer.parseInt(input[1]);
            String town = input[2];

            Person person = new Person(name, age, town);
            personList.add(person);
        }

        int index = Integer.parseInt(reader.readLine());
        int counter = 0;

        if (index > personList.size()-1 || index < 0) {
            System.out.println("No matches");
            return;
        } else {
            for (Person person : personList) {
                if (person.compareTo(personList.get(index)) == 0){
                    counter++;
                }
            }
        }

        System.out.println(counter + " " + (personList.size() - counter) + " " + personList.size());

    }
}
