package P01_ExtendedArrayList;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        ArrayList<String> arrayList = new ExtendedArrayList<String>(){{
            add("a");
            add("b");
            add("c");
            add("d");
        }};

        System.out.println(((ExtendedArrayList<String>) arrayList).min());
        System.out.println(((ExtendedArrayList<String>) arrayList).max());
    }
}
