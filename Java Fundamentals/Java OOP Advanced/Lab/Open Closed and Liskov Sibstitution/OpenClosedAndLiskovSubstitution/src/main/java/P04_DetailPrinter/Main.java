package P04_DetailPrinter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<String> docs = new ArrayList<>() {{
            add("doc1");
            add("doc2");
            add("doc3");
        }};
        Employee manager = new Manager("Manager", docs);
        Employee employee = new Employee("Employee");

        DetailsPrinter printer = new DetailsPrinter(Arrays.asList(manager, employee));

        printer.printDetails();
    }
}
