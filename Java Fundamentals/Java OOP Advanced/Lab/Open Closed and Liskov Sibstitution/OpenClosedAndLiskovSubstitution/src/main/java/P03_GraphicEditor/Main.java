package P03_GraphicEditor;

public class Main {
    public static void main(String[] args) {
        GraphicEditor editor = new GraphicEditor();

        editor.drawShape(new Circle());
        editor.drawShape(new Rectangle());
        editor.drawShape(new Shape());
    }
}
