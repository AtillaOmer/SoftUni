package P02_FileStream;

public class Main {
    public static void main(String[] args) {
        StreamProgressInfo progressInfo = new StreamProgressInfo(new Music());

        System.out.println(progressInfo.calculateCurrentPercent());
    }
}
