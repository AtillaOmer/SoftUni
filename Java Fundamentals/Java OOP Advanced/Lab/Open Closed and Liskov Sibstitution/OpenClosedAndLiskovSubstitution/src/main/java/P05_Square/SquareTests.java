package P05_Square;

import org.junit.Assert;
import org.junit.Test;

public class SquareTests {

    @Test
    public void getSidesTest() {
        Rectangle square = new Square(10);
        Assert.assertEquals(10, square.getWidth());
        Assert.assertEquals(10, square.getHeight());
    }

    @Test
    public void getAreaTest() {
        Rectangle rect = new Square(10);
        Assert.assertEquals(100, rect.getArea());
    }
}
