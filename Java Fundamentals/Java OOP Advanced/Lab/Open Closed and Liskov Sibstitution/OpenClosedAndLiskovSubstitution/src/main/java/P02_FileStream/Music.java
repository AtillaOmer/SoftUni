package P02_FileStream;

public class Music implements Stream {
    public static final String MUSIC_ARTIST = "Artist";
    public static final String MUSIC_ALBUM = "Album";
    public static final int MUSIC_LENGTH = 60;
    public static final int MUSIC_BYTES_SENT = 60;

    private String artist;
    private String album;
    private int length;
    private int bytesSent;

    public Music() {
        this.artist = MUSIC_ARTIST;
        this.album = MUSIC_ALBUM;
        this.length = MUSIC_LENGTH;
        this.bytesSent = MUSIC_BYTES_SENT;
    }

    public int getLength() {
        return length;
    }

    public int getBytesSent() {
        return bytesSent;
    }
}
