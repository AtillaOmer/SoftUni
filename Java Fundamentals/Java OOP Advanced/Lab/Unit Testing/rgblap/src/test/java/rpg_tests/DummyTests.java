package rpg_tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import rpg_lab.Axe;
import rpg_lab.Dummy;

public class DummyTests {
    private static final int AXE_ATTACK = 10;
    private static final int AXE_DURABILITY = 10;
    private static final int DUMMY_HEALTH = 10;
    private static final int DUMMY_XP = 10;
    private static final int EXPECTED_HEALTH = DUMMY_HEALTH - AXE_ATTACK;
    public static final int EXPECTED_XP = DUMMY_XP;


    private Axe axe;
    private Dummy dummy;

    @Before
    public void initializeTestObjects() {
        this.axe = new Axe(AXE_ATTACK, AXE_DURABILITY);
        this.dummy = new Dummy(DUMMY_HEALTH, DUMMY_XP);
    }

    @Test
    public void dummyLoseHealthIfAttacked() {
        dummy.takeAttack(axe.getAttackPoints());

        Assert.assertEquals(EXPECTED_HEALTH, dummy.getHealth());

    }

    @Test(expected = IllegalStateException.class)
    public void deadDummyThrowExceptionIfAttacked() {
        this.dummy.takeAttack(this.axe.getAttackPoints());
        this.dummy.takeAttack(this.axe.getAttackPoints());
    }

    @Test
    public void deadDummyCanGiveXP() {
        this.dummy.takeAttack(this.axe.getAttackPoints());

        int xp = dummy.giveExperience();

        Assert.assertEquals(EXPECTED_XP, xp);
    }

    @Test(expected = IllegalStateException.class)
    public void aliveDummyCanNotGiveXP() {
        this.dummy.giveExperience();
    }
}
