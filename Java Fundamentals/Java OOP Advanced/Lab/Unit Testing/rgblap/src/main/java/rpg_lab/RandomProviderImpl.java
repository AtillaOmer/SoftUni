package rpg_lab;

import rpg_lab.interfaces.RandomProvider;

import java.util.Random;

public class RandomProviderImpl implements RandomProvider {
    private Random randomGenerator;

    public RandomProviderImpl() {
        this.randomGenerator = new Random();
    }


    public int GetRandomNumber(int maxRange) {
        return this.randomGenerator.nextInt(maxRange);
    }
}
