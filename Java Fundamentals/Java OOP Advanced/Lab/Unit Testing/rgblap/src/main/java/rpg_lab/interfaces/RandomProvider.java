package rpg_lab.interfaces;

public interface RandomProvider {
    int GetRandomNumber(int maxRange);
}
