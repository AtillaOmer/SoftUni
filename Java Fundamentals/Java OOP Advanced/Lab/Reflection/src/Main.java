import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.*;

public class Main {
    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class<Reflection> aClass = Reflection.class;

//        Exercise 1
//======================================================================================================================
//        System.out.println(aClass);
//        System.out.println(aClass.getSuperclass());
//        Class[] interfaces = aClass.getInterfaces();
//        for (Class anInterface : interfaces)
//            System.out.println(anInterface);
//        //Reflection ref = aClass.newInstance();//Deprecated since Java 9
//        Reflection ref = aClass.getDeclaredConstructor().newInstance();
//        System.out.println(ref);

//      Exercise 2
//======================================================================================================================
//        Method[] methods = aClass.getDeclaredMethods();
//        List<Method> getters = new ArrayList<>();
//        for (Method method : methods) {
//            if (method.getName().startsWith("get")) {
//                if (method.getParameterTypes().length == 0) {
//                    getters.add(method);
//                }
//            }
//        }
//
//
//        getters.stream()
//                .sorted(Comparator.comparing(Method::getName)).forEach(method -> {
//            System.out.println(String.format("%s will return %s", method.getName(), method.getReturnType()));
//        });
//
//
//
//        List<Method> setters = new ArrayList<>();
//        for (Method method : methods) {
//            if (method.getName().startsWith("set")) {
//                if (method.getParameterTypes().length == 1) {
//                    if (void.class.equals(method.getReturnType())) {
//                        setters.add(method);
//                    }
//                }
//            }
//        }
//
//        setters.stream()
//                .sorted(Comparator.comparing(Method::getName)).forEach(method -> {
//            System.out.println(String.format("%s and will set field of %s", method.getName(), method.getParameterTypes()[0]));
//        });
//
//      Exercise 3
//======================================================================================================================
        List<Field> fields = Arrays.asList(aClass.getDeclaredFields());
        fields.sort(new Comparator<Field>() {
            @Override
            public int compare(Field o1, Field o2) {
                return o1.getName().compareTo(o2.getName());
            }
        });

        for (Field field : fields) {
            if (!Modifier.isPrivate(field.getModifiers())) {
                System.out.println(field.getName() + " must be private!");
            }
        }

        List<Method> methods =
                Arrays.asList(aClass.getDeclaredMethods());
        Collections.sort(methods, Comparator.comparing(Method::getName));
        for (Method method : methods) {
            if (method.getName().startsWith("get")) {
                if (method.getParameterTypes().length == 0) {
                    if (!Modifier.isPublic(method.getModifiers())) {
                        System.out.println(method.getName() +
                                " have to be public!");
                    }
                }
            }
        }

        for (Method method : methods) {
            if (method.getName().startsWith("set")) {
                if (method.getParameterTypes().length > 0) {
                    if (Modifier.isPublic(method.getModifiers())) {
                        System.out.println(method.getName() +
                                " have to be private!");
                    }
                }
            }
        }

    }
}
