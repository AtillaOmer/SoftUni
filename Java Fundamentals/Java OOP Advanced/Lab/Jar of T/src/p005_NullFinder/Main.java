package p005_NullFinder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> integers = new ArrayList<>();
        Collections.addAll(integers, 1, 2, null, 2, null);

        List<Integer> integerNulls = ListUtils.getNullIndices(integers);
        System.out.println(integerNulls.toString());

        List<String> strings = new ArrayList<>();
        Collections.addAll(strings, "a", null, "c");

        List<Integer> stringNulls = ListUtils.getNullIndices(strings);
        System.out.println(stringNulls.toString());

    }
}
