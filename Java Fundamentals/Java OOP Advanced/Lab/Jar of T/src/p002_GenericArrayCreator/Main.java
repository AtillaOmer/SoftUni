package p002_GenericArrayCreator;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        ArrayCreator<String> arrayCreator = new ArrayCreator<>();

        String[] strings = arrayCreator.create(10, "none");
        Integer[] integers = ArrayCreator.create(Integer.class, 10, 0);

        Arrays.stream(strings).forEach(System.out::println);
        Arrays.stream(integers).forEach(System.out::println);
    }
}
