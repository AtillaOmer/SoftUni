package p002_GenericArrayCreator;

import java.lang.reflect.Array;

public class ArrayCreator<T> {
    public ArrayCreator() {
    }

    public static <T> T[] create(int length, T item) {

        T[] array = (T[]) Array.newInstance(item.getClass(), length);
        for (int i = 0; i < length; i++) {
            array[i] = item;
        }
        return array;
    }

    public static <T> T[] create(Class<T> tClass, int length, T item) {
        T[] array = (T[]) Array.newInstance(tClass, length);

        for (int i = 0; i < length; i++) {
            array[i] = item;
        }
        return array;
    }

}
