package p04_recharge;

public class Main {
    public static void main(String[] args) {
        RechargeStation rechargeStation = new RechargeStation();
        Worker robot = new Robot("robko", 100);
        ((Robot) robot).setCurrentPower(50);
        System.out.println(((Robot) robot).getCurrentPower());
        rechargeStation.recharge(robot);
        System.out.println(((Robot) robot).getCurrentPower());
    }
}
