package p03_employee_info;

import p03_employee_info.contracts.Writer;

public class ConsoleWriter implements Writer {
    @Override
    public void writeLine(String string) {
        System.out.println(string);
    }
}
