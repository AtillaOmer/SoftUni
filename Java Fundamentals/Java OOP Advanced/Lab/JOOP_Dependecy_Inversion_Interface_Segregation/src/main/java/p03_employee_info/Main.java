package p03_employee_info;

import p03_employee_info.contracts.Database;
import p03_employee_info.contracts.Formatter;
import p03_employee_info.contracts.InfoProvider;
import p03_employee_info.contracts.Writer;

public class Main {

    public static void main(String[] args) {
        Formatter formatter = new ConsoleFormatter();
        Writer writer = new ConsoleWriter();
        Database database = new EmployeeDatabase();

        InfoProvider employeeInfo = new EmployeeInfoProvider(database);

        ConsoleClient client = new ConsoleClient(formatter, employeeInfo, writer);

        client.run();
    }
}
