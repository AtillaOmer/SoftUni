package p03_employee_info;

import p03_employee_info.contracts.Database;
import p03_employee_info.contracts.Formatter;
import p03_employee_info.contracts.InfoProvider;
import p03_employee_info.contracts.Writer;

public class ConsoleClient {
    private Formatter formatter;
    private InfoProvider infoProvider;
    private Writer writer;

    public ConsoleClient(Formatter formatter, InfoProvider infoProvider, Writer writer) {
        this.formatter = formatter;
        this.infoProvider = infoProvider;
        this.writer = writer;
    }

    public void run() {
        String output = formatter.format(infoProvider.getEmployeesByName());
        this.writer.writeLine(output);
    }
}
