package p02_services;

import p02_services.contracts.NotificationService;

public class OnlineStoreOrder {

    private NotificationService service;

    public OnlineStoreOrder(NotificationService service) {
        this.service = service;
    }

    public void process() {
        if (this.service.isActive())
            this.service.sendNotification();
    }
}
