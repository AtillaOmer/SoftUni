package p02_services;

import p02_services.contracts.NotificationService;

public class SmsNotificationService implements NotificationService {

    private boolean isActive;

    public SmsNotificationService(boolean isActive) {
        this.isActive = isActive;
    }

    @Override
    public void sendNotification() {
        System.out.println("You received sms notification");
    }

    @Override
    public boolean isActive() {
        return this.isActive;
    }

    @Override
    public void add(NotificationService service) {

    }
}
