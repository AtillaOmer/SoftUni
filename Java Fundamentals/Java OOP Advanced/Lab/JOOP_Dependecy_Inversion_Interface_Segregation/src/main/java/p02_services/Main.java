package p02_services;

import p02_services.contracts.NotificationService;

public class Main {
    public static void main(String[] args) {
        NotificationService services = new CompositeNotificationService();
        services.add(new EmailNotificationService(false));
        services.add(new SmsNotificationService(true));

        OnlineStoreOrder storeOrder = new OnlineStoreOrder(services);
        storeOrder.process();
    }
}
