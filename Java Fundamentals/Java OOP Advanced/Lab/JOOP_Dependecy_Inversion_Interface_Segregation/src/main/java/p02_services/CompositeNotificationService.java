package p02_services;

import p02_services.contracts.NotificationService;

import java.util.ArrayList;
import java.util.List;

public class CompositeNotificationService extends Composite implements NotificationService {
    private List<NotificationService> services;

    public CompositeNotificationService() {
        this.services = new ArrayList<>();
    }

    public List<NotificationService> getServices() {
        return services;
    }

    @Override
    public void sendNotification() {
        for (NotificationService service : this.getServices()) {
            if (service.isActive())
                service.sendNotification();
        }
    }

    @Override
    public boolean isActive() {
        return true;
    }

    @Override
    public void add(NotificationService service) {
        this.getServices().add(service);
    }
}
