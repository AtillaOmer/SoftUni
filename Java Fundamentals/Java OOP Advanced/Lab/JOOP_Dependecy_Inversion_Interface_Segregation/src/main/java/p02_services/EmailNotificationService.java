package p02_services;

import p02_services.contracts.NotificationService;

public class EmailNotificationService implements NotificationService {

    private boolean isActive;

    public EmailNotificationService(boolean isActive) {
        this.isActive = isActive;
    }

    @Override
    public void sendNotification() {
        System.out.println("You received email notification");
    }

    @Override
    public boolean isActive() {
        return this.isActive;
    }

    @Override
    public void add(NotificationService service) {

    }
}
