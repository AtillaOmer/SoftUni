package p01_system_resources.impl;

import p01_system_resources.interfaces.TimeProvider;

import java.time.LocalTime;

public class SystemTimeProvider implements TimeProvider {

    private LocalTime time;

    public SystemTimeProvider() {
        this.time = LocalTime.now();
    }

    @Override
    public int getHour() {
        return time.getHour();
    }
}
