package p01_system_resources;

import p01_system_resources.impl.ConsoleWriter;
import p01_system_resources.impl.SystemTimeProvider;
import p01_system_resources.interfaces.TimeProvider;
import p01_system_resources.interfaces.Writer;

public class Main {
    public static void main(String[] args) {
        TimeProvider time = new SystemTimeProvider();
        Writer writer = new ConsoleWriter();

        GreetingClock greetingClock = new GreetingClock(time, writer);
        greetingClock.greeting();
    }
}
