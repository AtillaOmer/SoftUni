package p01_system_resources;

import p01_system_resources.interfaces.TimeProvider;
import p01_system_resources.interfaces.Writer;

import java.time.LocalTime;

public class GreetingClock {
    private TimeProvider time;
    private Writer writer;

    public GreetingClock(TimeProvider time, Writer writer) {
        this.time = time;
        this.writer = writer;
    }

    public void greeting() {
        if (time.getHour() < 12) {
            System.out.println("Good morning...");
        } else if (time.getHour() < 18) {
            System.out.println("Good afternoon...");
        } else {
            System.out.println("Good evening...");
        }
    }
}
